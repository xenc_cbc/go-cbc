package encryptc

import (
	"crypto/md5"
	"encoding/hex"
	"io"
	"mime/multipart"
	"os"

	scrypt "github.com/elithrar/simple-scrypt"
)

// Encrypt 加密編碼
func Encrypt(password string) string {
	hash, _ := scrypt.GenerateFromPassword([]byte(password), scrypt.DefaultParams)
	return string(hash)
}

// EncryptCompare 加密並比較
func EncryptCompare(password string, hash string) bool {
	err := scrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

// GetMD5ByBytes 依 []byte 內容的 MD5
func GetMD5ByBytes(b []byte) (string, error) {
	hasher := md5.New()
	hasher.Write(b)
	return hex.EncodeToString(hasher.Sum(nil)), nil
}

// GetMD5ByReader 依 Reader 內容的 MD5
func GetMD5ByReader(reader io.Reader) string {
	hasher := md5.New()
	io.Copy(hasher, reader)
	return hex.EncodeToString(hasher.Sum(nil))
}

// GetMD5ByFileHeader 依 FileHeader 內容的 MD5
func GetMD5ByFileHeader(file *multipart.FileHeader) (string, error) {
	fileReader, err := file.Open()
	defer func() {
		fileReader.Close()
	}()

	if err != nil {
		return "", err
	}

	md5 := GetMD5ByReader(fileReader)

	return md5, nil
}

// GetMD5ByFile 產生檔案的 MD5
func GetMD5ByFile(path string) (string, error) {
	hasher := md5.New()
	s, err := os.ReadFile(path)

	if err != nil {
		return "", nil
	}

	hasher.Write(s)
	return hex.EncodeToString(hasher.Sum(nil)), nil
}
