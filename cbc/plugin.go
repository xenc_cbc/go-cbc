package cbc

type Plugin interface {
	// GetName 名稱
	GetName() string
	// GetState 狀態
	GetState() PluginState
}

type ThirdPlugin interface {
	Plugin
}

type FullPlugin interface {
	PluginWithInit
	PluginWithClose
}

type PluginWithInit interface {
	Plugin
	// Init 初始化相關資源
	Init(context *Context, cloader ConfigLoader, logger *Logger)
}

type PluginWithStartup interface {
	Plugin

	// Startup 開始額外的運作，如果已開始運作的話或是還沒有初始化，執行沒有效用
	//	@param cxt
	Startup(cxt *Context)
	// Shutdown 停止額外的運作，如果還沒有開始運作的話，執行沒有效用
	Shutdown()
	// IsStartup 是否已開始額外的運作
	IsStartup() bool
}

type PluginWithClose interface {
	Plugin
	// Close 關閉相關資源
	Close()
}

type PluginState int

const (
	// None 最一開始的狀態
	None PluginState = iota
	// Enable 初始化並成功
	Enable
	// Disable Close 或是初始化完判斷為未使用
	Disable
	// Default 同 Enable 但表示使用預設的設定
	Default
	// Failed 初始化時失敗
	Failed
)

const ThirdPluginNamePrefix = "third."

func (ps PluginState) ToString() string {
	switch ps {
	case Enable:
		return "Enable"
	case None:
		return "None"
	case Disable:
		return "Disable"
	case Failed:
		return "Failed"
	default:
		return "Unknown"
	}
}

type PluginImp struct {
	state PluginState
}

func (p *PluginImp) GetState() PluginState {
	return p.state
}
