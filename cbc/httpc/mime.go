package httpc

import (
	"path"
	"strings"
)

const DefaultMIMEType = "application/octet-stream"

var builtinMimeTypesLower map[string]string = map[string]string{
	"css":  "text/css; charset=utf-8",
	"gif":  "image/gif",
	"htm":  "text/html; charset=utf-8",
	"html": "text/html; charset=utf-8",
	"jpeg": "image/jpeg",
	"jpg":  "image/jpeg",
	"js":   "text/javascript; charset=utf-8",
	"mjs":  "text/javascript; charset=utf-8",
	"pdf":  "application/pdf",
	"png":  "image/png",
	"svg":  "image/svg+xml",
	"wasm": "application/wasm",
	"webp": "image/webp",
	"xml":  "text/xml; charset=utf-8",
	"xlsx": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
}

// DetectMimeTypeByExt 依副檔名判斷 MIME type
// 不在內建的列表中的話，第二個 bool 會傳回 false
func DetectMimeTypeByExt(extName string) (v string, ok bool) {
	v, ok = builtinMimeTypesLower[strings.ToLower(extName)]
	return
}

// DectecMimeTypeByPath 依檔名(路徑)判斷 MIME type
// 不在內建的列表中的話，預設回傳 application/octet-stream
func DectecMimeTypeByPath(filePath string) (string, bool) {
	ext := path.Ext(filePath)
	return DetectMimeTypeByExt(strings.TrimPrefix(ext, "."))
}
