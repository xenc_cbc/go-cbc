package httpc

import "math"

// PageInfo 分頁資訊
type PageInfo struct {
	// Current 現在的頁數
	Current int64 `json:"current"`
	// MaxPage 總頁數
	MaxPage int64 `json:"maxPage"`
	// PageSize 頁面大小
	PageSize int64 `json:"pageSize"`
	// 偏Offset 移量
	Offset int64 `json:"offset"`
	// Total 總筆數
	Total int64 `json:"total"`
	// Valid 數值是否有效
	Valid bool `json:"valid"`
	// OnBase Calculate 時是以 0 還是 1 為 Current 的最小值
	OneBase bool `json:"-"`
}

// IncrementOne Current 增量 1 並呼叫 Calculate
//
//  @receiver p
//  @return bool 是否可以成功
func (p *PageInfo) IncrementOne() bool {
	return p.Increment(1)
}

// Increment Current 增量並呼叫 Calculate
//
//  @receiver p
//  @param val
//  @return bool 是否可以成功
func (p *PageInfo) Increment(val int64) bool {
	calVal := p.Current + val

	if calVal <= p.MaxPage {
		if p.OneBase {
			if calVal > 0 {
				p.Current = calVal
				p.Calculate()
				return p.Valid
			}
		} else {
			if calVal >= 0 {
				p.Current = calVal
				p.Calculate()
				return p.Valid
			}
		}
	}

	return false
}

// DecrementOne Current 減量 1 並呼叫 Calculate
//
//  @receiver p
//  @return bool 是否可以成功
func (p *PageInfo) DecrementOne() bool {
	return p.Decrement(1)
}

// Decrement Current 減量並呼叫 Calculate
//
//  @receiver p
//  @param val
//  @return bool 是否可以成功
func (p *PageInfo) Decrement(val int64) bool {
	return p.Increment(-val)
}

// Calculate 依 Current、PageSize、Total 計算出 MaxPage、Offset、Valid
//
//  @receiver p
func (p *PageInfo) Calculate() {
	maxPage := int64(math.Ceil(float64(p.Total) / float64(p.PageSize)))
	p.Valid = false

	var (
		calPage int64
	)

	if p.OneBase {
		calPage = p.Current - 1
		p.MaxPage = maxPage
	} else {
		calPage = p.Current
		p.MaxPage = maxPage - 1
	}

	if calPage >= 0 && calPage <= p.MaxPage {
		p.Offset = calPage * p.PageSize

		if p.Offset >= 0 && p.Offset < p.Total {
			p.Valid = true
		}
	}
}
