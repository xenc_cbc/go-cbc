package httpc

// PageData 分頁資料
type PageData[T interface{}] struct {
	// 查詢出來的資料
	Data T `json:"data"`
	// 分頁資訊
	Info PageInfo `json:"info"`
}
