package httpc

import (
	"encoding/gob"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
)

const (
	// JSONErrorNotFound 找不到
	JSONErrorNotFound string = "error.not.found"
	// JSONErrorField 欄位驗證錯誤
	JSONErrorField = "error.field"
	// JSONErrorUnknown 不明的錯誤
	JSONErrorUnknown = "error.unknown"
	// JSONErrorGeneral 一般的錯誤
	JSONErrorGeneral = "error.general"
	// JSONErrorUpload 上傳檔案錯誤
	JSONErrorUpload = "error.upload"
	// JSONErrorNeedUpload 需要上傳檔案
	JSONErrorNeedUpload = "error.need.upload"
	// JSONErrorHasRelation 有其他關連資料
	JSONErrorHasRelation = "error.has.relation"
	// JSONErrorNeedPermission 需要權限
	JSONErrorNeedPermission = "error.need.permission"
	// JSONErrorCustome Client 自定顯示錯誤
	JSONErrorCustome = "error.custome"
	// HeaderLastModifiedFormat Http 中 LastModified 的時間格式
	HeaderLastModifiedFormat = "Mon, 02 Jan 2006 15:04:05 GMT"
	dateConvertFormat        = "2006-01-02"
)

func init() {
	gob.Register(UserDetailsImpl{})
}

// GeneratePageInfo 產生換頁資訊。
//
//	@param page zero base
//	@param pageSize
//	@param total
//	@param oneBase Current 及 MaxPage 是以 0 還是 1 為最小值
//	@return PageInfo
func GeneratePageInfo(page int64, pageSize int64, total int64, oneBase bool) PageInfo {
	p := PageInfo{
		Current:  page,
		PageSize: pageSize,
		Total:    total, OneBase: oneBase}
	p.Calculate()

	return p
}

// CreatePageData 包裝為 PageData
//
//	@param datas
//	@param info
//	@return PageData
func CreatePageData[T any](datas T, info PageInfo) PageData[T] {
	return PageData[T]{
		Data: datas,
		Info: info,
	}
}

// PanicError 和 CheckError 類似，但只要有 error 都呼叫 painc
func PanicError(err *error) {
	if *err != nil {
		panic(err)
	}
}

// CheckError 確認是不是有其他的錯誤，將 errorMsg 填入 Msg 及 Result(error 的 string，如果 isDev 為 true)，如果沒有問題回傳 true
func CheckError(json *JSONResult, err *error, errorMsg string, isDev bool) bool {
	if *err != nil {
		json.Msg = errorMsg

		if isDev {
			json.Result = (*err).Error()
		}
	} else {
		return true
	}

	return false
}

// GetMimeType 從 http header 中取得 Content-Type
func GetMimeType(file *multipart.FileHeader) (string, bool) {
	o, k := file.Header["Content-Type"]

	if k {
		return o[0], true
	}

	return "", false
}

// GetRealIP 透過 Request Header 判斷客戶的真實 IP
func GetRealIP(ctx *gin.Context) string {
	ip := ctx.Request.Header.Get("X-Forwarded-For")
	if strings.Contains(ip, "127.0.0.1") || ip == "" {
		ip = ctx.Request.Header.Get("X-Real-IP")
	}

	if ip == "" {
		return "127.0.0.1"
	}

	return ip
}

// ParseAcceptLang Parse accept-language in header to convert and tolower it as: zh_tw, en, jp ...
func ParseAcceptLang(ctx *gin.Context) string {
	httpLangArr := ctx.Request.Header["Accept-Language"]

	if len(httpLangArr) > 0 {
		tarStrings := strings.Split(httpLangArr[0], ";")[0]
		langArr := strings.Split(tarStrings, ",")
		return strings.ToLower(langArr[0])
	}

	return ""
}

// AddValidationErrorToJSON
//
//	加入 JSONErrorField 的訊息，name 及 tag 轉為 FieldError
func AddValidationErrorToJSON(json *JSONResult, name string, tag string) {
	json.Msg = JSONErrorField
	var fes []FieldError

	if json.Result == nil {
		fes = make([]FieldError, 0)
	} else {
		fes = json.Result.([]FieldError)
	}

	fes = append(fes, FieldError{Name: name, Tag: tag})
	json.Result = fes
}

// AddValidationErrorsToJSON
//
//	加入 JSONErrorField 的訊息，把 errorInfoMap 轉換為 FieldError
//	回傳 true 表示 errorInfoMap 不為空， false 則相反 (也不會設定 json.Msg 為 JSONErrorField)
func AddValidationErrorsToJSON(json *JSONResult, errorInfoMap map[string]string) bool {
	infoLen := len(errorInfoMap)

	if infoLen > 0 {
		json.Msg = JSONErrorField
		var fes []FieldError

		if json.Result == nil {
			fes = make([]FieldError, 0, infoLen)
		} else {
			fes = json.Result.([]FieldError)
		}

		for k, v := range errorInfoMap {
			fes = append(fes, FieldError{Name: k, Tag: v})
		}

		json.Result = fes
		return true
	}

	return false
}

// ConvertValidationErrorToJSON 同 ConvertErrorCode，但將結果存入 JsonResult 的 Result 中，不符合條件的 error 將是在 Msg 中寫入 "err.unknown"
func ConvertValidationErrorToJSON(json *JSONResult, err *error) {
	if fArr, ok := ConvertValidationError(err); ok {
		json.Msg = JSONErrorField
		json.Result = fArr
	} else {
		json.Msg = JSONErrorGeneral
		json.Result = (*err).Error()
	}
}

// ConvertValidationError 試著轉換 error 到 github.com/go-playground/validator.ValidationErrors
func ConvertValidationError(err *error) ([]FieldError, bool) {
	errs, ok := (*err).(validator.ValidationErrors)

	if ok {
		l := len(errs)
		fes := make([]FieldError, 0, l)
		for _, v := range errs {
			fes = append(fes, FieldError{Name: v.Field(), Tag: v.Tag()})
		}
		return fes, true
	}

	return nil, false
}

// GetUploadFile 透過 FormFile 取得 FileHeader
//
//	"http: no such file" 或 "request Content-Type isn't multipart/form-data" 會回傳 nil,nil
//	有其他 error 的話，回傳 nil,error
//	沒問題的話，則是回傳 file,nil
func GetUploadFile(c *gin.Context, name string) (*multipart.FileHeader, error) {
	file, err := c.FormFile(name)

	if err == nil {
		return file, nil
	}

	if isInvalidFileRequestError(err) {
		return nil, nil
	}

	return nil, err
}

// MustGetUploadFile 透過 FormFile 取得 FileHeader
//
//	"http: no such file" 或 "request Content-Type isn't multipart/form-data" 會回傳 nil
//	有其他 error 的話，則呼叫 panic
//	沒問題的話，則是回傳 file
func MustGetUploadFile(c *gin.Context, name string) *multipart.FileHeader {
	file, err := GetUploadFile(c, name)

	if err == nil {
		return file
	}

	panic(err)
}

// GetUploadFiles 透過 MultipartForm 取得 FileHeader
//
//	"http: no such file" 或 "request Content-Type isn't multipart/form-data" 會回傳 nil,nil
//	不存在對應名稱的 FileHeader 的話也會傳會 nil,nil
//	有其他 error 的話，回傳 nil,error
//	沒問題的話，則是回傳 file,nil
func GetUploadFiles(c *gin.Context, name string) ([]*multipart.FileHeader, error) {
	fm, err := c.MultipartForm()

	if err == nil {
		f, fileExist := fm.File[name]

		if fileExist {
			return f, nil
		}

		return nil, nil
	}

	if isInvalidFileRequestError(err) {
		return nil, nil
	}

	return nil, err
}

// MustGetUploadFiles 透過 MultipartForm 取得 FileHeader
//
//	"http: no such file" 或 "request Content-Type isn't multipart/form-data" 會回傳 nil
//	不存在對應名稱的 FileHeader 的話也會傳會 nil
//	有其他 error 的話，則呼叫 panic
//	沒問題的話，則是回傳 file
func MustGetUploadFiles(c *gin.Context, name string) []*multipart.FileHeader {
	files, err := GetUploadFiles(c, name)

	if err == nil {
		return files
	}

	panic(err)
}

// isInvalidFileRequestError 確認是不是 "http: no such file" or "request Content-Type isn't multipart/form-data"
func isInvalidFileRequestError(err error) bool {
	if err.Error() == "http: no such file" || err.Error() == "request Content-Type isn't multipart/form-data" {
		return true
	}

	return false
}

// CheckLastModifiedHeader 檢查 Header 中的 If-Modified-Since 符合的話回傳 304 並 abort
//
//	有回傳 304 的話會傳回 false
func CheckLastModifiedHeader(c *gin.Context, lastTime time.Time) bool {
	requestHeader := c.Request.Header
	val, ok := requestHeader["If-Modified-Since"]

	if ok && val[0] == lastTime.UTC().Format(HeaderLastModifiedFormat) {
		c.AbortWithStatus(http.StatusNotModified)
		return false
	}

	return true
}

// CheckEtagHeader 檢查 Header 中的 If-None-Match 符合的話回傳 304 並 abort
//
//	有回傳 304 的話會傳回 false
func CheckEtagHeader(c *gin.Context, nowEtag string) bool {
	requestHeader := c.Request.Header
	etag, etagOk := requestHeader["If-None-Match"]

	if etagOk && etag[0] == nowEtag {
		c.AbortWithStatus(http.StatusNotModified)
		return false
	}

	return true
}

// AddMaxAgeCacheHeader 加入 Cache-Control : public, max-age=maxAge
func AddMaxAgeCacheHeader(c *gin.Context, maxAge int) {
	c.Header("Cache-Control", "public, max-age="+strconv.Itoa(maxAge))
}

// AddFileHeader 加入下傳檔案相關的 Header
//
//	Content-Type,Content-Disposition,Content-Length
//	如果 useLastModified 為 true 加入 Last-Modified
//	如果 etagHash 不為空字串 加入 Etag
//	如果 isAttachment 為 true，Content-Disposition 會加入 attachment
func AddFileHeader(c *gin.Context, fileName string, mimeType string,
	etagHash string, storePath string, useLastModified bool, isAttachment bool) error {
	fileName = url.QueryEscape(fileName)
	disposition := `filename="` + fileName + `"; filename*=utf-8''` + fileName

	if isAttachment {
		disposition = "attachment; " + disposition
	}

	c.Header("Content-Type", mimeType)
	c.Header("Content-Disposition", disposition)

	info, infoErr := os.Stat(storePath)

	if infoErr == nil {
		c.Header("Content-Length", strconv.FormatInt(info.Size(), 10))

		if useLastModified {
			c.Header("Last-Modified", info.ModTime().UTC().Format(HeaderLastModifiedFormat))
		}
	}

	if etagHash != "" {
		c.Header("Etag", etagHash)
	}

	return infoErr
}

// AddCustomFileHeader 加入下傳檔案相關的 Header
//
//	Content-Type,Content-Disposition,Content-Length
//	fileSize 大於 0 才會加入 Content-Length
//	依 lastModified 加入 Last-Modified
//	如果 etagHash 不為空字串 加入 Etag
//	如果 isAttachment 為 true，Content-Disposition 會加入 attachment
func AddCustomFileHeader(c *gin.Context, fileName string, mimeType string,
	etagHash string, fileSize int64, lastModified time.Time, isAttachment bool) {
	fileName = url.QueryEscape(fileName)
	disposition := `filename="` + fileName + `"; filename*=utf-8''` + fileName

	if isAttachment {
		disposition = "attachment; " + disposition
	}

	c.Header("Content-Type", mimeType)
	c.Header("Content-Disposition", disposition)
	c.Header("Last-Modified", lastModified.UTC().Format(HeaderLastModifiedFormat))

	if fileSize > 0 {
		c.Header("Content-Length", strconv.FormatInt(fileSize, 10))
	}

	if etagHash != "" {
		c.Header("Etag", etagHash)
	}
}

// AddDisableCacheHeader 加入 Cache-Control
//
//	Cache-Control: private, no-cache, no-store, must-revalidate
//	Pragma: no-cache
//	Expires: 0
func AddDisableCacheHeader(c *gin.Context) {
	c.Header("Cache-Control", "private, no-cache, no-store, must-revalidate")
	c.Header("Pragma", "no-cache")
	c.Header("Expires", "0")
}

// AddNoStoreHeader 加入 Cache-Control，每次 request 檢查檔案
//
//	Cache-Control: public, no-cache
func AddNoCacheHeader(c *gin.Context) {
	c.Header("Cache-Control", "public, no-cache")
}

// MustShouldBind 同 gin.Context 中的 ShouldBind 不同的是有錯誤會直接使用 panic
func MustShouldBind(c *gin.Context, obj interface{}) {
	if err := c.ShouldBind(obj); err != nil {
		panic(err)
	}
}

// ShouldBindAndConvert 同 gin.Context 中的 ShouldBind ，有錯誤會直接使用 ConvertValidationErrorToJSON
func ShouldBindAndConvert(c *gin.Context, obj interface{}, json *JSONResult) bool {
	if err := c.ShouldBind(obj); err != nil {
		ConvertValidationErrorToJSON(json, &err)
		return false
	}

	return true
}

// SendFileNoStore 傳送檔案加入 no Store Header
//
//	回傳 true 表示有 send file , false 表示檔案不存在不做任何處理
func SendFileNoStore(c *gin.Context, fileName string, mimeType string,
	filePath string, isAttachment bool) bool {
	_, infoErr := os.Stat(filePath)

	if os.IsNotExist(infoErr) {
		return false
	}

	AddDisableCacheHeader(c)

	if err := AddFileHeader(c, fileName, mimeType, "", filePath,
		false, isAttachment); err == nil {
		c.File(filePath)
	} else {
		panic(err)
	}

	return true
}

// SendFile 傳送檔案依設定使用 Etag 及 LastModified Header
//
//	如果 maxAge 是負數的話，會加入 no-cache header
//	回傳 true 表示有 send file or 304 , false 表示檔案不存在不做任何處理
func SendFile(c *gin.Context, fileName string, mimeType string,
	hash string, filePath string, maxAge int, useEtag bool,
	useLastModified bool, isAttachment bool) bool {

	if useEtag {
		if !CheckEtagHeader(c, hash) {
			return true
		}
	}

	info, infoErr := os.Stat(filePath)

	if useLastModified {
		if infoErr == nil {
			lastModified := info.ModTime()
			if !CheckLastModifiedHeader(c, lastModified) {
				return true
			}
		}
	}

	if os.IsNotExist(infoErr) {
		return false
	}

	if maxAge < 0 {
		AddNoCacheHeader(c)
	} else {
		AddMaxAgeCacheHeader(c, maxAge)
	}

	if err := AddFileHeader(c, fileName, mimeType, hash, filePath,
		useLastModified, isAttachment); err == nil {
		c.File(filePath)
	} else {
		panic(err)
	}

	return true
}

// convDate 從 string 中轉換為 date
// 格式為 2006-01-02
func convDate(val string) (time.Time, bool) {
	var (
		t   time.Time
		err error
	)

	if len(val) == 0 {
		return t, false
	}

	t, err = time.Parse(dateConvertFormat, val)

	return t, err == nil
}

// ConvDateParam 從 url path param 中轉換為 time
// 格式為 2006-01-02
func ConvDateParam(c *gin.Context, name string) (time.Time, bool) {
	return convDate(c.Param(name))
}

// ConvIntParam 從 url path param 中轉換為 int
func ConvIntParam(c *gin.Context, name string, defValue int) int {
	val, ok := strconv.Atoi(c.Param(name))

	if ok == nil {
		return val
	}

	return defValue
}

// ConvInt64Param 從 url path param 中轉換為 int64
func ConvInt64Param(c *gin.Context, name string, defValue int64) int64 {
	val, ok := strconv.ParseInt(c.Param(name), 10, 64)

	if ok == nil {
		return val
	}

	return defValue
}

// ConvBoolParam 從 url path param 中轉換為 bool
// 第二個 bool 代表有沒有轉換成功
func ConvBoolParam(c *gin.Context, name string) (bool, bool) {
	val := c.Param(name)

	if len(val) == 0 {
		return false, false
	}

	bVal, err := strconv.ParseBool(val)

	if err != nil {
		return false, false
	}

	return bVal, true
}

// ConvIntQuery 從 query param 中轉換為 int
func ConvIntQuery(c *gin.Context, name string, defValue int) int {
	val, ok := strconv.Atoi(c.Query(name))

	if ok == nil {
		return val
	}

	return defValue
}

// ConvDateQuery 從 query param 中轉換為 time
// 格式為 2006-01-02
func ConvDateQuery(c *gin.Context, name string) (time.Time, bool) {
	return convDate(c.Query(name))
}

// ConvInt64Query 從 query param 中轉換為 int64
func ConvInt64Query(c *gin.Context, name string, defValue int64) int64 {
	val, ok := strconv.ParseInt(c.Query(name), 10, 64)

	if ok == nil {
		return val
	}

	return defValue
}

// ConvPageParam 從 url path param 中轉換為 page(1 base)
func ConvPageParam(c *gin.Context, name string) int {
	p := ConvIntParam(c, name, 1)
	return p
}

// ConvBoolQuery 從 query param 中轉換為 bool
// 第二個 bool 代表有沒有轉換成功
func ConvBoolQuery(c *gin.Context, name string) (bool, bool) {
	val := c.Query(name)

	if len(val) == 0 {
		return false, false
	}

	bVal, err := strconv.ParseBool(val)

	if err != nil {
		return false, false
	}

	return bVal, true
}

// GetInt 從 session 中轉換值為 int
func GetInt(s *sessions.Session, key string) (int, bool) {
	c, ok := (*s).Get(key).(int)
	return c, ok
}

// GetString 從 session 中轉換值為 string
func GetString(s *sessions.Session, key string) (string, bool) {
	c, ok := (*s).Get(key).(string)
	return c, ok
}

// GetStringArray 從 session 中轉換值為 []string
func GetStringArray(s *sessions.Session, key string) ([]string, bool) {
	c, ok := (*s).Get(key).([]string)
	return c, ok
}

// GetBool 從 session 中轉換值為 bool
func GetBool(s *sessions.Session, key string) (bool, bool) {
	c, ok := (*s).Get(key).(bool)
	return c, ok
}

// GetFormIntArray 從 post 中的資料轉換指定名稱為 []int
func GetFormIntArray(c *gin.Context, name string) ([]int, bool) {
	strArr := c.PostFormArray(name)
	strLen := len(strArr)
	intArr := make([]int, strLen)

	for i := 0; i < strLen; i++ {
		num, err := strconv.Atoi(strArr[i])

		if err == nil {
			intArr[i] = num
		} else {
			return intArr, false
		}
	}

	return intArr, true
}

// GetQueryInt64Array 從 query 中的資料轉換指定名稱為 []int64
func GetQueryInt64Array(c *gin.Context, name string) ([]int64, bool) {
	strArr := c.QueryArray(name)
	strLen := len(strArr)
	intArr := make([]int64, strLen)

	for i := 0; i < strLen; i++ {
		num, err := strconv.ParseInt(strArr[i], 10, 64)

		if err == nil {
			intArr[i] = num
		} else {
			return intArr, false
		}
	}

	return intArr, true
}

// GetQueryIntArray 從 query 中的資料轉換指定名稱為 []int
func GetQueryIntArray(c *gin.Context, name string) ([]int, bool) {
	strArr := c.QueryArray(name)
	strLen := len(strArr)
	intArr := make([]int, strLen)

	for i := 0; i < strLen; i++ {
		num, err := strconv.Atoi(strArr[i])

		if err == nil {
			intArr[i] = num
		} else {
			return intArr, false
		}
	}

	return intArr, true
}

// GetFormInt64Array 從 post 中的資料轉換指定名稱為 []int64
func GetFormInt64Array(c *gin.Context, name string) ([]int64, bool) {
	strArr := c.PostFormArray(name)
	strLen := len(strArr)
	intArr := make([]int64, strLen)

	for i := 0; i < strLen; i++ {
		num, err := strconv.ParseInt(strArr[i], 10, 64)

		if err == nil {
			intArr[i] = num
		} else {
			return intArr, false
		}
	}

	return intArr, true
}
