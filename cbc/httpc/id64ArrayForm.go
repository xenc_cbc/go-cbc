package httpc

// ID64ArrayForm int64 array
type ID64ArrayForm struct {
	ID []int64 `form:"id" json:"id" binding:"required"`
}
