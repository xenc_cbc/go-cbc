package httpc

// IDArrayForm int array
type IDArrayForm struct {
	ID []int `form:"id" json:"id" binding:"required"`
}
