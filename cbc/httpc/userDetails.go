package httpc

// UserDetails 識別身份
type UserDetails interface {
	// GetID 識別用的 ID
	GetID() int
	// GetID64 識別用的 ID
	GetID64() int64
	// GetUsername 帳號
	GetUsername() string
	// GetName 名稱
	GetName() string
	// GetRoles role 列表
	GetRoles() []string
	// GetExtInfo 附加的資訊
	GetExtInfo() interface{}
	// HasAnyOneRole 有任一相符的 role
	HasAnyOneRole(targetRole ...string) bool
	// HasRole 相符有全部的 role
	HasRole(targetRole ...string) bool
	// GetPlatform 帳號建立來源
	GetPlatform() string
	// GetLoginWith 登入來源
	GetLoginWith() string
}
