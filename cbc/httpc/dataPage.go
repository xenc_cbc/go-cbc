package httpc

type DataPage struct {
	Data interface{} `json:"data"`
	Page PageInfo    `json:"info"`
}
