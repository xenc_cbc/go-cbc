package httpc

type UserDetailsImpl struct {
	Id        int64       `json:"id"`
	Username  string      `json:"username"`
	Name      string      `json:"name"`
	Roles     []string    `json:"roles"`
	ExtInfo   interface{} `json:"extInfo"`
	Platform  string      `json:"platform"`
	LoginWith string      `json:"loginWith"`
}

func (u UserDetailsImpl) GetID() int {
	return int(u.Id)
}

func (u UserDetailsImpl) GetID64() int64 {
	return u.Id
}

func (u UserDetailsImpl) GetUsername() string {
	return u.Username
}

func (u UserDetailsImpl) GetName() string {
	return u.Name
}

func (u UserDetailsImpl) GetRoles() []string {
	return u.Roles
}

func (u UserDetailsImpl) GetExtInfo() interface{} {
	return u.ExtInfo
}

func (u UserDetailsImpl) HasAnyOneRole(targetRole ...string) bool {
	for _, nr := range u.Roles {
		for _, tr := range targetRole {
			if nr == tr {
				return true
			}
		}
	}

	return false
}

func (u UserDetailsImpl) HasRole(targetRole ...string) bool {
	targetCount := len(targetRole)

	if targetCount <= 1 {
		return u.HasAnyOneRole(targetRole...)
	}

	count := 0

	for _, tr := range targetRole {
		for _, nr := range u.Roles {
			if nr == tr {
				count++
				break
			}
		}
	}

	return targetCount == count
}

func (u UserDetailsImpl) GetPlatform() string {
	return u.Platform
}

func (u UserDetailsImpl) GetLoginWith() string {
	return u.LoginWith
}
