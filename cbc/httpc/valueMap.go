package httpc

type VauleMap map[string]interface{}

func (vm VauleMap) Put(name string, value interface{}) {
	vm[name] = value
}
