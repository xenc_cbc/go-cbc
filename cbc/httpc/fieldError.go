package httpc

type FieldError struct {
	Name string
	Tag  string
}
