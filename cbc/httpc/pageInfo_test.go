package httpc

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPageInfo_IncrementOne(t *testing.T) {
	tests := []struct {
		name   string
		p      *PageInfo
		offset int64
		want   bool
		valid  bool
	}{
		{name: "3+1", p: &PageInfo{Current: 3, Total: 2922, PageSize: 30, MaxPage: 98, OneBase: true, Valid: true},
			offset: 90, want: true, valid: true},
		{name: "2+1", p: &PageInfo{Current: 2, Total: 2922, PageSize: 30, MaxPage: 98, OneBase: true, Valid: true},
			offset: 60, want: true, valid: true},
		{name: "98+1", p: &PageInfo{Current: 98, Total: 2922, PageSize: 30, MaxPage: 98, OneBase: true, Valid: true},
			offset: 0, want: false, valid: true},
		{name: "0+1 zerobase",
			p:      &PageInfo{Current: 0, Total: 2922, PageSize: 30, MaxPage: 97, OneBase: false, Valid: true},
			offset: 30, want: true, valid: true},
		{name: "3+1 zerobase",
			p:      &PageInfo{Current: 3, Total: 2922, PageSize: 30, MaxPage: 97, OneBase: false, Valid: true},
			offset: 120, want: true, valid: true},
		{name: "97+1 zerobase",
			p:      &PageInfo{Current: 97, Total: 2922, PageSize: 30, MaxPage: 97, OneBase: false, Valid: true},
			offset: 0, want: false, valid: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			oldCurrent := tt.p.Current
			got := tt.p.IncrementOne()

			assert.Equal(t, tt.want, got)
			assert.Equal(t, tt.valid, tt.p.Valid)

			if tt.want {
				assert.Equal(t, tt.offset, tt.p.Offset)
			} else {
				assert.Equal(t, oldCurrent, tt.p.Current)
			}
		})
	}
}

func TestPageInfo_Increment(t *testing.T) {
	tests := []struct {
		name   string
		p      *PageInfo
		num    int64
		offset int64
		valid  bool
		want   bool
	}{
		{name: "3+2", p: &PageInfo{Current: 3, Total: 2922, PageSize: 30, MaxPage: 98, OneBase: true, Valid: true},
			num: 2, offset: 120, want: true, valid: true},
		{name: "95+4", p: &PageInfo{Current: 95, Total: 2922, PageSize: 30, MaxPage: 98, OneBase: true, Valid: true},
			num: 4, offset: 0, want: false, valid: true},
		{name: "3+2 zerobase",
			p:   &PageInfo{Current: 3, Total: 2922, PageSize: 30, MaxPage: 97, OneBase: false, Valid: true},
			num: 2, offset: 150, want: true, valid: true},
		{name: "95+4 zerobase",
			p:   &PageInfo{Current: 95, Total: 2922, PageSize: 30, MaxPage: 97, OneBase: false, Valid: true},
			num: 4, offset: 0, want: false, valid: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			oldCurrent := tt.p.Current
			got := tt.p.Increment(tt.num)

			assert.Equal(t, tt.want, got)
			assert.Equal(t, tt.valid, tt.p.Valid)

			if tt.want {
				assert.Equal(t, tt.offset, tt.p.Offset)
			} else {
				assert.Equal(t, oldCurrent, tt.p.Current)
			}
		})
	}
}

func TestPageInfo_DecrementOne(t *testing.T) {
	tests := []struct {
		name   string
		p      *PageInfo
		offset int64
		want   bool
		valid  bool
	}{
		{name: "3-1", p: &PageInfo{Current: 3, Total: 2922, PageSize: 30, MaxPage: 98, OneBase: true, Valid: true},
			offset: 30, want: true, valid: true},
		{name: "2-1", p: &PageInfo{Current: 2, Total: 2922, PageSize: 30, MaxPage: 98, OneBase: true, Valid: true},
			offset: 0, want: true, valid: true},
		{name: "1-1", p: &PageInfo{Current: 1, Total: 2922, PageSize: 30, MaxPage: 98, OneBase: true, Valid: true},
			offset: 0, want: false, valid: true},
		{name: "2-1 zerobase",
			p:      &PageInfo{Current: 2, Total: 2922, PageSize: 30, MaxPage: 97, OneBase: false, Valid: true},
			offset: 30, want: true, valid: true},
		{name: "1-1 zerobase",
			p:      &PageInfo{Current: 1, Total: 2922, PageSize: 30, MaxPage: 97, OneBase: false, Valid: true},
			offset: 0, want: true, valid: true},
		{name: "0-1 zerobase",
			p:      &PageInfo{Current: 0, Total: 2922, PageSize: 30, MaxPage: 97, OneBase: false, Valid: true},
			offset: 0, want: false, valid: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			oldCurrent := tt.p.Current
			got := tt.p.DecrementOne()

			assert.Equal(t, tt.want, got)
			assert.Equal(t, tt.valid, tt.p.Valid)

			if tt.p.Valid {
				assert.Equal(t, tt.offset, tt.p.Offset)
			} else {
				assert.Equal(t, oldCurrent, tt.p.Current)
			}
		})
	}
}

func TestPageInfo_Decrement(t *testing.T) {
	tests := []struct {
		name   string
		p      *PageInfo
		num    int64
		offset int64
		valid  bool
		want   bool
	}{
		{name: "3-2", p: &PageInfo{Current: 3, Total: 2922, PageSize: 30, MaxPage: 98, OneBase: true, Valid: true},
			num: 2, offset: 0, want: true, valid: true},
		{name: "3-4", p: &PageInfo{Current: 3, Total: 2922, PageSize: 30, MaxPage: 98, OneBase: true, Valid: true},
			num: 4, offset: 60, want: false, valid: true},
		{name: "3-3 zerobase",
			p:   &PageInfo{Current: 3, Total: 2922, PageSize: 30, MaxPage: 97, OneBase: false, Valid: true},
			num: 3, offset: 0, want: true, valid: true},
		{name: "3-4 zerobase",
			p:   &PageInfo{Current: 3, Total: 2922, PageSize: 30, MaxPage: 97, OneBase: false, Valid: true},
			num: 4, offset: 60, want: false, valid: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			oldCurrent := tt.p.Current
			got := tt.p.Decrement(tt.num)

			assert.Equal(t, tt.want, got)
			assert.Equal(t, tt.valid, tt.p.Valid)

			if tt.want {
				assert.Equal(t, tt.offset, tt.p.Offset)
			} else {
				assert.Equal(t, oldCurrent, tt.p.Current)
			}
		})
	}
}

func TestPageInfo_Calculate(t *testing.T) {
	tests := []struct {
		name   string
		p      *PageInfo
		max    int64
		offset int64
		valid  bool
	}{
		{name: "1", p: &PageInfo{Current: 1, Total: 2922, PageSize: 30, OneBase: true},
			max: 98, offset: 0, valid: true},
		{name: "2", p: &PageInfo{Current: 2, Total: 2922, PageSize: 30, OneBase: true},
			max: 98, offset: 30, valid: true},
		{name: "3", p: &PageInfo{Current: 3, Total: 2922, PageSize: 30, OneBase: true},
			max: 98, offset: 60, valid: true},
		{name: "err", p: &PageInfo{Current: 999, Total: 2922, PageSize: 30, OneBase: true},
			max: 98, offset: 0, valid: false},
		{name: "0 zerobase", p: &PageInfo{Current: 0, Total: 2922, PageSize: 30, OneBase: false},
			max: 97, offset: 0, valid: true},
		{name: "3 zerobase", p: &PageInfo{Current: 3, Total: 2922, PageSize: 30, OneBase: false},
			max: 97, offset: 90, valid: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.p.Calculate()
			assert.Equal(t, tt.valid, tt.p.Valid)

			if tt.p.Valid {
				assert.Equal(t, tt.max, tt.p.MaxPage)
				assert.Equal(t, tt.offset, tt.p.Offset)
			}
		})
	}
}
