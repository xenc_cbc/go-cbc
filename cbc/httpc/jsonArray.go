package httpc

import "encoding/json"

// JsonArray 為了傳 struct 的 array 又要能 validation
type JsonArray[T any] struct {
	Data []T `json:"data" binding:"dive"`
}

func (i *JsonArray[T]) UnmarshalJSON(b []byte) error {
	return json.Unmarshal(b, &i.Data)
}
