package httpc

import (
	"net/http"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/utilc"
	"github.com/gin-gonic/gin"
)

// JSONResult 對 json 回應的規格
type JSONResult struct {
	Success   bool          `json:"success"`
	Result    interface{}   `json:"result"`
	Msg       string        `json:"msg"`
	MsgDetail []interface{} `json:"msgDetail"`
}

// MarkSuccess 標示為成功
func (j *JSONResult) MarkSuccess(result interface{}) {
	j.Success = true
	j.Result = result
}

// MarkPageData 把結果包裝為 PageData struct
//
//	@receiver j
//	@param data
//	@param info
func (j *JSONResult) MarkPageData(data any, info PageInfo) {
	j.Success = true
	j.Result = PageData[any]{
		Data: data,
		Info: info,
	}
}

// MarkFailed 標示為錯誤
func (j *JSONResult) MarkFailed(msg string) {
	j.Success = false
	j.Msg = msg
}

// MarkFailedWithDetail 標示為錯誤，並提供其他的資訊
//
//	@receiver j
//	@param msg
//	@param detail
func (j *JSONResult) MarkFailedWithDetail(msg string, detail ...interface{}) {
	j.Success = false
	j.Msg = msg
	j.MsgDetail = detail
}

// MarkError 使用 error 的 Error() 設定 Msg，如果是 DetailError 會設定 MsgDetail
//
//	@receiver j
//	@param err
func (j *JSONResult) MarkError(err error) {
	j.Success = false
	j.Msg = err.Error()
	dErr, isDetailErr := err.(utilc.DetailError)

	if isDetailErr {
		j.MsgDetail = dErr.Detail()
	}
}

// MarkDataNotFound 標示為 JSONErrorNotFound
func (j *JSONResult) MarkDataNotFound() {
	j.Success = false
	j.Msg = JSONErrorNotFound
}

// MarkErrorField 標示為 JSONErrorField
func (j *JSONResult) MarkErrorField() {
	j.Success = false
	j.Msg = JSONErrorField
}

// MarkErrorGeneral 標示為 JSONErrorGeneral
func (j *JSONResult) MarkErrorGeneral() {
	j.Success = false
	j.Msg = JSONErrorGeneral
}

// MarkHasRelation 標示為 JSONErrorHasRelation
func (j *JSONResult) MarkHasRelation() {
	j.Success = false
	j.Msg = JSONErrorHasRelation
}

// MarkNeedPermission 標示為 JSONErrorNeedPermission
func (j *JSONResult) MarkNeedPermission() {
	j.Success = false
	j.Msg = JSONErrorNeedPermission
}

// MarkNeedUpload 標示為 JSONErrorNeedUpload
func (j *JSONResult) MarkNeedUpload() {
	j.Success = false
	j.Msg = JSONErrorNeedUpload
}

// MarkErrorUnknown 標示為 JSONErrorUnknown
func (j *JSONResult) MarkErrorUnknown() {
	j.Success = false
	j.Msg = JSONErrorUnknown
}

// MarkErrorUpload 標示為 JSONErrorUpload
func (j *JSONResult) MarkErrorUpload() {
	j.Success = false
	j.Msg = JSONErrorUpload
}

// MarkCustomeError 標示為 JSONErrorCustome
func (j *JSONResult) MarkCustomeError() {
	j.Success = false
	j.Msg = JSONErrorCustome
}

// ResponseOk 呼叫 Context.JSON StatusOK
func (j *JSONResult) ResponseOk(c *gin.Context) {
	c.JSON(http.StatusOK, j)
}

// ResponseForbidden 呼叫 Context.JSON StatusForbidden
func (j *JSONResult) ResponseForbidden(c *gin.Context) {
	c.JSON(http.StatusForbidden, j)
}

// ResponseError 呼叫 Context.JSON StatusInternalServerError
func (j *JSONResult) ResponseError(c *gin.Context) {
	c.JSON(http.StatusInternalServerError, j)
}

// ResponseNotFound 呼叫 Context.JSON StatusNotFound
func (j *JSONResult) ResponseNotFound(c *gin.Context) {
	c.JSON(http.StatusNotFound, j)
}
