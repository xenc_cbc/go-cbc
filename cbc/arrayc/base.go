package arrayc

// DeleteWithIndex 把指定索引的值從陣列中刪除
//  @param arr 來源
//  @param index 索引
//  @return []T 結果
func DeleteWithIndex[T any](arr []T, index int) []T {
	var finalArr []T
	arrLastIndex := len(arr) - 1

	if index < arrLastIndex {
		finalArr = append(arr[:index], arr[index+1:]...)
	} else {
		finalArr = arr[:index]
	}

	return finalArr
}

// DeleteMatchFirst 把第一個相符的從陣列中刪除
//  @param arr 來源
//  @param target 要比對相符的值
//  @return []T 結果
//  @return bool 有沒有相符的
func DeleteMatchFirst[T comparable](arr []T, target T) ([]T, bool) {
	index := 0
	hit := false

	for ; index < len(arr); index++ {
		if hit = (arr[index] == target); hit {
			break
		}
	}

	if hit {
		var finalArr []T
		arrLastIndex := len(arr) - 1

		if index < arrLastIndex {
			finalArr = append(arr[:index], arr[index+1:]...)
		} else {
			finalArr = arr[:index]
		}

		return finalArr, true
	}

	return arr, false
}
