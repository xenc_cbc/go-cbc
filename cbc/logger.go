package cbc

import (
	"log"
	"os"

	zapsyslog "github.com/imperfectgo/zap-syslog"
	"github.com/imperfectgo/zap-syslog/syslog"
	"github.com/natefinch/lumberjack"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var _ PluginWithClose = &Logger{}

// LoggerConfig log 設定
type LoggerConfig struct {
	Debug   bool            `json:"debug"`
	Loggers []LoggerSetting `json:"loggers"`
}

// LoggerSetting log 細部設定
type LoggerSetting struct {
	Type       string            `json:"type"`
	Level      string            `json:"level"`
	Encoder    string            `json:"encoder"`
	Enable     bool              `json:"enable"`
	Properties map[string]string `json:"properties"`
}

// Logger log
type Logger struct {
	PluginImp
	Log              *zap.Logger
	Config           LoggerConfig
	ZapEncoderConfig *zapcore.EncoderConfig
}

// IsDebug 是否為 debug
func (lgr *Logger) IsDebug() bool {
	return lgr.Config.Debug
}

func (lgr *Logger) GetName() string {
	return LoggerPluginName
}

func (lgr *Logger) Close() {
	if lgr.Log != nil {
		lgr.Log.Sync()
	}
}

// Init 初始化
func (lgr *Logger) Init(context *Context, cloader ConfigLoader) *zap.Logger {
	if lgr.Log != nil {
		return lgr.Log
	}

	if lgr.ZapEncoderConfig == nil {
		lgr.ZapEncoderConfig = &zapcore.EncoderConfig{
			TimeKey:        "time",
			LevelKey:       "level",
			NameKey:        "logger",
			CallerKey:      "linenum",
			MessageKey:     "msg",
			StacktraceKey:  "stacktrace",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    zapcore.LowercaseLevelEncoder,
			EncodeTime:     zapcore.ISO8601TimeEncoder,
			EncodeDuration: zapcore.SecondsDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
			EncodeName:     zapcore.FullNameEncoder,
		}
	}

	log.Println("Init Logger")

	var cores []zapcore.Core = make([]zapcore.Core, 0)

	jsonExist, jsonErr := cloader.DecodeConfig("log.json", &lgr.Config)
	jsonFailFlag := jsonErr != nil

	if jsonExist {
		for _, lg := range lgr.Config.Loggers {
			if lg.Enable {
				logLevel := zapcore.InfoLevel
				err := logLevel.UnmarshalText([]byte(lg.Level))
				checkErrorAndStop("log level error.", err)

				atomicLevel := zap.NewAtomicLevel()
				atomicLevel.SetLevel(logLevel)

				var encoder zapcore.Encoder

				if lg.Type == "syslog" {
					hostname, err := os.Hostname()
					checkErrorAndStop("syslog hostname error.", err)
					appName := checkPropertiesAndStop("appName", lg.Properties, "syslog appName empty.")

					pid := os.Getpid()
					encoder = zapsyslog.NewSyslogEncoder(zapsyslog.SyslogEncoderConfig{
						EncoderConfig: *lgr.ZapEncoderConfig,
						Facility:      syslog.LOG_LOCAL0,
						Hostname:      hostname,
						PID:           pid,
						App:           appName,
					})
				} else {
					switch lg.Encoder {
					case "console":
						encoder = zapcore.NewConsoleEncoder(*lgr.ZapEncoderConfig)
					case "json":
						encoder = zapcore.NewJSONEncoder(*lgr.ZapEncoderConfig)
					default:
						log.Fatal("log encoder error.", lg.Encoder)
					}
				}

				var writer zapcore.WriteSyncer

				switch lg.Type {
				case "file":
					filePath := context.ConvertPath(checkPropertiesAndStop("filePath", lg.Properties, "lumberjack filePath."))
					maxSize := checkIntPropertiesAndStop("maxSize", lg.Properties, "lumberjack maxSize.")
					maxBackups := checkIntPropertiesAndStop("maxBackups", lg.Properties, "lumberjack maxBackups.")
					maxAge := checkIntPropertiesAndStop("maxAge", lg.Properties, "lumberjack maxAge.")
					compress := checkBoolPropertiesAndStop("compress", lg.Properties, "lumberjack compress.")
					hook := lumberjack.Logger{
						Filename:   filePath,
						MaxSize:    maxSize,
						MaxBackups: maxBackups,
						MaxAge:     maxAge,
						Compress:   compress,
					}
					writer = zapcore.AddSync(&hook)
				case "syslog":
					serverAddress := checkPropertiesAndStop("serverAddress", lg.Properties, "syslog serverAddress empty.")
					protocol := checkPropertiesAndStop("protocol", lg.Properties, "syslog protocol empty.")
					sink, err := zapsyslog.NewConnSyncer(protocol, serverAddress)
					checkErrorAndStop("syslog Connnet Error.", err)

					writer = zapcore.Lock(sink)
				case "stdout":
					writer = zapcore.AddSync(os.Stdout)
				default:
					log.Fatal("log type error.", lg.Type)
				}

				cores = append(cores, zapcore.NewCore(
					encoder,
					writer,
					atomicLevel,
				))
			}
		}

		lgr.state = Enable
	}

	if len(cores) == 0 {
		// add default logger
		cores = append(cores, zapcore.NewCore(
			zapcore.NewConsoleEncoder(*lgr.ZapEncoderConfig),
			zapcore.AddSync(os.Stdout),
			zap.InfoLevel,
		))
	}

	core := zapcore.NewTee(cores...)

	if lgr.Config.Debug {
		lgr.Log = zap.New(core, zap.AddCaller(), zap.AddStacktrace(zapcore.WarnLevel), zap.Development())
	} else {
		lgr.Log = zap.New(core, zap.AddCaller(), zap.AddStacktrace(zapcore.WarnLevel))
	}

	if jsonFailFlag {
		lgr.state = Failed
		lgr.Log.Error("Logger JSON Load Error", zap.Error(jsonErr))
	} else if !jsonExist {
		lgr.state = Default
	}

	return lgr.Log
}

func checkErrorAndStop(msg string, err error) {
	if err != nil {
		log.Fatal(msg, err)
	}
}
