package cbc

import (
	"log"
	"strconv"
)

const AclPluginName = "ACL"
const DatabasePluginName = "Database"
const GinPluginName = "Gin"
const LoggerPluginName = "Logger"
const RedisPluginName = "Redis"
const StompPluginName = "Stomp"
const TemplatePluginName = "Template"
const WebsocketPluginName = "Websocket"

// GetPluginWithType 以指定的名稱及型態取得 plugin
//
//	@param cxt
//	@param name plugin name
//	@return T
//	@return bool
func GetPluginWithType[T Plugin](cxt *Context, name string) (T, bool) {
	var (
		plugin   T
		pluginOk bool
	)

	v, ok := cxt.GetPlugin(name)

	if ok {
		plugin, pluginOk = v.(T)
		return plugin, pluginOk
	}

	return plugin, ok
}

// GetThirdPluginWithType 以指定的名稱及型態取得 ThirdPlugin
//
//	@param cxt
//	@param name plugin name
//	@return T
//	@return bool
func GetThirdPluginWithType[T ThirdPlugin](cxt *Context, name string) (T, bool) {
	return GetPluginWithType[T](cxt, ThirdPluginNamePrefix+name)
}

func checkBoolPropertiesAndStop(key string, props map[string]string, msg string) bool {
	val := checkPropertiesAndStop(key, props, msg)
	valBool, err := strconv.ParseBool(val)

	if err != nil {
		log.Fatal(msg, err)
	}

	return valBool
}

func checkIntPropertiesAndStop(key string, props map[string]string, msg string) int {
	val := checkPropertiesAndStop(key, props, msg)
	valInt, err := strconv.Atoi(val)

	if err != nil {
		log.Fatal(msg, err)
	}

	return valInt
}

func checkPropertiesAndStop(key string, props map[string]string, msg string) string {
	val, ok := props[key]

	if !ok {
		log.Fatal(msg)
	}

	return val
}
