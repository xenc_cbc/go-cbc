package cbc

import (
	"encoding/json"
	"log"
	"os"
	"path/filepath"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/filec"
)

var _ ConfigLoader = &FileConfigLoader{}

// FileConfigLoader 設定檔載入用
type FileConfigLoader struct {
	ConfigBasePath string
}

// NewFileConfigLoader create new config loader with filesystem
//
//	@param configBasePath 如果為空字串，則使用目前工作目錄中的 config 目錄
//	@return *FileConfigLoader
func NewFileConfigLoader(configBasePath string) *FileConfigLoader {
	if len(configBasePath) == 0 {
		workDir, _ := filec.GetExecutableFolder()
		configBasePath = filepath.Join(workDir, "config")
	}

	return &FileConfigLoader{
		ConfigBasePath: configBasePath,
	}
}

func (config *FileConfigLoader) DecodeConfig(name string, v interface{}) (bool, error) {
	if config.ConfigBasePath == "" {
		config.ConfigBasePath = "./config"
	}
	return config.DecodeJSONFile(filepath.Join(config.ConfigBasePath, name), v)
}

// DecodeJSONFile JSON 設定檔 Decode
//
// 返回 bool 表示檔案是否存在
// 應該判斷 error 再判斷 bool
func (config *FileConfigLoader) DecodeJSONFile(path string, v interface{}) (bool, error) {
	var (
		file *os.File
		err  error
	)

	if !filec.FileExist(path) {
		return false, nil
	}

	file, err = os.Open(path)

	if err != nil {
		return false, err
	}

	defer file.Close()

	decoder := json.NewDecoder(file)
	err = decoder.Decode(v)

	if err != nil {
		log.Fatal("Decode Json error.", err)
	}

	return true, nil
}

func (config *FileConfigLoader) LoadConfig(name string) (bool, *Config) {
	var cfg *Config = &Config{}

	exist, err := config.DecodeConfig(name, cfg)

	if exist {
		return true, cfg
	} else if err != nil {
		log.Println("Load Config Error", err)
	} else {
		log.Println("Load Config Not Exist")
	}

	return false, cfg
}
