package cbc

import (
	"errors"
	"strings"
)

// ACLType ACL 類型
type ACLType int

const (
	// Permit 允許
	Permit ACLType = iota
	// Deny 禁止
	Deny
	// Has 必須有
	Has
	// AnyOne 有其中一個
	AnyOne
	// LoggedIn 登入的話允許
	LoggedIn
	// Anonymous 未登入的話允許
	Anonymous
)

func (a ACLType) String() string {
	return aclTypeArr[a]
}

// Ordinal 轉換成對應的 int
func (a ACLType) Ordinal() int {
	return int(a)
}

var aclTypeArr = []string{
	"Permit",
	"Deny",
	"Has",
	"AnyOne",
	"LoggedIn",
	"Anonymous",
}

// UnmarshalJSON 將 []byte 轉為 ACLType
func (a *ACLType) UnmarshalJSON(data []byte) error {
	var s string = string(data)
	switch strings.ToLower(s) {
	default:
		return errors.New("Unknown ACL Type : " + s)
	case "\"permit\"":
		*a = Permit
	case "\"deny\"":
		*a = Deny
	case "\"has\"":
		*a = Has
	case "\"anyone\"":
		*a = AnyOne
	case "\"loggedin\"":
		*a = LoggedIn
	case "\"anonymous\"":
		*a = Anonymous
	}

	return nil
}

// MarshalJSON 將 []byte 轉為 JSON(String)
func (a ACLType) MarshalJSON() (data []byte, err error) {
	var s string
	switch a {
	default:
		s = "\"Unknown\""
	case Permit:
		s = "\"Permit\""
	case Deny:
		s = "\"Deny\""
	case Has:
		s = "\"Has\""
	case AnyOne:
		s = "\"AnyOne\""
	case LoggedIn:
		s = "\"LoggedIn\""
	case Anonymous:
		s = "\"Anonymous\""
	}
	return []byte(s), nil
}
