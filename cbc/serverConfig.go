package cbc

// ServerConfig http server 設定
type ServerConfig struct {
	Listen             string        `json:"listen"`
	ServerName         string        `json:"serverName"`
	Port               string        `json:"port"`
	Ssl                bool          `json:"ssl"`
	SslSkipVerify      bool          `json:"sslSkipVerify"`
	WebURL             string        `json:"webUrl"`
	ReadTimeoutSecond  int           `json:"readTimeoutSecond"`
	WriteTimeoutSecond int           `json:"writeTimeoutSecond"`
	LoggerLevel        string        `json:"loggerLevel"`
	SslKey             string        `json:"sslKey"`
	SslCrt             string        `json:"sslCrt"`
	SessionConfig      SessionConfig `json:"sessionConfig"`
	StorePath          string        `json:"storePath"`
	AllowCORS          bool          `json:"allowCors"`
	CorsConfig         CorsConfig    `json:"corsConfig"`
}
