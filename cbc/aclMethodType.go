package cbc

import (
	"errors"
	"strings"
)

// ACLMethodType ACL Reqeust Method 類型
type ACLMethodType int

const (
	// ALL All HTTP Method
	ALL ACLMethodType = iota
	// GET HTTP GET
	GET
	// POST HTTP POST
	POST
	// PUT HTTP PUT
	PUT
	// DELETE HTTP DELETE
	DELETE
	// PATCH HTTP PATCH
	PATCH
)

func (a ACLMethodType) String() string {
	return aclMethodTypeArr[a]
}

// Ordinal 轉換成對應的 int
func (a ACLMethodType) Ordinal() int {
	return int(a)
}

var aclMethodTypeArr = []string{
	"ALL",
	"GET",
	"POST",
	"PUT",
	"DELETE",
	"PATCH",
}

// UnmarshalJSON 將 []byte 轉為 ACLMethodType
func (a *ACLMethodType) UnmarshalJSON(data []byte) error {
	var s string = string(data)
	switch strings.ToLower(s) {
	default:
		return errors.New("Unknown ACL Method Type : " + s)
	case "\"all\"":
		*a = ALL
	case "\"get\"":
		*a = GET
	case "\"post\"":
		*a = POST
	case "\"put\"":
		*a = PUT
	case "\"delete\"":
		*a = DELETE
	case "\"patch\"":
		*a = PATCH
	}

	return nil
}

// MarshalJSON 將 []byte 轉為 JSON(String)
func (a ACLMethodType) MarshalJSON() (data []byte, err error) {
	var s string
	switch a {
	default:
		s = "\"unknown\""
	case ALL:
		s = "\"all\""
	case GET:
		s = "\"get\""
	case POST:
		s = "\"post\""
	case PUT:
		s = "\"put\""
	case DELETE:
		s = "\"delete\""
	case PATCH:
		s = "\"patch\""
	}
	return []byte(s), nil
}
