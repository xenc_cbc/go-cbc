package cbc

import (
	"net"
	"sync"
	"time"

	gws "github.com/gorilla/websocket"
	"go.uber.org/zap"
)

type WebSocketWrap struct {
	cWebSocket *WebSocket
	con        *gws.Conn
}

func (ws *WebSocketWrap) NextAndWriteJSON(v interface{}) error {
	ws.NextWriteDeadline()
	return ws.con.WriteJSON(v)
}

func (ws *WebSocketWrap) WriteJSON(v interface{}) error {
	return ws.con.WriteJSON(v)
}

func (ws *WebSocketWrap) GetConnection() *gws.Conn {
	return ws.con
}

func (ws *WebSocketWrap) ReadJSON(v interface{}) error {
	return ws.con.ReadJSON(v)
}

// ReadMessageLoop 使用 for 循環呼叫 ReadMessage
//
//	一直不斷 ReadMessage 傳給 handler 處理，
//	如果 handler 回傳 false代表要結束循環，
//	如果有 error 則呼叫 errorHandler，如果 errorHandler 傳回 false 或是沒有設定 errorHandler 都會結束 for
//	結束時如果有 finishHandler 就呼叫。
func (ws *WebSocketWrap) ReadMessageLoop(con *WebSocketWrap,
	handler func(con *WebSocketWrap, msgType int, data []byte) bool,
	errorHandler func(con *WebSocketWrap, err error) bool,
	finishHandler func(con *WebSocketWrap)) {
	ws.ReadMessageLoopWithPong(handler, errorHandler, finishHandler, nil)
}

// ReadMessageLoopWithPong 使用 for 循環呼叫 ReadMessage
//
//	一直不斷 ReadMessage 傳給 handler 處理，
//	如果 handler 回傳 false代表要結束循環，
//	如果有 error 則呼叫 errorHandler，如果 errorHandler 傳回 false 或是沒有設定 errorHandler 都會結束 for
//	結束時如果有 finishHandler 就呼叫，在 Pong 結束後呼叫指定的 func。
func (ws *WebSocketWrap) ReadMessageLoopWithPong(
	handler func(con *WebSocketWrap, msgType int, data []byte) bool,
	errorHandler func(con *WebSocketWrap, err error) bool,
	finishHandler func(con *WebSocketWrap),
	pongAfterHandler func(con *WebSocketWrap)) {
	defer func() {
		if finishHandler != nil {
			finishHandler(ws)
		}
	}()

	ws.initReadLoopSetting(pongAfterHandler)

	for {
		msgType, data, err := ws.con.ReadMessage()

		if err == nil {
			if !handler(ws, msgType, data) {
				ws.socketLogBreakReadLoop("Handler Return Break Flag")
				break
			}
		} else {
			if errorHandler == nil {
				ws.socketLogBreakReadLoop("Default Error Handle")
				break
			} else {
				if !errorHandler(ws, err) {
					ws.socketLogBreakReadLoop("Error Handler Return Break Flag")
					break
				}
			}
		}
	}
}

// ReadLoop 使用 for 循環呼叫 handler
//
//	一直不斷將 con 傳給 handler 處理，
//	如果 handler 回傳 false，代表要結束循環，
//	結束時如果有 finishHandler 就呼叫。
func (ws *WebSocketWrap) ReadLoop(
	handler func(con *WebSocketWrap) bool,
	finishHandler func(con *WebSocketWrap)) {
	ws.ReadLoopWithPong(handler, finishHandler, nil)
}

// ReadLoopWithPong 使用 for 循環呼叫 handler
//
//	一直不斷將 con 傳給 handler 處理，
//	如果 handler 回傳 false，代表要結束循環，
//	結束時如果有 finishHandler 就呼叫，在 Pong 結束後呼叫指定的 func。
func (ws *WebSocketWrap) ReadLoopWithPong(
	handler func(con *WebSocketWrap) bool,
	finishHandler func(con *WebSocketWrap),
	pongAfterHandler func(con *WebSocketWrap)) {
	defer func() {
		if finishHandler != nil {
			finishHandler(ws)
		}
	}()

	ws.initReadLoopSetting(pongAfterHandler)

	for {
		if !handler(ws) {
			break
		}
	}
}

func (ws *WebSocketWrap) socketLogBreakReadLoop(msg string) {
	if ws.cWebSocket.ActiveLogger.IsDebug() {
		ws.cWebSocket.log.Debug(msg,
			zap.String("action", "breakReadLoop"),
			zap.String("remote", ws.con.RemoteAddr().String()))
	}
}

func (ws *WebSocketWrap) initReadLoopSetting(callback func(*WebSocketWrap)) {
	ws.con.SetReadLimit(ws.cWebSocket.MaxMessageSize)
	ws.NextReadDeadline()
	ws.con.SetPongHandler(func(string) error {
		ws.cWebSocket.NextReadDeadline(ws.con)

		if ws.cWebSocket.Config.ShowPingLog {
			ws.cWebSocket.log.Debug("Receive Pong.",
				zap.String("action", "pong"),
				zap.String("remote", ws.con.RemoteAddr().String()))
		}

		if callback != nil {
			callback(ws)
		}

		return nil
	})
}

// StartPingTicker 使用 goroutine 定時 ping
//
//	回傳的 func 如果呼叫，將會結束 goroutine，在結束後呼叫會導致 error
//	sync.Mutex 是 write 時的鎖定使用.
func (ws *WebSocketWrap) StartPingTicker() (func(), *sync.Mutex) {
	var stopChan chan bool = make(chan bool, 1)
	lock := sync.Mutex{}
	log := ws.cWebSocket.log

	go func() {
		ticker := ws.cWebSocket.CreatePingTicker()

		defer func() {
			ticker.Stop()
		}()

		if ws.cWebSocket.Config.ShowPingLog {
			log.Debug("Start Ping Ticker.",
				zap.String("action", "Start PingTicker"),
				zap.String("remote", ws.con.RemoteAddr().String()))
		}

	bkfor:
		for {
			select {
			case <-stopChan:
				if ws.cWebSocket.Config.ShowPingLog {
					log.Debug("Stop Ping Ticker.",
						zap.String("action", "breakPingTicker"),
						zap.String("remote", ws.con.RemoteAddr().String()))
				}
				break bkfor
			case <-ticker.C:
				if ws.cWebSocket.Config.ShowPingLog {
					log.Debug("Send Ping.",
						zap.String("action", "ping"),
						zap.String("remote", ws.con.RemoteAddr().String()))
				}

				ws.NextWriteDeadline()
				lock.Lock()
				err := ws.con.WriteControl(gws.PingMessage,
					nil,
					time.Now().Add(ws.cWebSocket.WriteWait))
				lock.Unlock()
				if err != nil {
					if ws.cWebSocket.Config.ShowPingLog {
						log.Debug("Ping Error.",
							zap.String("action", "ping"),
							zap.String("remote", ws.con.RemoteAddr().String()),
							zap.Error(err))
					}

					break bkfor
				}
			}
		}

		close(stopChan)
	}()
	return func() {
		stopChan <- true
	}, &lock
}

// NextWriteDeadline 設定 WriteDeadline
func (ws *WebSocketWrap) NextWriteDeadline() {
	ws.con.SetWriteDeadline(time.Now().Add(ws.cWebSocket.WriteWait))
}

// NextReadDeadline 設定 ReadDeadline
func (ws *WebSocketWrap) NextReadDeadline() {
	ws.con.SetReadDeadline(time.Now().Add(ws.cWebSocket.PongWait))
}

// WirtePingMessage 寫入 Ping
func (ws *WebSocketWrap) WirtePingMessage(msg []byte) error {
	ws.NextWriteDeadline()
	if err := ws.con.WriteMessage(gws.PingMessage, msg); err != nil {
		return err
	}

	return nil
}

func (ws *WebSocketWrap) RemoteAddr() net.Addr {
	return ws.con.RemoteAddr()
}

func (ws *WebSocketWrap) RemoteAddrStr() string {
	return ws.con.RemoteAddr().String()
}

func (ws *WebSocketWrap) Close() error {
	return ws.con.Close()
}
