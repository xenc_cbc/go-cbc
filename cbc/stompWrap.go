package cbc

import (
	"github.com/go-stomp/stomp"
	"github.com/go-stomp/stomp/frame"
)

// StompWrap 對 stomp 的輔助使用
type StompWrap struct {
	Con *stomp.Conn
}

// SendText 傳送 text/plain
func (sp *StompWrap) SendText(destination string, body string,
	opts ...func(*frame.Frame) error) error {
	return sp.Con.Send(destination, "text/plain", []byte(body), opts...)
}

// Send 傳送
func (sp *StompWrap) Send(destination string, contentType string,
	body []byte, opts ...func(*frame.Frame) error) error {
	return sp.Con.Send(destination, contentType, body, opts...)
}

// Subscribe 訂閱
func (sp *StompWrap) Subscribe(destination string, ack stomp.AckMode,
	opts ...func(*frame.Frame) error) (*stomp.Subscription, error) {
	return sp.Con.Subscribe(destination, ack, opts...)
}
