package cbc

// ConfigLoader 設定檔載入用
type ConfigLoader interface {

	// DecodeConfigFile 依名稱讀取設定
	//  @param name 設定的名稱
	//  @param v 目的
	//  @return bool 表示檔案是否存在
	//  @return error 應該判斷 error 再判斷 bool
	DecodeConfig(name string, v interface{}) (bool, error)

	// LoadConfig Load JSON格式的 設定檔
	//  @param name 檔名
	//  @return bool 表示檔案是否存在
	//  @return *Config
	LoadConfig(name string) (bool, *Config)
}
