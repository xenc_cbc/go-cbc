package cbc

// RedisConfig redis 設定
type RedisConfig struct {
	Address     string `json:"address"`
	Protocol    string `json:"protocol"`
	Password    string `json:"password"`
	MaxIdle     int    `json:"maxIdle"`
	MaxActive   int    `json:"maxActive"`
	IdleTimeout int64  `json:"idleTimeout"`
	Wait        bool   `json:"wait"`
}
