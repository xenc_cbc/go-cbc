package utilc

import "fmt"

type DetailError interface {
	error
	Message() string
	Detail() []any
}

type DetailErrorImp struct {
	Msg       string `json:"msg"`
	MsgDetail []any  `json:"detail"`
}

func (e DetailErrorImp) Message() string {
	return e.Msg
}

func (e DetailErrorImp) Detail() []any {
	return e.MsgDetail
}

func (e DetailErrorImp) Error() string {
	return e.Msg
}

// Format 使用 fmt.Sprintf，將結果設為 Msg，args 設為 Detail
//  @receiver e
//  @param format
//  @param args
func (e *DetailErrorImp) Format(format string, args ...any) {
	e.Msg = fmt.Sprintf(format, args...)
	e.MsgDetail = args
}

// FmtDetailError 使用 fmt.Sprintf，將結果設為 Msg，args 設為 Detail
//  @param format
//  @param args
func FmtDetailError(format string, args ...any) DetailError {
	var e DetailErrorImp
	e.Msg = fmt.Sprintf(format, args...)
	e.MsgDetail = args
	return e
}
