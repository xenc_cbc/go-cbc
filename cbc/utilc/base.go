package utilc

import (
	"crypto/rand"
	"encoding/hex"
)

// GetRandomName 使用 rand 產生隨機名稱
func GetRandomName() string {
	randBytes := make([]byte, 16)
	rand.Read(randBytes)
	return hex.EncodeToString(randBytes)
}

// IntToAZ 把數字 1 ~ 26 轉為 A ~ Z
//
//	超過 26 的話會以第二位數表現，如 AA
//	主要是用在 EXCEL 的定位
func IntToAZ(v int) string {
	code := ""
	c := v % 26
	as := int(v / 26)

	if as > 0 && c == 0 {
		as--
		c = 26
	}

	for i := 0; i < as; i++ {
		code += "A"
	}

	if c != 0 {
		code = code + string([]rune{rune(c + 64)})
	}

	return code
}

// ToPointer 將傳入的轉為 pointer
func ToPointer[T any](v T) *T {
	return &v
}

// ReverseArray 將傳入的 array 內容順序反轉
//
//	@param arr
func ReverseArray[T any](arr []T) {
	for i, j := 0, len(arr)-1; i < j; i, j = i+1, j-1 {
		arr[i], arr[j] = arr[j], arr[i]
	}
}
