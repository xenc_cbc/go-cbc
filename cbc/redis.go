package cbc

import (
	"time"

	"github.com/gomodule/redigo/redis"
	"go.uber.org/zap"
)

var _ FullPlugin = &Redis{}

// Redis 對應 Redis
type Redis struct {
	PluginImp
	RedisPool *redis.Pool
	Config    RedisConfig
	log       *zap.Logger
}

func (rs *Redis) GetName() string {
	return RedisPluginName
}

// Init 初始化 (redis.json)
func (rs *Redis) Init(context *Context, cloader ConfigLoader, logger *Logger) {
	rs.InitWithFileName(context, cloader, "redis.json", logger)
}

// InitWithFileName 初始化，可自定檔名
func (rs *Redis) InitWithFileName(context *Context, cloader ConfigLoader,
	fileName string, logger *Logger) *redis.Pool {
	rs.log = logger.Log.With(zap.String("source", "redis"))
	jsonExist, jsonErr := cloader.DecodeConfig(fileName, &rs.Config)

	if jsonErr != nil {
		rs.log.Info("json error")
		rs.state = Failed
		return nil
	} else if !jsonExist {
		rs.log.Info("json not found , Disable.")
		rs.state = Disable
		return nil
	}

	v := time.Duration(rs.Config.IdleTimeout) * time.Second
	rs.RedisPool = &redis.Pool{
		MaxIdle:     rs.Config.MaxIdle,
		MaxActive:   rs.Config.MaxActive,
		IdleTimeout: v,
		Wait:        rs.Config.Wait,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial(rs.Config.Protocol, rs.Config.Address)

			if err != nil {
				rs.log.Error("Redis Dial", zap.Error(err))
				return nil, err
			}

			if rs.Config.Password != "" {
				_, err = c.Do("AUTH", rs.Config.Password)

				if err != nil {
					rs.log.Error("Redis AUTH", zap.Error(err))
					return nil, err
				}
			}
			return c, nil
		},
	}

	rs.state = Enable
	return rs.RedisPool
}

func (rs *Redis) Close() {
	if rs.RedisPool != nil {
		rs.RedisPool.Close()
	}

	rs.state = Disable
}
