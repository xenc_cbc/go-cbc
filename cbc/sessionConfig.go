package cbc

import "net/http"

// SessionConfig Session 設定
type SessionConfig struct {
	// Type , redis or cookie
	Name          string        `json:"name"`
	Type          string        `json:"type"`
	KeyPairs      string        `json:"keyPairs"`
	RedisMaxIdel  int           `json:"redisMaxIdel"`
	RedisAddr     string        `json:"redisAddr"`
	RedisProtocol string        `json:"redisProtocol"`
	RedisPassword string        `json:"redisPassword"`
	Path          string        `json:"path"`
	Domain        string        `json:"domain"`
	MaxAge        int           `json:"maxAge"`
	HTTPOnly      bool          `json:"httpOnly"`
	Secure        bool          `json:"secure"`
	SameSite      http.SameSite `json:"sameSite"`
}
