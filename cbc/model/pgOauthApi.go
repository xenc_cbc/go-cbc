package model

import (
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/sqlc"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/web/oauth"
)

type PgOAuthApi struct {
	UserApi UserApi
	RoleApi RoleApi
}

func (dbApi *PgOAuthApi) FindOwnRoleNameByUserID(con sqlc.DB, userID int) []string {
	return dbApi.RoleApi.FindOwnRoleNameByUserID(con, userID)
}

func (dbApi *PgOAuthApi) AddRole(db sqlc.DB, userID int, roleID []int) {
	dbApi.RoleApi.AddRole(db, userID, roleID)
}

func (dbApi *PgOAuthApi) FindByID(con sqlc.DB, id int) (oauth.User, bool) {
	return dbApi.UserApi.FindByID(con, id)
}

func (dbApi *PgOAuthApi) FindByPlatform(con sqlc.DB, platfrom string, platfromID string) (oauth.User, bool) {
	return dbApi.UserApi.FindByPlatform(con, platfrom, platfromID)
}

func (dbApi *PgOAuthApi) GetOAuthAllowEmailForUpdate(dbCon sqlc.DB, email string) (oauth.OAuthAllowEmail, bool) {
	var oAllowEmail oauth.OAuthAllowEmail

	if sqlc.MustGetx(dbCon, &oAllowEmail, `select * from public.oauth_allow_email where email = $1 for update`,
		email) {
		return oAllowEmail, true
	}

	return oAllowEmail, false
}

func (dbApi *PgOAuthApi) FindOAuthInitRoles(dbCon sqlc.DB, email string) []oauth.Role {
	dataArr := make([]oauth.Role, 0)
	sqlc.MustSelectx(dbCon, &dataArr, `select b.id,b.name from public.oauth_role a
	 join public.role b on a.role_id = b.id where a.email = $1`, email)
	return dataArr
}

func (dbApi *PgOAuthApi) FindOAuthLinkByUser(dbCon sqlc.DB, userID int) []oauth.OAuthLink {
	dataArr := make([]oauth.OAuthLink, 0)
	sqlc.MustSelectx(dbCon, &dataArr, `select * from public.oauth_link where user_id = $1`,
		userID)
	return dataArr
}

func (dbApi *PgOAuthApi) GetOAuthLinkByPlatformID(dbCon sqlc.DB, platform string, platformID string) (oauth.OAuthLink, bool) {
	var data oauth.OAuthLink
	return data, sqlc.MustGetx(dbCon, &data, `select * from public.oauth_link where platform = $1 and platform_id = $2`,
		platform, platformID)
}

func (dbApi *PgOAuthApi) AddOAuthLink(dbCon sqlc.DB, platform string, platformID string, userID int) bool {
	rs := dbCon.MustExec(`INSERT INTO public.oauth_link(platform,platform_id,user_id)
	VALUES($1, $2, $3) ON CONFLICT (platform,user_id)
		DO UPDATE SET platform_id=$2`, platform, platformID, userID)
	c, _ := rs.RowsAffected()
	return c > 0
}

func (dbApi *PgOAuthApi) DeleteOAuthLink(dbCon sqlc.DB, platform string, userID int) bool {
	rs := dbCon.MustExec(`DELETE FROM public.oauth_link WHERE platform = $1 AND user_id = $2`,
		platform, userID)
	c, _ := rs.RowsAffected()
	return c > 0
}
