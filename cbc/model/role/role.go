package role

import (
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/sqlc"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/web/oauth"
)

type user2role struct {
	UserID int `db:"user_id"`
	RoleID int `db:"role_id"`
}

// Deprecated: Use model.RoleApi
// FindAll 取得全部的權限角色
func FindAll(con sqlc.DB) []oauth.Role {
	var roles []oauth.Role = make([]oauth.Role, 0)
	sqlc.MustSelectx(con, &roles, "select id , name from public.role")
	return roles
}

// Deprecated: Use model.RoleApi
// HasRole 有沒有指定的權限
func HasRole(con sqlc.DB, userID int, roleName string) bool {
	var count int

	sqlc.MustGetx(con, &count, `select count(*) c from public.user a join public.user2role b
	on a.id = b.user_id join public.role c on c.id = b.role_id
	where a.id = $1 and c.name = $2`, userID, roleName)

	return count > 0
}

// Deprecated: Use model.RoleApi
// FindOwnRoleByUserID 取得 user 所擁有的 role
func FindOwnRoleByUserID(con sqlc.DB, userID int) []oauth.Role {
	var roles []oauth.Role = make([]oauth.Role, 0)

	sqlc.MustSelectx(con, &roles,
		`select c.id,c.name from public.user a join public.user2role b
		on a.id = b.user_id join public.role c on c.id = b.role_id
		where a.id = $1`, userID)

	return roles
}

// Deprecated: Use model.RoleApi
// FindOwnRoleNameByUserID 取得 user 所擁有的 role name
func FindOwnRoleNameByUserID(con sqlc.DB, userID int) []string {
	var roles []string = make([]string, 0)
	rolesArr := FindOwnRoleByUserID(con, userID)

	for _, role := range rolesArr {
		roles = append(roles, role.Name)
	}

	return roles
}

// Deprecated: Use model.RoleApi
// AddRole 加入權限
func AddRole(db sqlc.DB, userID int, roleID []int) {
	var dataArr = make([]user2role, 0)

	for i := 0; i < len(roleID); i++ {
		dataArr = append(dataArr, user2role{
			UserID: userID,
			RoleID: roleID[i],
		})
	}

	db.NamedExec(`insert into public.user2role(user_id,role_id) values(:user_id,:role_id)`, dataArr)
}

// Deprecated: Use model.RoleApi
// RemoveRole 移除權限
func RemoveRole(db sqlc.DB, userID int, roleID int) {
	db.MustExec(`delete from public.user2role where user_id=$1 and role_id=$2`, userID, roleID)
}

// Deprecated: Use model.RoleApi
// RemoveAllRole 變更權限
func RemoveAllRole(db sqlc.DB, userID int) {
	db.MustExec(`delete from public.user2role where user_id=$1`, userID)
}
