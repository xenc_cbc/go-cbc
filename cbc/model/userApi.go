package model

import (
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/model/user"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/sqlc"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/web/oauth"
)

type UserApi interface {

	// UpdatePassword 更新使用者密碼
	//
	//  @param db
	//  @param userID
	//  @param password
	//  @return bool rows affected > 0
	UpdatePassword(db sqlc.DB, userID int, password string) bool

	// GetFilePath user 對應的 filepath
	//
	//  @param db
	//  @param userID
	//  @param fileName
	//  @param storePath
	//  @return string
	GetFilePath(userID int, fileName string, storePath string) string

	// FindByUsername 找 user
	//
	//  @param db
	//  @param username
	//  @return oauth.User
	//  @return bool
	FindByUsername(db sqlc.DB, username string) (oauth.User, bool)

	// SetEnable 設定是否停用
	//
	//  @param db
	//  @param userID
	//  @param enabled
	//  @return bool
	SetEnable(db sqlc.DB, userID int, enabled bool) bool

	// FindList 找 user list
	//
	//  @param db
	//  @param keyword
	//  @param offset
	//  @param limit
	//  @return []oauth.User
	FindList(db sqlc.DB, keyword string,
		offset int64, limit int64) []oauth.User

	// FindListCount user list 總數
	//
	//  @param db
	//  @param keyword
	//  @return int64
	FindListCount(db sqlc.DB, keyword string) int64

	// FindByPlatform 找 user
	//
	//  @param db
	//  @param platfrom
	//  @param platfromID
	//  @return oauth.User
	//  @return bool
	FindByPlatform(db sqlc.DB, platfrom string, platfromID string) (oauth.User, bool)

	// Create 建立帳號(原生)
	//
	//  @param db
	//  @param data
	//  @return int
	//  @return error
	Create(db sqlc.DB, data user.CreateForm) (int, error)

	// Update 更新帳號
	//
	//  @param db
	//  @param id
	//  @param data
	//  @return bool rows affected > 0
	Update(db sqlc.DB, id int, data user.UpdateForm) bool

	// FindByID 找 user
	//
	//  @param db
	//  @param id
	//  @return oauth.User
	//  @return bool
	FindByID(db sqlc.DB, id int) (oauth.User, bool)

	// DeleteByID 刪除帳號
	//
	//  @param db
	//  @param userID
	//  @return bool 代表是否有異動到的列數
	//  @return error sql 執行時是否有錯誤
	DeleteByID(db sqlc.DB, userID int) (bool, error)
}
