package user

import (
	"path/filepath"
	"strconv"
	"strings"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/encryptc"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/sqlc"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/web/oauth"
)

// CreateForm 建立帳號用
type CreateForm struct {
	Username string `form:"username" json:"username" binding:"trim,min=3,unameChar"`
	Name     string `form:"name" json:"name" binding:"trim,required"`
	Email    string `form:"email" json:"email" binding:"trim,required,email"`
	Password string `form:"password" json:"password" binding:"min=6,pwdChar"`
}

// UpdateForm 更新帳號用
type UpdateForm struct {
	Name  string `form:"name" json:"name" binding:"trim,required"`
	Email string `form:"email" json:"email" binding:"trim,required,email"`
}

// UpdatePasswordForm 更新密碼用
type UpdatePasswordForm struct {
	Password string `form:"password" json:"password" binding:"min=6,pwdChar"`
}

// EnableStatus 啟用的狀態
type EnableStatus int

const (
	// Enabled 啓用
	Enabled EnableStatus = iota
	// Disabled 關閉
	Disabled
	// None 不指定
	None
)

// Deprecated: Use UserApi
// UpdatePassword 更新使用者密碼
//
//	回傳為 rows affected > 0
func UpdatePassword(con sqlc.DB, userID int, password string) bool {
	secPwd := encryptc.Encrypt(password)
	rs := con.MustExec(`update public.user set password = $1 where id = $2`, secPwd, userID)
	c, _ := rs.RowsAffected()
	return c > 0
}

// Deprecated: Use model.UserApi
// GetFilePath user 對應的 filepath
func GetFilePath(userID int, fileName string, storePath string) string {
	return filepath.Join(storePath,
		"user",
		strconv.Itoa(userID), fileName)
}

// Deprecated: Use model.UserApi
// FindByUsername 找 user
func FindByUsername(con sqlc.DB, username string) (oauth.User, bool) {
	var ac oauth.User
	ok := sqlc.MustGetx(con, &ac, `select a.id,a.username,a.name,
		a.email,a.password,a.enabled,a.platform,a.platform_id from public.user a
		where a.username = $1`, username)
	return ac, ok
}

// Deprecated: Use model.UserApi
// SetEnable 設定是否停用
func SetEnable(con sqlc.DB, userID int, enabled bool) bool {
	rs := con.MustExec(`update public.user set enabled = $1 where id = $2`, enabled, userID)
	c, _ := rs.RowsAffected()
	return c > 0
}

// Deprecated: Use model.UserApi
// FindList 找 user list，以 username 排序
//
//	@param con
//	@param keyword 包含 username , name , email
//	@param offset
//	@param limit
//	@return []oauth.User
func FindList(con sqlc.DB, keyword string,
	offset int64, limit int64) []oauth.User {
	var userArr []oauth.User = make([]oauth.User, 0)
	sql := `select a.id,a.username,a.name,
		a.email,a.enabled,a.platform,a.platform_id from public.user a`
	var params map[string]interface{} = make(map[string]interface{})

	keyword = strings.TrimSpace(keyword)

	if len(keyword) != 0 {
		params["keyword"] = "%" + strings.ToLower(keyword) + "%"
		sql = sql + ` where LOWER(a.name) like :keyword
			OR LOWER(a.email) like :keyword OR a.username like :keyword `
	}

	params["limit"] = limit
	params["offset"] = offset

	sql = sql + " order by a.username limit :limit offset :offset"
	rs, err := con.NamedQuery(sql, params)

	if err == nil {
		defer rs.Close()

		for rs.Next() {
			var user oauth.User
			rs.StructScan(&user)
			userArr = append(userArr, user)
		}
	} else {
		sqlc.CheckOtherError(&err)
	}

	return userArr
}

// Deprecated: Use model.UserApi
// FindListCount 找 user list 總數
//
//	@param con
//	@param keyword 包含 username , name , email
//	@return int64
func FindListCount(con sqlc.DB, keyword string) int64 {
	var count int64
	sql := `select count(*) from public.user a`

	keyword = strings.TrimSpace(keyword)
	hasParams := len(keyword) != 0

	if hasParams {
		var params map[string]interface{} = make(map[string]interface{})
		params["keyword"] = "%" + keyword + "%"
		sql = sql + ` where a.name like :keyword
			OR LOWER(a.email) like :keyword OR a.username like :keyword `

		rs, err := con.NamedQuery(sql, params)

		if err == nil {
			defer rs.Close()

			for rs.Next() {
				rs.Scan(&count)
			}
		} else {
			sqlc.CheckOtherError(&err)
		}
	} else {
		sqlc.MustGetx(con, &count, sql)
	}

	return count
}

// Deprecated: Use model.UserApi
// FindByPlatform 找 user
func FindByPlatform(con sqlc.DB, platfrom string, platfromID string) (oauth.User, bool) {
	var ac oauth.User
	ok := sqlc.MustGetx(con, &ac, `select a.id,a.username,a.name,
		a.email,a.password,a.enabled,a.platform,a.platform_id from public.user a
		where a.platform = $1 and a.platform_id = $2`, platfrom, platfromID)
	return ac, ok
}

// Deprecated: Use model.UserApi
// Create 建立帳號(原生)
func Create(db sqlc.DB, data CreateForm) (int, error) {
	var id int
	row := db.QueryRowx(`Insert into public.user(username,name,email,password,platform,platform_id)
		values($1,$2,$3,$4,'web',$1) RETURNING id`,
		data.Username, data.Name,
		data.Email, encryptc.Encrypt(data.Password))

	if err := row.Err(); err != nil {
		return 0, err
	}

	row.Scan(&id)
	return id, nil
}

// Deprecated: Use model.UserApi
// Update 更新帳號
//
//	回傳為 rows affected > 0
func Update(db sqlc.DB, id int, data UpdateForm) bool {
	rows, _ := db.MustExec(`update public.user set name=$2 , email=$3 where id=$1`,
		id, data.Name, data.Email).RowsAffected()

	return rows > 0
}

// Deprecated: Use model.UserApi
// FindByID 找 user
func FindByID(con sqlc.DB, id int) (oauth.User, bool) {
	var ac oauth.User
	ok := sqlc.MustGetx(con, &ac, `select a.id,a.username,a.name,
		a.email,a.password,a.enabled,a.platform,a.platform_id from public.user a
		where a.id = $1`, id)
	return ac, ok
}

// Deprecated: Use model.UserApi
// DeleteByID 刪除帳號
// 傳回的 bool 是代表是否有異動到的列數，而 error 則是 sql 執行時是否有錯誤
func DeleteByID(con sqlc.DB, userID int) (bool, error) {
	rs, err := con.Exec(`delete from public.user where id = $1`, userID)
	c, _ := rs.RowsAffected()
	return c > 0, err
}
