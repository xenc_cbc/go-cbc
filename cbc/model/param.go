package model

import "golang.org/x/exp/constraints"

// Param 簡化判斷傳過來的參數
type Param[T any] struct {
	Valid bool
	Value T
}

// CheckParamByLimit 如果 Param 為有效的話，檢查及限制最大及最小值
//  @param param
//  @param defaultVal 如果無效的話，傳回的值
//  @param maxVal
//  @param minVal
//  @return int64
func CheckParamByLimit[T constraints.Ordered](param Param[T], defaultVal T,
	maxVal T, minVal T) T {
	if param.Valid {
		switch {
		case param.Value > maxVal:
			return maxVal
		case param.Value < minVal:
			return minVal
		default:
			return param.Value
		}
	}

	return defaultVal
}
