package model

import (
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/sqlc"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/web/oauth"
)

var _ RoleApi = &PgRoleApi{}

type PgRoleApi struct {
}

type user2role struct {
	UserID int `db:"user_id"`
	RoleID int `db:"role_id"`
}

func (db *PgRoleApi) FindAll(con sqlc.DB) []oauth.Role {
	var roles []oauth.Role = make([]oauth.Role, 0)
	sqlc.MustSelectx(con, &roles, "select id , name from public.role")
	return roles
}

func (db *PgRoleApi) HasRole(con sqlc.DB, userID int, roleName string) bool {
	var count int

	sqlc.MustGetx(con, &count, `select count(*) c from public.user a join public.user2role b
	on a.id = b.user_id join public.role c on c.id = b.role_id
	where a.id = $1 and c.name = $2`, userID, roleName)

	return count > 0
}

func (db *PgRoleApi) FindOwnRoleByUserID(con sqlc.DB, userID int) []oauth.Role {
	var roles []oauth.Role = make([]oauth.Role, 0)

	sqlc.MustSelectx(con, &roles,
		`select c.id,c.name from public.user a join public.user2role b
		on a.id = b.user_id join public.role c on c.id = b.role_id
		where a.id = $1`, userID)

	return roles
}

func (db *PgRoleApi) FindOwnRoleNameByUserID(con sqlc.DB, userID int) []string {
	var roles []string = make([]string, 0)
	rolesArr := db.FindOwnRoleByUserID(con, userID)

	for _, role := range rolesArr {
		roles = append(roles, role.Name)
	}

	return roles
}

func (db *PgRoleApi) AddRole(con sqlc.DB, userID int, roleID []int) {
	var dataArr = make([]user2role, 0)

	for i := 0; i < len(roleID); i++ {
		dataArr = append(dataArr, user2role{
			UserID: userID,
			RoleID: roleID[i],
		})
	}

	con.NamedExec(`insert into public.user2role(user_id,role_id) values(:user_id,:role_id)`, dataArr)
}

func (db *PgRoleApi) RemoveRole(con sqlc.DB, userID int, roleID int) {
	con.MustExec(`delete from public.user2role where user_id=$1 and role_id=$2`, userID, roleID)
}

func (db *PgRoleApi) RemoveAllRole(con sqlc.DB, userID int) {
	con.MustExec(`delete from public.user2role where user_id=$1`, userID)
}
