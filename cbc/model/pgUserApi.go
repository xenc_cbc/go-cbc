package model

import (
	"path/filepath"
	"strconv"
	"strings"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/encryptc"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/model/user"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/sqlc"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/web/oauth"
)

var _ UserApi = &PgUserApi{}

type PgUserApi struct {
}

func (db *PgUserApi) UpdatePassword(con sqlc.DB, userID int, password string) bool {
	secPwd := encryptc.Encrypt(password)
	rs := con.MustExec(`update public.user set password = $1 where id = $2`, secPwd, userID)
	c, _ := rs.RowsAffected()
	return c > 0
}

func (db *PgUserApi) GetFilePath(userID int, fileName string, storePath string) string {
	return filepath.Join(storePath,
		"user",
		strconv.Itoa(userID), fileName)
}

func (db *PgUserApi) FindByUsername(con sqlc.DB, username string) (oauth.User, bool) {
	var ac oauth.User
	ok := sqlc.MustGetx(con, &ac, `select a.id,a.username,a.name,
		a.email,a.password,a.enabled,a.platform,a.platform_id from public.user a
		where a.username = $1`, username)
	return ac, ok
}

func (db *PgUserApi) SetEnable(con sqlc.DB, userID int, enabled bool) bool {
	rs := con.MustExec(`update public.user set enabled = $1 where id = $2`, enabled, userID)
	c, _ := rs.RowsAffected()
	return c > 0
}

func (db *PgUserApi) FindList(con sqlc.DB, keyword string,
	offset int64, limit int64) []oauth.User {
	var userArr []oauth.User = make([]oauth.User, 0)
	sql := `select a.id,a.username,a.name,
			a.email,a.enabled,a.platform,a.platform_id from public.user a`
	var params map[string]interface{} = make(map[string]interface{})

	keyword = strings.TrimSpace(keyword)

	if len(keyword) != 0 {
		params["keyword"] = "%" + strings.ToLower(keyword) + "%"
		sql = sql + ` where LOWER(a.name) like :keyword
				OR LOWER(a.email) like :keyword `
	}

	params["limit"] = limit
	params["offset"] = offset

	sql = sql + " order by a.username limit :limit offset :offset"
	rs, err := con.NamedQuery(sql, params)

	if err == nil {
		defer rs.Close()

		for rs.Next() {
			var user oauth.User
			rs.StructScan(&user)
			userArr = append(userArr, user)
		}
	} else {
		sqlc.CheckOtherError(&err)
	}

	return userArr
}

func (db *PgUserApi) FindListCount(con sqlc.DB, keyword string) int64 {
	var count int64
	sql := `select count(*) from public.user a`

	keyword = strings.TrimSpace(keyword)
	hasParams := len(keyword) != 0

	if hasParams {
		var params map[string]interface{} = make(map[string]interface{})
		params["keyword"] = "%" + keyword + "%"
		sql = sql + ` where a.name like :keyword
			OR LOWER(a.email) like :keyword `

		rs, err := con.NamedQuery(sql, params)

		if err == nil {
			defer rs.Close()

			for rs.Next() {
				rs.Scan(&count)
			}
		} else {
			sqlc.CheckOtherError(&err)
		}
	} else {
		sqlc.MustGetx(con, &count, sql)
	}

	return count
}

func (db *PgUserApi) FindByPlatform(con sqlc.DB, platfrom string, platfromID string) (oauth.User, bool) {
	var ac oauth.User
	ok := sqlc.MustGetx(con, &ac, `select a.id,a.username,a.name,
		a.email,a.password,a.enabled,a.platform,a.platform_id from public.user a
		where a.platform = $1 and a.platform_id = $2`, platfrom, platfromID)
	return ac, ok
}

func (db *PgUserApi) Create(con sqlc.DB, data user.CreateForm) (int, error) {
	var id int
	row := con.QueryRowx(`Insert into public.user(username,name,email,password,platform,platform_id)
		values($1,$2,$3,$4,'web',$1) RETURNING id`,
		data.Username, data.Name,
		data.Email, encryptc.Encrypt(data.Password))

	if err := row.Err(); err != nil {
		return 0, err
	}

	row.Scan(&id)
	return id, nil
}

func (db *PgUserApi) Update(con sqlc.DB, id int, data user.UpdateForm) bool {
	rows, _ := con.MustExec(`update public.user set name=$2 , email=$3 where id=$1`,
		id, data.Name, data.Email).RowsAffected()

	return rows > 0
}

func (db *PgUserApi) FindByID(con sqlc.DB, id int) (oauth.User, bool) {
	var ac oauth.User
	ok := sqlc.MustGetx(con, &ac, `select a.id,a.username,a.name,
		a.email,a.password,a.enabled,a.platform,a.platform_id from public.user a
		where a.id = $1`, id)
	return ac, ok
}

func (db *PgUserApi) DeleteByID(con sqlc.DB, userID int) (bool, error) {
	rs, err := con.Exec(`delete from public.user where id = $1`, userID)
	c, _ := rs.RowsAffected()
	return c > 0, err
}
