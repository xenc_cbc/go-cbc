package model

import (
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/sqlc"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/web/oauth"
)

type RoleApi interface {

	// FindAll 取得全部的權限角色
	//
	//	@param db
	//  @return []oauth.Role
	FindAll(db sqlc.DB) []oauth.Role

	// HasRole 有沒有指定的權限
	//
	//	@param db
	//  @param userID
	//  @param roleName
	//  @return bool
	HasRole(db sqlc.DB, userID int, roleName string) bool

	// FindOwnRoleByUserID 取得 user 所擁有的 role
	//
	//	@param db
	//  @param userID
	//  @return []oauth.Role
	FindOwnRoleByUserID(db sqlc.DB, userID int) []oauth.Role

	// FindOwnRoleNameByUserID 取得 user 所擁有的 role name
	//
	//	@param db
	//  @param userID
	//  @return []string
	FindOwnRoleNameByUserID(db sqlc.DB, userID int) []string

	// AddRole 加入權限
	//
	//	@param db
	//  @param userID
	//  @param roleID
	AddRole(db sqlc.DB, userID int, roleID []int)

	// RemoveRole 移除權限
	//
	//	@param db
	//  @param userID
	//  @param roleID
	RemoveRole(db sqlc.DB, userID int, roleID int)

	// RemoveAllRole 變更權限
	//
	//	@param db
	//  @param userID
	RemoveAllRole(db sqlc.DB, userID int)
}
