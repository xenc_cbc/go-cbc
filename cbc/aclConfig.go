package cbc

// ACLConfig 設定檔
type ACLConfig struct {
	Settings          []ACLSetting           `json:"settings"`
	LogginRequireSsl  bool                   `json:"logginRequireSsl"`
	SessionACLKey     string                 `json:"sessionACLKey"`
	SessionUserKey    string                 `json:"sessionUserKey"`
	SessionMessageKey string                 `json:"sessionMessageKey"`
	SslPort           string                 `json:"sslPort"`
	Hostname          string                 `json:"hostname"`
	LoginPath         string                 `json:"loginPath"`
	DenyPath          string                 `json:"denyPath"`
	DefaultAction     ACLType                `json:"defaultAction"`
	Default403JsonKey string                 `json:"default403JsonKey"`
	JSON403Map        map[string]interface{} `json:"json403Map"`
}
