package cbc

import (
	"errors"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"sync"

	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	"github.com/shopspring/decimal"
)

// DefaultValidator DefaultValidator
type DefaultValidator struct {
	once     sync.Once
	validate *validator.Validate
	lazyFunc func(v *validator.Validate)
}

var _ binding.StructValidator = &DefaultValidator{}
var (
	Validator       *DefaultValidator
	usernameCharReg *regexp.Regexp
	pwdCharReg      *regexp.Regexp
)

func init() {
	Validator = new(DefaultValidator)
	usernameCharReg = regexp.MustCompile(`^[a-zA-Z]+[a-zA-Z0-9@_.]+$`)
	pwdCharReg = regexp.MustCompile(`^[a-zA-Z0-9!@#$%^&*()\[\]<>{}\-+_,.|=~?/\\]+$`)
}

// ValidateStruct ValidateStruct
func (v *DefaultValidator) ValidateStruct(obj interface{}) error {

	if kindOfData(obj) == reflect.Struct {

		v.lazyinit()

		if err := v.validate.Struct(obj); err != nil {
			return error(err)
		}
	}

	return nil
}

// Engine Engine
func (v *DefaultValidator) Engine() interface{} {
	v.lazyinit()
	return v.validate
}

func (v *DefaultValidator) lazyinit() {
	v.once.Do(func() {
		v.validate = validator.New()
		v.validate.SetTagName("binding")
		v.validate.RegisterValidation("unameChar", usernameChar)
		v.validate.RegisterValidation("pwdChar", pwdChar)
		v.validate.RegisterValidation("zeroOrMin", zeroOrMin)
		v.validate.RegisterValidation("trim", trimSpace)

		// add for decimal
		v.validate.RegisterCustomTypeFunc(func(field reflect.Value) interface{} {
			if valuer, ok := field.Interface().(decimal.Decimal); ok {
				return valuer.String()
			}
			return nil
		}, decimal.Decimal{})

		v.validate.RegisterValidation("dgt", decimalGreaterThan)
		v.validate.RegisterValidation("dge", decimalGreaterEqual)
		v.validate.RegisterValidation("dlt", decimalLessThan)
		v.validate.RegisterValidation("dle", decimalLessEqual)

		// add any custom validations etc. here
		if v.lazyFunc != nil {
			v.lazyFunc(v.validate)
		}
	})
}

func (v *DefaultValidator) LazyInit(fn func(v *validator.Validate)) {
	v.lazyFunc = fn
}

func kindOfData(data interface{}) reflect.Kind {

	value := reflect.ValueOf(data)
	valueType := value.Kind()

	if valueType == reflect.Ptr {
		valueType = value.Elem().Kind()
	}
	return valueType
}

// trimSpace 去掉文字前後的空白
//
//	@param fl
//	@return bool
func trimSpace(fl validator.FieldLevel) bool {
	rv := fl.Field()

	if rv.Kind() != reflect.String {
		return false
	}

	rv.SetString(strings.TrimSpace(rv.String()))

	return true
}

// zeroOrMin 檢查為 0 or >= min
//
//	string 的話，為 0 or 大於等於 min 的長度/r/n
//	int 的話為 0 or 大於等於 min
//
//	@param fl
//	@return bool
func zeroOrMin(fl validator.FieldLevel) bool {

	rv := fl.Field()

	if rv.Kind() == reflect.String {
		min, pOk := strconv.Atoi(fl.Param())

		if pOk != nil {
			panic(errors.New(fl.Param() + " invalid number"))
		}

		valLen := len(fl.Field().String())
		return valLen == 0 || valLen >= min
	} else if rv.Kind() == reflect.Int || rv.Kind() == reflect.Int8 ||
		rv.Kind() == reflect.Int16 || rv.Kind() == reflect.Int32 ||
		rv.Kind() == reflect.Int64 {
		min, pOk := strconv.ParseInt(fl.Param(), 10, 64)

		if pOk != nil {
			panic(errors.New(fl.Param() + " invalid number"))
		}

		val := fl.Field().Int()
		return val >= min
	}

	return false
}

func usernameChar(fl validator.FieldLevel) bool {
	rv := fl.Field()

	if rv.Kind() != reflect.String {
		return false
	}

	return len(rv.String()) == 0 || usernameCharReg.MatchString(rv.String())
}

func pwdChar(fl validator.FieldLevel) bool {
	rv := fl.Field()

	if rv.Kind() != reflect.String {
		return false
	}

	return len(rv.String()) == 0 || pwdCharReg.MatchString(rv.String())
}

// decimalGreaterThan  decimal >
//
//	@param fl
//	@return bool
func decimalGreaterThan(fl validator.FieldLevel) bool {
	data, ok := fl.Field().Interface().(string)
	if !ok {
		return false
	}
	value, err := decimal.NewFromString(data)
	if err != nil {
		return false
	}
	baseValue, err := decimal.NewFromString(fl.Param())
	if err != nil {
		return false
	}
	return value.GreaterThan(baseValue)
}

// decimalLessThan  decimal <
//
//	@param fl
//	@return bool
func decimalLessThan(fl validator.FieldLevel) bool {
	data, ok := fl.Field().Interface().(string)
	if !ok {
		return false
	}
	value, err := decimal.NewFromString(data)
	if err != nil {
		return false
	}
	baseValue, err := decimal.NewFromString(fl.Param())
	if err != nil {
		return false
	}
	return value.LessThan(baseValue)
}

// decimalGreaterEqual  decimal >=
//
//	@param fl
//	@return bool
func decimalGreaterEqual(fl validator.FieldLevel) bool {
	data, ok := fl.Field().Interface().(string)
	if !ok {
		return false
	}
	value, err := decimal.NewFromString(data)
	if err != nil {
		return false
	}
	baseValue, err := decimal.NewFromString(fl.Param())
	if err != nil {
		return false
	}
	return value.GreaterThanOrEqual(baseValue)
}

// decimalLessEqual decimal <=
//
//	@param fl
//	@return bool
func decimalLessEqual(fl validator.FieldLevel) bool {
	data, ok := fl.Field().Interface().(string)
	if !ok {
		return false
	}
	value, err := decimal.NewFromString(data)
	if err != nil {
		return false
	}
	baseValue, err := decimal.NewFromString(fl.Param())
	if err != nil {
		return false
	}
	return value.LessThanOrEqual(baseValue)
}
