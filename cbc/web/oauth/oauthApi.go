package oauth

import "bitbucket.org/xenc_cbc/go-cbc/v2/cbc/sqlc"

// User 帳號資料
type User struct {
	ID         int    `db:"id" json:"id"`
	Username   string `db:"username" json:"username"`
	Name       string `db:"name" json:"name"`
	Email      string `db:"email" json:"email"`
	Password   string `db:"password" json:"-"`
	Enabled    bool   `db:"enabled" json:"enabled"`
	Platform   string `db:"platform" json:"platform"`
	PlatformID string `db:"platform_id" json:"platformId"`
}

// Role 權限角色
type Role struct {
	ID   int    `db:"id" json:"id"`
	Name string `db:"name" json:"name"`
}

// OAuthAllowEmail db struct
type OAuthAllowEmail struct {
	Email string `db:"email"`
	Used  bool   `db:"used"`
}

// OAuthLink db struct
type OAuthLink struct {
	Platform   string `db:"platform" json:"platform"`
	PlatformID string `db:"platform_id" json:"platformId"`
	UserID     int    `db:"user_id" json:"userId"`
}

type OAuthApi interface {
	RoleApi
	UserApi
	// GetOAuthAllowEmailForUpdate 依 email 取出對應的記錄並 for update 鎖定
	GetOAuthAllowEmailForUpdate(dbCon sqlc.DB, email string) (OAuthAllowEmail, bool)
	// FindOAuthInitRoles 依 email 取出要初始化的 role 列表
	FindOAuthInitRoles(dbCon sqlc.DB, email string) []Role
	// FindOAuthLinkByUser 依 user id 取出對應的連結記錄
	FindOAuthLinkByUser(dbCon sqlc.DB, userID int) []OAuthLink
	// GetOAuthLinkByPlatformID 依 platform 及 id 取出對應的連結記錄
	GetOAuthLinkByPlatformID(dbCon sqlc.DB, platform string, platformID string) (OAuthLink, bool)
	// AddOAuthLink 加入 OAuthLink , platform 及 userID 重複的話，更新 platformID
	AddOAuthLink(dbCon sqlc.DB, platform string, platformID string, userID int) bool
	// DeleteOAuthLink 刪除 OAuthLink
	DeleteOAuthLink(dbCon sqlc.DB, platform string, userID int) bool
}
