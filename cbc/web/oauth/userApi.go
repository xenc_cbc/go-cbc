package oauth

import "bitbucket.org/xenc_cbc/go-cbc/v2/cbc/sqlc"

type UserApi interface {
	// FindByID 找 user
	FindByID(con sqlc.DB, id int) (User, bool)
	// FindByPlatform 找 user
	FindByPlatform(con sqlc.DB, platfrom string, platfromID string) (User, bool)
}
