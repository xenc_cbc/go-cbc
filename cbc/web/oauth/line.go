package oauth

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/datec"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/httpc"
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
)

// LineConfig oauth line 設定
type LineConfig struct {
	OAuthConfig
	Nonce     string   `json:"nonce"`
	Prompt    string   `json:"prompt"`
	MaxAge    int      `json:"maxAge"`
	BotPrompt string   `json:"botPrompt"`
	Locales   []string `json:"locales"`
}

// LineTokenResponse Line Token Response
type LineTokenResponse struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	RefreshToken string `json:"refresh_token"`
	ExpiresIn    int    `json:"expires_in"`
	Scope        string `json:"scope"`
	IDToken      string `json:"id_token"`
}

// LineProfile Line API Profile Response
type LineProfile struct {
	UserID  string `json:"userId"`
	Name    string `json:"displayName"`
	Picture string `json:"pictureUrl"`
}

// LineVerify Line API Verify Response
type LineVerify struct {
	Iss              string   `json:"iss"`
	Sub              string   `json:"sub"`
	Aud              string   `json:"aud"`
	Exp              int      `json:"exp"`
	Iat              int      `json:"iat"`
	Nonce            string   `json:"nonce"`
	Amr              []string `json:"amr"`
	Name             string   `json:"name"`
	Picture          string   `json:"picture"`
	Email            string   `json:"email"`
	Error            string   `json:"error"`
	ErrorDescription string   `json:"error_description"`
}

func (cfg *LineConfig) InitConfig(log *zap.Logger, dbApi OAuthApi) {
	cfg.OAuthConfig.InitConfig(log, dbApi)
	cfg.OAuthConfig.PlatformCode = "line"
}

func (cfg LineConfig) IsActiveLinkUser() bool {
	return cfg.LinkUser
}

// IsEnabled 是否啟用
func (cfg LineConfig) IsEnabled() bool {
	return cfg.Enabled
}

func (cfg LineConfig) GetTokenResponse(code string) (LineTokenResponse, error) {
	var resJson LineTokenResponse
	data := url.Values{}
	data.Set("grant_type", "authorization_code")
	data.Set("code", code)
	data.Set("redirect_uri", cfg.RedirectURL)
	data.Set("client_id", cfg.ClientID)
	data.Set("client_secret", cfg.ClientSecret)
	resp, err := http.Post("https://api.line.me/oauth2/v2.1/token",
		"application/x-www-form-urlencoded",
		strings.NewReader(data.Encode()))

	if err == nil {
		defer resp.Body.Close()
		err = json.NewDecoder(resp.Body).Decode(&resJson)

		if err == nil {
			return resJson, nil
		} else {
			return resJson, err
		}
	}

	return resJson, err
}

func (cfg LineConfig) GetProfileResponse(accessToken string) (LineProfile, error) {
	var resJson LineProfile

	client := &http.Client{}
	req, _ := http.NewRequest("GET", "https://api.line.me/v2/profile", nil)
	req.Header.Set("Authorization", "Bearer "+accessToken)
	resp, err := client.Do(req)

	if err == nil {
		defer resp.Body.Close()
		err = json.NewDecoder(resp.Body).Decode(&resJson)

		if err == nil {
			if resJson.UserID == "" {
				return resJson, errors.New("profile response valid")
			}

			return resJson, nil
		} else {
			return resJson, err
		}
	}

	return resJson, err
}

func (cfg LineConfig) GetVerifyResponse(idToken string) (LineVerify, error) {
	var resJson LineVerify
	data := url.Values{}
	data.Set("id_token", idToken)
	data.Set("client_id", cfg.ClientID)
	resp, err := http.Post("https://api.line.me/oauth2/v2.1/verify",
		"application/x-www-form-urlencoded",
		strings.NewReader(data.Encode()))

	if err == nil {
		defer resp.Body.Close()
		err = json.NewDecoder(resp.Body).Decode(&resJson)

		if err == nil {
			if resJson.Error == "" {
				return resJson, nil
			} else {
				return resJson, errors.New(resJson.ErrorDescription)
			}
		} else {
			return resJson, err
		}
	}

	return resJson, err
}

// GetURL 取得登入的 URL、state，如果有錯誤的話 error 不會是 nil
func (cfg LineConfig) GetURL() (string, string, error) {
	state := datec.GetTimeStampStr(time.Now())
	URL, err := url.Parse("https://access.line.me/oauth2/v2.1/authorize")

	if err == nil {
		parameters := url.Values{}
		parameters.Add("client_id", cfg.ClientID)
		parameters.Add("scope", strings.Join(cfg.Scopes, " "))
		parameters.Add("redirect_uri", cfg.RedirectURL)
		parameters.Add("response_type", "code")
		parameters.Add("state", state)

		if len(cfg.Nonce) > 0 {
			parameters.Add("nonce", cfg.Nonce)
		}

		if len(cfg.Prompt) > 0 {
			parameters.Add("prompt", cfg.Prompt)
		}

		if cfg.MaxAge >= 0 {
			parameters.Add("max_age", strconv.Itoa(cfg.MaxAge))
		}

		if len(cfg.Locales) > 0 {
			locales := strings.Join(cfg.Locales, " ")
			parameters.Add("ui_locales", locales)
		}

		if len(cfg.BotPrompt) > 0 {
			parameters.Add("bot_prompt", cfg.BotPrompt)
		}

		URL.RawQuery = parameters.Encode()
		return URL.String(), state, nil
	}

	return "", state, err
}

func (cfg LineConfig) GetUserDetail(code string, db *sqlx.DB) (httpc.UserDetails, string, error) {
	oauthUser, err := cfg.GetUserInfo(code)

	if err == nil {
		ud, err := cfg.Login(cfg.GetPlatformCode(), oauthUser.Code, oauthUser.Name, oauthUser.Email, db)
		return ud, oauthUser.Code, err
	}

	return httpc.UserDetailsImpl{}, "", err
}

func (cfg LineConfig) GetUserInfo(code string) (OAuthUser, error) {
	var oauthUser = OAuthUser{}

	var (
		tokenRes LineTokenResponse
		err      error
	)

	tokenRes, err = cfg.GetTokenResponse(code)

	if err == nil {
		var (
			profile LineProfile
			verify  LineVerify
		)

		aToken := tokenRes.AccessToken
		idToken := tokenRes.IDToken

		if aToken == "" || idToken == "" {
			return oauthUser, errors.New("token or id_toke is empty string")
		} else {
			profile, err = cfg.GetProfileResponse(aToken)

			if err == nil {
				verify, err = cfg.GetVerifyResponse(idToken)

				if err == nil {
					oauthUser.Code = profile.UserID
					oauthUser.Name = profile.Name
					oauthUser.Email = verify.Email

					return oauthUser, nil
				}
			}
		}
	}

	return oauthUser, err
}
