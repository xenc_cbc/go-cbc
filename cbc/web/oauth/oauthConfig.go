package oauth

import (
	"errors"
	"strconv"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/httpc"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/sqlc"
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
)

type OAuthExecutor interface {
	// IsEnabled 是否啟用
	IsEnabled() bool
	// IsActiveLinkUser 是否可以登入連結帳號
	IsActiveLinkUser() bool
	GetPlatformCode() string
	// GetUserInfo 驗證後回傳 OAuthUser
	GetUserInfo(code string) (OAuthUser, error)
	// GetUserDetail 驗證並登入後回傳 UserDetails , OAuth 那邊的 user 識別碼
	GetUserDetail(code string, db *sqlx.DB) (httpc.UserDetails, string, error)
	// GetURL 取得登入的 URL、state，如果有錯誤的話 error 不會是 nil
	GetURL() (string, string, error)
	InitConfig(log *zap.Logger, dbApi OAuthApi)
}

// OAuthUser oauth 回傳的基本資料
type OAuthUser struct {
	Code  string
	Name  string
	Email string
}

// OAuthConfig oauth 設定
type OAuthConfig struct {
	PlatformCode string   `json:"platform"`
	Enabled      bool     `json:"enabled"`
	ClientID     string   `json:"clientId"`
	ClientSecret string   `json:"clientSecret"`
	Scopes       []string `json:"scopes"`
	EmailCheck   bool     `json:"emailCheck"`
	DefaultRoles []string `json:"defaultRoles"`
	LinkUser     bool     `json:"linkUser"`
	RedirectURL  string   `json:"redirectUrl"`
	log          *zap.Logger
	dbApi        OAuthApi
}

func (cfg *OAuthConfig) InitConfig(log *zap.Logger, dbApi OAuthApi) {
	cfg.log = log
	cfg.dbApi = dbApi
}

func (cfg *OAuthConfig) GetPlatformCode() string {
	return cfg.PlatformCode
}

func (cfg OAuthConfig) Login(platform string, id string,
	name string, email string, db *sqlx.DB) (httpc.UserDetails, error) {
	ud := httpc.UserDetailsImpl{
		Name: name,
	}

	if platform == "" {
		return ud, errors.New("platform empty")
	}

	if id == "" {
		return ud, errors.New("id empty")
	}

	if name == "" {
		return ud, errors.New("name empty")
	}

	if email == "" {
		return ud, errors.New("email empty")
	}

	var (
		ac   User
		acOk bool
	)

	roles := make([]string, 0)
	userLinked := false

	if cfg.LinkUser {
		oauthLink, linkOk := cfg.dbApi.GetOAuthLinkByPlatformID(db, platform, id)

		if linkOk {
			userID := oauthLink.UserID
			cfg.log.Debug("Login Link User", zap.Int("userID", oauthLink.UserID))

			ac, _ = cfg.dbApi.FindByID(db, userID)
			roles = cfg.dbApi.FindOwnRoleNameByUserID(db, ac.ID)

			userLinked = true
		}
	}

	canLogin := true

	if !userLinked {
		ac, acOk = cfg.dbApi.FindByPlatform(db, platform, id)

		if acOk {
			cfg.log.Debug("Login Exist OAuth User", zap.Int("userID", ac.ID),
				zap.String("username", ac.Username))

			if !ac.Enabled {
				return ud, errors.New("disabled")
			}

			if ac.Email != email {
				// update exist email
				db.MustExec(`UPDATE public.user SET email = $2 where id = $1`,
					ac.ID, email)
			}

			roles = cfg.dbApi.FindOwnRoleNameByUserID(db, ac.ID)
		} else {
			txDbCon := db.MustBegin()
			defer txDbCon.Rollback()

			if cfg.EmailCheck {
				oAllowEmail, emailOk := cfg.dbApi.GetOAuthAllowEmailForUpdate(txDbCon, email)

				if emailOk {
					if oAllowEmail.Used {
						return ud, errors.New("OAuth Email Used")
					}
				} else {
					canLogin = false
				}
			}

			if canLogin {
				cfg.log.Debug("Create OAuth User",
					zap.String("platform", platform), zap.String("platformID", id))
				username := platform + id

				var userID int
				sqlc.MustGetx(txDbCon, &userID, `INSERT INTO public.user(username, name, email, password, enabled, platform, platform_id)
						Values($1,$2,$3,$4,$5,$6,$7) returning id`,
					username, name, email, "-", true, platform, id)

				// * update final username
				username = platform + "_" + strconv.Itoa(userID)
				txDbCon.MustExec(`UPDATE public.user SET username = $2 where id = $1`,
					userID, username)

				ac.ID = userID
				ac.Username = username
				ac.Email = email
				ac.Enabled = true
				ac.Name = name
				ac.Platform = platform
				ac.PlatformID = id

				if cfg.EmailCheck {
					roleArr := cfg.dbApi.FindOAuthInitRoles(txDbCon, email)

					if len(roleArr) != 0 {
						cfg.log.Debug("Init OAuth User Roles",
							zap.Int("userID", userID), zap.String("username", ac.Username))
						roleIDArr := make([]int, 0)

						for i := 0; i < len(roleArr); i++ {
							roleIDArr = append(roleIDArr, roleArr[i].ID)
							roles = append(roles, roleArr[i].Name)
						}

						cfg.dbApi.AddRole(txDbCon, userID, roleIDArr)
					}

					txDbCon.MustExec(`UPDATE public.oauth_allow_email set used = true where email = $1`,
						email)
				}
			}

			txDbCon.Commit()
		}
	}

	if canLogin {
		var newRoles []string
		newRoles = append(newRoles, roles...)

		for _, nRole := range cfg.DefaultRoles {
			hit := true

			for _, oldRole := range roles {
				if oldRole == nRole {
					hit = false
					break
				}
			}

			if hit {
				newRoles = append(newRoles, nRole)
			}
		}

		ud.Id = int64(ac.ID)
		ud.Username = ac.Username
		ud.Name = ac.Name
		ud.Roles = newRoles
		ud.Platform = ac.Platform
		ud.LoginWith = cfg.GetPlatformCode()

		return ud, nil
	}

	return ud, errors.New("disallow login")
}
