package oauth

import "bitbucket.org/xenc_cbc/go-cbc/v2/cbc/sqlc"

type RoleApi interface {
	// FindOwnRoleNameByUserID 取得 user 所擁有的 role name
	FindOwnRoleNameByUserID(con sqlc.DB, userID int) []string
	// AddRole 加入權限
	AddRole(db sqlc.DB, userID int, roleID []int)
}
