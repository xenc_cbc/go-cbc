package oauth

import (
	"context"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/datec"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/httpc"
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

// GoogleConfig oauth Google 設定
type GoogleConfig struct {
	OAuthConfig
	OAuthConf            *oauth2.Config
	AccessType           string `json:"accessType"`
	IncludeGrantedScopes bool   `json:"includeGrantedScopes"`
	LoginHint            string `json:"loginHint"`
	Prompt               string `json:"prompt"`
}

// GoogleUserInfo oauth userinfo
type GoogleUserInfo struct {
	Sub           string `json:"sub"`
	Name          string `json:"name"`
	Given_name    string `json:"given_name"`
	FamilyName    string `json:"family_name"`
	Picture       string `json:"picture"`
	Email         string `json:"email"`
	EmailVerified bool   `json:"email_verified"`
	Locale        string `json:"locale"`
}

func (cfg *GoogleConfig) InitConfig(log *zap.Logger, dbApi OAuthApi) {
	cfg.OAuthConfig.InitConfig(log, dbApi)
	cfg.OAuthConfig.PlatformCode = "google"
	cfg.OAuthConf = &oauth2.Config{
		ClientID:     cfg.ClientID,
		ClientSecret: cfg.ClientSecret,
		Scopes:       cfg.Scopes,
		Endpoint:     google.Endpoint,
	}
}

// IsEnabled 是否啟用
func (cfg GoogleConfig) IsEnabled() bool {
	return cfg.Enabled
}

func (cfg GoogleConfig) IsActiveLinkUser() bool {
	return cfg.LinkUser
}

// GetURL 取得登入的 URL、state，如果有錯誤的話 error 不會是 nil
func (cfg GoogleConfig) GetURL() (string, string, error) {
	state := datec.GetTimeStampStr(time.Now())
	URL, err := url.Parse(cfg.OAuthConf.Endpoint.AuthURL)

	if err == nil {
		parameters := url.Values{}
		parameters.Add("client_id", cfg.OAuthConf.ClientID)
		parameters.Add("scope", strings.Join(cfg.OAuthConf.Scopes, " "))
		parameters.Add("redirect_uri", cfg.RedirectURL)
		parameters.Add("response_type", "code")
		parameters.Add("state", state)

		if len(cfg.AccessType) > 0 {
			parameters.Add("access_type", cfg.AccessType)
		}

		if cfg.IncludeGrantedScopes {
			parameters.Add("include_granted_scopes", "true")
		}

		if len(cfg.LoginHint) > 0 {
			parameters.Add("login_hint", cfg.LoginHint)
		}

		if len(cfg.Prompt) > 0 {
			parameters.Add("prompt", cfg.Prompt)
		}

		URL.RawQuery = parameters.Encode()
		return URL.String(), state, nil
	}

	return "", state, err
}

func (cfg GoogleConfig) GetUserDetail(code string, db *sqlx.DB) (httpc.UserDetails, string, error) {
	oauthUser, err := cfg.GetUserInfo(code)

	if err == nil {
		ud, err := cfg.Login(cfg.GetPlatformCode(),
			oauthUser.Code, oauthUser.Name,
			oauthUser.Email, db)
		return ud, oauthUser.Code, err
	}

	return httpc.UserDetailsImpl{}, "", err
}

func (cfg GoogleConfig) GetUserInfo(code string) (OAuthUser, error) {
	var oauthUser = OAuthUser{}

	var (
		token *oauth2.Token
		err   error
	)

	ops := oauth2.SetAuthURLParam("redirect_uri", cfg.RedirectURL)
	token, err = cfg.OAuthConf.Exchange(context.Background(), code, ops)

	if err == nil {
		var resp *http.Response

		client := &http.Client{}
		req, _ := http.NewRequest("GET", "https://www.googleapis.com/oauth2/v3/userinfo", nil)
		req.Header.Set("Authorization", "Bearer "+token.AccessToken)
		resp, err := client.Do(req)

		if err == nil {
			defer resp.Body.Close()
			var response []byte

			response, err = io.ReadAll(resp.Body)

			if err == nil {
				var userInfo GoogleUserInfo
				err = json.Unmarshal(response, &userInfo)

				if err == nil {
					if userInfo.EmailVerified {
						oauthUser.Code = userInfo.Sub
						oauthUser.Name = userInfo.Name
						oauthUser.Email = userInfo.Email
						return oauthUser, nil
					}

					return oauthUser, errors.New("email_verified , false")
				}
			}
		}
	}

	return oauthUser, err
}
