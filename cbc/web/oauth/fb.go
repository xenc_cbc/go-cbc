package oauth

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/datec"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/httpc"
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/facebook"
)

// FacebookConfig oauth fb 設定
type FacebookConfig struct {
	OAuthConfig
	OAuthConf *oauth2.Config
}

func (cfg *FacebookConfig) InitConfig(log *zap.Logger, dbApi OAuthApi) {
	cfg.OAuthConfig.InitConfig(log, dbApi)
	cfg.OAuthConfig.PlatformCode = "fb"
	cfg.OAuthConf = &oauth2.Config{
		ClientID:     cfg.ClientID,
		ClientSecret: cfg.ClientSecret,
		Scopes:       cfg.Scopes,
		Endpoint:     facebook.Endpoint,
	}
}

// IsEnabled 是否啟用
func (cfg FacebookConfig) IsEnabled() bool {
	return cfg.Enabled
}

func (cfg FacebookConfig) IsActiveLinkUser() bool {
	return cfg.LinkUser
}

// GetLoginURL 取得登入的 URL、state，如果有錯誤的話 error 不會是 nil
func (cfg FacebookConfig) GetURL() (string, string, error) {
	state := datec.GetTimeStampStr(time.Now())
	URL, err := url.Parse(cfg.OAuthConf.Endpoint.AuthURL)

	if err == nil {
		parameters := url.Values{}
		parameters.Add("client_id", cfg.OAuthConf.ClientID)
		parameters.Add("scope", strings.Join(cfg.OAuthConf.Scopes, " "))
		parameters.Add("redirect_uri", cfg.RedirectURL)
		parameters.Add("response_type", "code")
		parameters.Add("state", state)
		URL.RawQuery = parameters.Encode()
		return URL.String(), state, nil
	}

	return "", state, err
}

func (cfg FacebookConfig) GetUserDetail(code string, db *sqlx.DB) (httpc.UserDetails, string, error) {
	oauthUser, err := cfg.GetUserInfo(code)

	if err == nil {
		ud, err := cfg.Login(cfg.GetPlatformCode(), oauthUser.Code, oauthUser.Name, oauthUser.Email, db)
		return ud, oauthUser.Code, err
	}

	return httpc.UserDetailsImpl{}, "", err
}

func (cfg FacebookConfig) GetUserInfo(code string) (OAuthUser, error) {
	var oauthUser = OAuthUser{}

	var (
		token *oauth2.Token
		err   error
	)

	ops := oauth2.SetAuthURLParam("redirect_uri", cfg.RedirectURL)
	token, err = cfg.OAuthConf.Exchange(context.Background(), code, ops)

	if err == nil {
		var resp *http.Response

		resp, err = http.Get("https://graph.facebook.com/me?fields=id,name,email&access_token=" +
			url.QueryEscape(token.AccessToken))

		if err == nil {
			defer resp.Body.Close()
			var response []byte

			response, err = io.ReadAll(resp.Body)

			if err == nil {
				var resMap map[string]string
				err = json.Unmarshal(response, &resMap)

				if err == nil {
					oauthUser.Code = resMap["id"]
					oauthUser.Name = resMap["name"]
					oauthUser.Email = resMap["email"]
					return oauthUser, nil
				}
			}
		}
	}

	return oauthUser, err
}
