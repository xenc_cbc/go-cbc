package web

import (
	"reflect"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc"
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
)

// BaseRoute
type BaseRoute struct {
	Context *WebContext
	Acl     *cbc.ACL
	Log     *zap.Logger
	DB      *sqlx.DB
}

func (inr *BaseRoute) Init(ctxt *WebContext, instance interface{}) {
	var logName string
	t := reflect.TypeOf(instance)

	if t.Kind() == reflect.Pointer {
		v := t.Elem()
		logName = v.Name()
	} else {
		logName = t.Name()
	}

	inr.Context = ctxt
	inr.DB = ctxt.DB()
	inr.Acl = ctxt.ACL
	inr.Log = ctxt.Logger().With(zap.String("source", logName))
}
