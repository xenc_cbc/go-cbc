package web

import (
	"time"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/httpc"
	"github.com/gin-gonic/gin"
)

type ResultType int

const (
	// 400
	Ok ResultType = iota
	// 403
	Forbidden
	// 404
	NotFound
	// 500
	Error
	// not do anything
	Nothing
)

var (
	dateConvertFormat = "2006-01-02"
)

type JSONHandleFunc func(*GinWrap, *httpc.JSONResult) ResultType
type HttpHandleFunc func(*GinWrap)

type Routes struct {
	GinRoutes gin.IRouter
	Context   *WebContext
}

func RoutesWrap(context *WebContext, route gin.IRouter) *Routes {
	return &Routes{GinRoutes: route, Context: context}
}

func (r Routes) doHTTP(f HttpHandleFunc) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		cw := &GinWrap{Gin: ctx, Context: r.Context}
		f(cw)
	}
}

func (r Routes) doJSON(f JSONHandleFunc) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		cw := &GinWrap{Gin: ctx, Context: r.Context}
		json := httpc.JSONResult{}

		switch f(cw, &json) {
		case Ok:
			json.ResponseOk(ctx)
		case NotFound:
			json.ResponseNotFound(ctx)
		case Error:
			json.ResponseError(ctx)
		case Forbidden:
			json.ResponseForbidden(ctx)
		case Nothing:
			// user custome respone , not do anything
		default:
			json.ResponseOk(ctx)
		}

	}
}

func (r *Routes) Any(path string, fn HttpHandleFunc) *Routes {
	r.GinRoutes.Any(path, r.doHTTP(fn))
	return r
}

func (r *Routes) GET(path string, fn HttpHandleFunc) *Routes {
	r.GinRoutes.GET(path, r.doHTTP(fn))
	return r
}

func (r *Routes) POST(path string, fn HttpHandleFunc) *Routes {
	r.GinRoutes.POST(path, r.doHTTP(fn))
	return r
}

func (r *Routes) DELETE(path string, fn HttpHandleFunc) *Routes {
	r.GinRoutes.DELETE(path, r.doHTTP(fn))
	return r
}

func (r *Routes) PATCH(path string, fn HttpHandleFunc) *Routes {
	r.GinRoutes.PATCH(path, r.doHTTP(fn))
	return r
}

func (r *Routes) PUT(path string, fn HttpHandleFunc) *Routes {
	r.GinRoutes.PUT(path, r.doHTTP(fn))
	return r
}

func (r *Routes) OPTIONS(path string, fn HttpHandleFunc) *Routes {
	r.GinRoutes.OPTIONS(path, r.doHTTP(fn))
	return r
}

func (r *Routes) HEAD(path string, fn HttpHandleFunc) *Routes {
	r.GinRoutes.HEAD(path, r.doHTTP(fn))
	return r
}

func (r *Routes) Group(path string) *Routes {
	return RoutesWrap(r.Context, r.GinRoutes.Group(path))
}

func (r *Routes) AnyWithJSON(path string, fn JSONHandleFunc) *Routes {
	r.GinRoutes.Any(path, r.doJSON(fn))
	return r
}

func (r *Routes) GetWithJSON(path string, fn JSONHandleFunc) *Routes {
	r.GinRoutes.GET(path, r.doJSON(fn))
	return r
}

func (r *Routes) PostWithJSON(path string, fn JSONHandleFunc) *Routes {
	r.GinRoutes.POST(path, r.doJSON(fn))
	return r
}

func (r *Routes) DeleteWithJSON(path string, fn JSONHandleFunc) *Routes {
	r.GinRoutes.DELETE(path, r.doJSON(fn))
	return r
}

func (r *Routes) PatchWithJSON(path string, fn JSONHandleFunc) *Routes {
	r.GinRoutes.PATCH(path, r.doJSON(fn))
	return r
}

func (r *Routes) PutWithJSON(path string, fn JSONHandleFunc) *Routes {
	r.GinRoutes.PUT(path, r.doJSON(fn))
	return r
}

func (r *Routes) OptionsWithJSON(path string, fn JSONHandleFunc) *Routes {
	r.GinRoutes.OPTIONS(path, r.doJSON(fn))
	return r
}

func (r *Routes) HeadWithJSON(path string, fn JSONHandleFunc) *Routes {
	r.GinRoutes.HEAD(path, r.doJSON(fn))
	return r
}

// convDate 從 string 中轉換為 date
// 格式為 2006-01-02
func convDate(val string) (time.Time, bool) {
	var (
		t   time.Time
		err error
	)

	if len(val) == 0 {
		return t, false
	}

	t, err = time.Parse(dateConvertFormat, val)

	return t, err == nil
}
