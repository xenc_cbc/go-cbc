package web

import (
	"mime/multipart"
	"net/http"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/httpc"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/model"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/stringc"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

type GinWrap struct {
	Context *WebContext
	Gin     *gin.Context
}

// StringQuery 從 GET 的 Query 取得 string (並 trim 空白)
//
//	@receiver cw
//	@param name
//	@return model.Param 如果為空的 string , Valid 也會是 false
func (cw *GinWrap) StringQuery(name string) model.Param[string] {
	val := strings.Trim(cw.Gin.Query(name), " ")
	return model.Param[string]{Valid: len(val) != 0, Value: val}
}

// IntQuery 從 GET 的 Query 取得 int
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) IntQuery(name string) model.Param[int] {
	val, err := strconv.Atoi(cw.Gin.Query(name))
	return model.Param[int]{Valid: err == nil, Value: val}
}

// Int64Query 從 GET 的 Query 取得 int64
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) Int64Query(name string) model.Param[int64] {
	val, err := strconv.ParseInt(cw.Gin.Query(name), 10, 64)
	return model.Param[int64]{Valid: err == nil, Value: val}
}

// BoolQuery 從 GET 的 Query 取得 bool
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) BoolQuery(name string) model.Param[bool] {
	val := cw.Gin.Query(name)

	if len(val) == 0 {
		return model.Param[bool]{Valid: false, Value: false}
	}

	bVal, err := strconv.ParseBool(val)

	if err != nil {
		return model.Param[bool]{Valid: false, Value: false}
	}

	return model.Param[bool]{Valid: true, Value: bVal}
}

// FloatQuery 從 GET 的 Query 取得 float64
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) FloatQuery(name string) model.Param[float64] {
	val, err := strconv.ParseFloat(cw.Gin.Query(name), 64)
	return model.Param[float64]{Valid: err == nil, Value: val}
}

// DateQuery 從 GET 的 Query 取得 Time (格式為 2006-01-02)
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) DateQuery(name string) model.Param[time.Time] {
	v, ok := convDate(cw.Gin.Query(name))
	return model.Param[time.Time]{Valid: ok, Value: v}
}

// Int64ArrayQuery 從 QueryArray 中的資料轉換為 []int64
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) Int64ArrayQuery(name string) model.Param[[]int64] {
	valArr, err := stringc.ToInt64Array(cw.Gin.QueryArray(name))
	return model.Param[[]int64]{Valid: err == nil, Value: valArr}
}

// Int64ArrayQueryWithSplit ArrayQuery 從 Query 的資料 split 轉換為 []int64
//
//  @receiver cw
//  @param name
//  @param split
//  @return model.Param 轉換錯誤的話 , Valid 會是 false

func (cw *GinWrap) Int64ArrayQueryWithSplit(name string, split string) model.Param[[]int64] {
	valArr, err := stringc.SplitToInt64Array(strings.Trim(cw.Gin.Query(name), " "), split)
	return model.Param[[]int64]{Valid: err == nil, Value: valArr}
}

// IntArrayQuery 從 QueryArray 中的資料轉換為 []int
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) IntArrayQuery(name string) model.Param[[]int] {
	valArr, err := stringc.ToIntArray(cw.Gin.QueryArray(name))
	return model.Param[[]int]{Valid: err == nil, Value: valArr}
}

// IntArrayQueryWithSplit 從 Query 的資料 split 轉換為 []int
//
//  @receiver cw
//  @param name
//  @param split
//  @return model.Param 轉換錯誤的話 , Valid 會是 false

func (cw *GinWrap) IntArrayQueryWithSplit(name string, split string) model.Param[[]int] {
	valArr, err := stringc.SplitToIntArray(strings.Trim(cw.Gin.Query(name), " "), split)
	return model.Param[[]int]{Valid: err == nil, Value: valArr}
}

// FloatArrayQuery 從 QueryArray 中的資料轉換為 []float64
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) FloatArrayQuery(name string) model.Param[[]float64] {
	valArr, err := stringc.ToFloatArray(cw.Gin.QueryArray(name))
	return model.Param[[]float64]{Valid: err == nil, Value: valArr}
}

// FloatArrayQueryWithSplit 從 Query 的資料 split 轉換為 []float64
//
//  @receiver cw
//  @param name
//  @param split
//  @return model.Param 轉換錯誤的話 , Valid 會是 false

func (cw *GinWrap) FloatArrayQueryWithSplit(name string, split string) model.Param[[]float64] {
	valArr, err := stringc.SplitToFloatArray(strings.Trim(cw.Gin.Query(name), " "), split)
	return model.Param[[]float64]{Valid: err == nil, Value: valArr}
}

// BoolArrayQuery 從 QueryArray 中的資料轉換為 []bool
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) BoolArrayQuery(name string) model.Param[[]bool] {
	valArr, err := stringc.ToBoolArray(cw.Gin.QueryArray(name))
	return model.Param[[]bool]{Valid: err == nil, Value: valArr}
}

// BoolArrayQueryWithSplit 從 Query 的資料 split 轉換為 []bool
//
//	@receiver cw
//	@param name
//	@param split
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) BoolArrayQueryWithSplit(name string, split string) model.Param[[]bool] {
	strArr, err := stringc.SplitToBoolArray(strings.Trim(cw.Gin.Query(name), " "), split)
	return model.Param[[]bool]{Valid: err == nil, Value: strArr}
}

// StringArrayQuery 從 QueryArray 中的資料轉換為 []string
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) StringArrayQuery(name string) model.Param[[]string] {
	strArr := cw.Gin.QueryArray(name)
	strLen := len(strArr)

	if strLen == 0 {
		return model.Param[[]string]{Valid: false}
	}

	return model.Param[[]string]{Valid: true, Value: strArr}
}

// StringArrayQueryWithSplit 從 Query 中的資料轉換為 []string
//
//	@receiver cw
//	@param name
//	@param split
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) StringArrayQueryWithSplit(name string, split string) model.Param[[]string] {
	strArr := strings.Split(strings.Trim(cw.Gin.Query(name), " "), split)
	strLen := len(strArr)

	if strLen == 0 {
		return model.Param[[]string]{Valid: false}
	}

	return model.Param[[]string]{Valid: true, Value: strArr}
}

// StringParam 從 URL model.Param 取得 string (並 trim 空白)
//
//	@receiver cw
//	@param name
//	@return model.Param 如果為空的 string , Valid 也會是 false
func (cw *GinWrap) StringParam(name string) model.Param[string] {
	val := strings.Trim(cw.Gin.Param(name), " ")
	return model.Param[string]{Valid: len(val) != 0, Value: val}
}

// IntParam 從 URL model.Param 取得 int
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) IntParam(name string) model.Param[int] {
	val, err := strconv.Atoi(cw.Gin.Param(name))
	return model.Param[int]{Valid: err == nil, Value: val}
}

// Int64Param 從 URL model.Param 取得 int64
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) Int64Param(name string) model.Param[int64] {
	val, err := strconv.ParseInt(cw.Gin.Param(name), 10, 64)
	return model.Param[int64]{Valid: err == nil, Value: val}
}

// BoolParam 從 URL model.Param 取得 bool
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) BoolParam(name string) model.Param[bool] {
	val := cw.Gin.Param(name)

	if len(val) == 0 {
		return model.Param[bool]{Valid: false, Value: false}
	}

	bVal, err := strconv.ParseBool(val)

	if err != nil {
		return model.Param[bool]{Valid: false, Value: false}
	}

	return model.Param[bool]{Valid: true, Value: bVal}
}

// FloatParam 從 URL model.Param 取得 float64
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) FloatParam(name string) model.Param[float64] {
	val, err := strconv.ParseFloat(cw.Gin.Param(name), 64)
	return model.Param[float64]{Valid: err == nil, Value: val}
}

// DateParam 從 URL model.Param 取得 Time (格式為 2006-01-02)
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) DateParam(name string) model.Param[time.Time] {
	v, ok := convDate(cw.Gin.Param(name))
	return model.Param[time.Time]{Valid: ok, Value: v}
}

// UseWebSocket 呼叫 Upgrader 的 Upgrade，回傳 WebSocketWrap
//
//	@receiver cw
//	@return *cbc.WebSocketWrap
//	@return error
func (cw *GinWrap) UseWebSocket() (*cbc.WebSocketWrap, error) {
	return cw.Context.Socket.Wrap(cw.Gin.Writer, cw.Gin.Request, nil)
}

// GetSession 取出 Session
//
//	@receiver cw
//	@return SessionWrap
func (cw *GinWrap) GetSession() *SessionWrap {
	return &SessionWrap{cw.Context, sessions.Default(cw.Gin)}
}

// IsDebug 是否運作在 debug mode
//
//	@receiver cw
//	@return bool
func (cw *GinWrap) IsDebug() bool {
	return cw.Context.IsDebug()
}

// GetRealIP 透過 Request Header 判斷客戶的真實 IP
//
//	@receiver cw
//	@return string
func (cw *GinWrap) GetRealIP() string {
	return httpc.GetRealIP(cw.Gin)
}

// ParseAcceptLang Parse accept-language in header to convert and tolower it as: zh_tw, en, jp ...
//
//	@param ctx
//	@return string
func (cw *GinWrap) ParseAcceptLang() string {
	return httpc.ParseAcceptLang(cw.Gin)
}

// GetUploadFile 透過 FormFile 取得 FileHeader
//
// "http: no such file" 或 "request Content-Type isn't multipart/form-data" 會回傳 nil,nil
// 有其他 error 的話，回傳 nil,error
// 沒問題的話，則是回傳 file,nil
//
//	@receiver cw
//	@param name
//	@return *multipart.FileHeader
//	@return error
func (cw *GinWrap) GetUploadFile(name string) (*multipart.FileHeader, error) {
	return httpc.GetUploadFile(cw.Gin, name)
}

// GetUploadFiles func GetUploadFiles 透過 MultipartForm 取得 FileHeader
//
// "http: no such file" 或 "request Content-Type isn't multipart/form-data" 會回傳 nil,nil
// 不存在對應名稱的 FileHeader 的話也會傳會 nil,nil
// 有其他 error 的話，回傳 nil,error
// 沒問題的話，則是回傳 file,nil
//
//	@receiver cw
//	@param name
//	@return []*multipart.FileHeader
//	@return error
func (cw *GinWrap) GetUploadFiles(name string) ([]*multipart.FileHeader, error) {
	return httpc.GetUploadFiles(cw.Gin, name)
}

// MustGetUploadFiles 透過 MultipartForm 取得 FileHeader
//
// "http: no such file" 或 "request Content-Type isn't multipart/form-data" 會回傳 nil
// 不存在對應名稱的 FileHeader 的話也會傳會 nil
// 有其他 error 的話，則呼叫 panic
// 沒問題的話，則是回傳 file
//
//	@receiver cw
//	@param name
//	@return []*multipart.FileHeader
func (cw *GinWrap) MustGetUploadFiles(name string) []*multipart.FileHeader {
	return httpc.MustGetUploadFiles(cw.Gin, name)
}

// CheckLastModifiedHeader 檢查 Header 中的 If-Modified-Since 符合的話回傳 304 並 abort
//
// 有回傳 304 的話會傳回 false
//
//	@receiver cw
//	@param lastTime
//	@return bool
func (cw *GinWrap) CheckLastModifiedHeader(lastTime time.Time) bool {
	return httpc.CheckLastModifiedHeader(cw.Gin, lastTime)
}

// CheckEtagHeader 檢查 Header 中的 If-None-Match 符合的話回傳 304 並 abort
//
// 有回傳 304 的話會傳回 false
//
//	@receiver cw
//	@param nowEtag
//	@return bool
func (cw *GinWrap) CheckEtagHeader(nowEtag string) bool {
	return httpc.CheckEtagHeader(cw.Gin, nowEtag)
}

// AddMaxAgeCacheHeader 加入 Cache-Control : public, max-age=maxAge
//
//	@receiver cw
//	@param maxAge
func (cw *GinWrap) AddMaxAgeCacheHeader(maxAge int) {
	httpc.AddMaxAgeCacheHeader(cw.Gin, maxAge)
}

// AddFileHeader 加入下傳檔案相關的 Header
//
// Content-Type,Content-Disposition,Content-Length
// 如果 useLastModified 為 true 加入 Last-Modified
// 如果 etagHash 不為空字串 加入 Etag
// 如果 isAttachment 為 true，Content-Disposition 會加入 attachment
//
//	@receiver cw
//	@param fileName
//	@param mimeType
//	@param etagHash
//	@param storePath
//	@param useLastModified
//	@param isAttachment
//	@return error
func (cw *GinWrap) AddFileHeader(fileName string, mimeType string,
	etagHash string, storePath string, useLastModified bool, isAttachment bool) error {
	return httpc.AddFileHeader(cw.Gin, fileName, mimeType, etagHash, storePath, useLastModified, isAttachment)
}

// AddCustomFileHeader 加入下傳檔案相關的 Header
//
// Content-Type,Content-Disposition,Content-Length
// fileSize 大於 0 才會加入 Content-Length
// 依 lastModified 加入 Last-Modified
// 如果 etagHash 不為空字串 加入 Etag
// 如果 isAttachment 為 true，Content-Disposition 會加入 attachment
//
//	@receiver cw
//	@param fileName
//	@param mimeType
//	@param etagHash
//	@param fileSize
//	@param lastModified
//	@param isAttachment
func (cw *GinWrap) AddCustomFileHeader(fileName string, mimeType string,
	etagHash string, fileSize int64, lastModified time.Time, isAttachment bool) {
	httpc.AddCustomFileHeader(cw.Gin, fileName, mimeType, etagHash,
		fileSize, lastModified, isAttachment)
}

// AddDisableCacheHeader 加入 Cache-Control
//
// Cache-Control: private, no-cache, no-store, must-revalidate
// Pragma: no-cache
// Expires: 0
//
//	@receiver cw
func (cw *GinWrap) AddDisableCacheHeader() {
	httpc.AddDisableCacheHeader(cw.Gin)
}

// AddNoCacheHeader AddNoStoreHeader 加入 Cache-Control，每次 request 檢查檔案
//
// Cache-Control: public, no-cache
//
//	@receiver cw
func (cw *GinWrap) AddNoCacheHeader() {
	httpc.AddNoCacheHeader(cw.Gin)
}

// ShouldBind 同 gin.Context 中的 ShouldBind
//
//	@receiver cw
//	@param obj
func (cw *GinWrap) ShouldBind(obj interface{}) error {
	return cw.Gin.ShouldBind(obj)
}

// MustShouldBind 同 gin.Context 中的 ShouldBind 不同的是有錯誤會直接使用 panic
//
//	@receiver cw
//	@param obj
func (cw *GinWrap) MustShouldBind(obj interface{}) {
	httpc.MustShouldBind(cw.Gin, obj)
}

// ShouldBindAndConvert 同 gin.Context 中的 ShouldBind ，有錯誤會直接使用 ConvertValidationErrorToJSON
//
//	@receiver cw
//	@param obj
//	@param json
//	@return bool
func (cw *GinWrap) ShouldBindAndConvert(obj interface{}, json *httpc.JSONResult) bool {
	return httpc.ShouldBindAndConvert(cw.Gin, obj, json)
}

// SendFileNoStore 傳送檔案加入 no Store Header
//
// 回傳 true 表示有 send file , false 表示檔案不存在不做任何處理
//
//	@receiver cw
//	@param fileName
//	@param mimeType
//	@param filePath
//	@param isAttachment
//	@return bool
func (cw *GinWrap) SendFileNoStore(fileName string, mimeType string,
	filePath string, isAttachment bool) bool {
	return httpc.SendFileNoStore(cw.Gin, fileName, mimeType,
		filePath, isAttachment)
}

// SendFile 傳送檔案依設定使用 Etag 及 LastModified Header
//
// 如果 maxAge 是負數的話，會加入 no-cache header
// 回傳 true 表示有 send file or 304 , false 表示檔案不存在不做任何處理
//
//	@receiver cw
//	@param fileName
//	@param mimeType
//	@param hash
//	@param filePath
//	@param maxAge
//	@param useEtag
//	@param useLastModified
//	@param isAttachment
//	@return bool
func (cw *GinWrap) SendFile(fileName string, mimeType string,
	hash string, filePath string, maxAge int, useEtag bool,
	useLastModified bool, isAttachment bool) bool {
	return httpc.SendFile(cw.Gin, fileName, mimeType, hash, filePath,
		maxAge, useEtag, useLastModified, isAttachment)
}

func (cw *GinWrap) Redirect(code int, path string) {
	cw.Gin.Redirect(code, path)
}

// TemporaryRedirect 307
//
//	@receiver cw
//	@param path
func (cw *GinWrap) TemporaryRedirect(path string) {
	cw.Gin.Redirect(http.StatusTemporaryRedirect, path)
}

// PermanentRedirect 308
//
//	@receiver cw
//	@param path
func (cw *GinWrap) PermanentRedirect(path string) {
	cw.Gin.Redirect(http.StatusPermanentRedirect, path)
}

func (cw *GinWrap) AbortWithStatus(code int) {
	cw.Gin.AbortWithStatus(code)
}

func (cw *GinWrap) AbortWith404() {
	cw.Gin.AbortWithStatus(http.StatusNotFound)
}

func (cw *GinWrap) AbortWith500() {
	cw.Gin.AbortWithStatus(http.StatusInternalServerError)
}

func (cw *GinWrap) StatusOk() {
	cw.Gin.Status(http.StatusOK)
}

func (cw *GinWrap) HTML(code int, name string, obj interface{}) {
	cw.Gin.HTML(code, name, obj)
}

func (cw *GinWrap) HtmlOk(name string, obj interface{}) {
	cw.Gin.HTML(http.StatusOK, name, obj)
}

func (cw *GinWrap) Html404(name string, obj interface{}) {
	cw.Gin.HTML(http.StatusNotFound, name, obj)
}

func (cw *GinWrap) Html500(name string, obj interface{}) {
	cw.Gin.HTML(http.StatusInternalServerError, name, obj)
}

// PostFormString 從 POST 取得 string (並 trim 空白)
//
//	@receiver cw
//	@param name
//	@return model.Param 如果為空的 string , Valid 也會是 false
func (cw *GinWrap) PostFormString(name string) model.Param[string] {
	val := strings.Trim(cw.Gin.PostForm(name), " ")
	return model.Param[string]{Valid: len(val) != 0, Value: val}
}

// PostFormInt 從 POST 取得 int
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) PostFormInt(name string) model.Param[int] {
	val, err := strconv.Atoi(cw.Gin.PostForm(name))
	return model.Param[int]{Valid: err == nil, Value: val}
}

// PostFormInt64 從 POST 取得 int64
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) PostFormInt64(name string) model.Param[int64] {
	val, err := strconv.ParseInt(cw.Gin.PostForm(name), 10, 64)
	return model.Param[int64]{Valid: err == nil, Value: val}
}

// PostFormBool 從 POST 取得 bool
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) PostFormBool(name string) model.Param[bool] {
	val := cw.Gin.PostForm(name)

	if len(val) == 0 {
		return model.Param[bool]{Valid: false, Value: false}
	}

	bVal, err := strconv.ParseBool(val)

	if err != nil {
		return model.Param[bool]{Valid: false, Value: false}
	}

	return model.Param[bool]{Valid: bVal, Value: true}
}

// PostFormFloat 從 POST 的 Query 取得 float64
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) PostFormFloat(name string) model.Param[float64] {
	val, err := strconv.ParseFloat(cw.Gin.PostForm(name), 64)
	return model.Param[float64]{Valid: err == nil, Value: val}
}

// PostFormDate 從 POST 取得 Time (格式為 2006-01-02)
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) PostFormDate(name string) model.Param[time.Time] {
	v, ok := convDate(cw.Gin.PostForm(name))
	return model.Param[time.Time]{Valid: ok, Value: v}
}

// PostFormInt64Array 從 POST 中的資料轉換為 []int64
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) PostFormInt64Array(name string) model.Param[[]int64] {
	strArr := cw.Gin.PostFormArray(name)
	strLen := len(strArr)

	if strLen == 0 {
		return model.Param[[]int64]{Valid: false}
	}

	intArr := make([]int64, strLen)

	for i := 0; i < strLen; i++ {
		num, err := strconv.ParseInt(strArr[i], 10, 64)

		if err == nil {
			intArr[i] = num
		} else {
			return model.Param[[]int64]{Valid: false}
		}
	}

	return model.Param[[]int64]{Valid: true, Value: intArr}
}

// PostFormIntArray 從 POST 中的資料轉換為 []int
//
//	@receiver cw
//	@param name
//	@return model.Param 轉換錯誤的話 , Valid 會是 false
func (cw *GinWrap) PostFormIntArray(name string) model.Param[[]int] {
	strArr := cw.Gin.PostFormArray(name)
	strLen := len(strArr)

	if strLen == 0 {
		return model.Param[[]int]{Valid: false}
	}

	intArr := make([]int, strLen)

	for i := 0; i < strLen; i++ {
		num, err := strconv.Atoi(strArr[i])

		if err == nil {
			intArr[i] = num
		} else {
			return model.Param[[]int]{Valid: false, Value: intArr}
		}
	}

	return model.Param[[]int]{Valid: true, Value: intArr}
}

// PostFormStringArray 從 POST 中的資料轉換為 []string
//
//	@receiver cw
//	@param name
//	@return model.Param len = 0 的話 , Valid 會是 false
func (cw *GinWrap) PostFormStringArray(name string) model.Param[[]string] {
	strArr := cw.Gin.PostFormArray(name)
	strLen := len(strArr)

	if strLen == 0 {
		return model.Param[[]string]{Valid: false}
	}

	return model.Param[[]string]{Valid: true, Value: strArr}
}

// PageInfoParam 產生換頁資訊。
//
//	@receiver cw
//	@param pageName url param 的名稱
//	@param pageSize 頁面大小
//	@param total 已知的總筆數
//	@return httpc.PageInfo Current 和 MaxPage 是以 1 為最小值
func (cw *GinWrap) PageInfoParam(pageName string, pageSize int64, total int64) httpc.PageInfo {
	var page int64
	pageParam := cw.Int64Param(pageName)

	if pageParam.Valid {
		page = pageParam.Value
	}

	return httpc.GeneratePageInfo(page, pageSize, total, true)
}

// PageInfoQuery 產生換頁資訊。
//
//	@receiver cw
//	@param pageName Get Query 的名稱
//	@param pageSize 頁面大小
//	@param total 已知的總筆數
//	@return httpc.PageInfo Current 和 MaxPage 是以 1 為最小值
func (cw *GinWrap) PageInfoQuery(pageName string, pageSize int64, total int64) httpc.PageInfo {
	var page int64
	pageParam := cw.Int64Query(pageName)

	if pageParam.Valid {
		page = pageParam.Value
	}

	return httpc.GeneratePageInfo(page, pageSize, total, true)
}
