package message

import (
	"sync"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc"
	"github.com/gorilla/websocket"
	"golang.org/x/exp/constraints"
)

var _ MsgPlugin[int, any] = &LocalMsgPlugin[int, any]{}

type LocalMsgPlugin[K constraints.Integer | string, T any] struct {
	state  cbc.PluginState
	sender *WebSocketSender[K, T]
}

func (p *LocalMsgPlugin[K, T]) GetName() string {
	return MsgPluginName
}

func (p *LocalMsgPlugin[K, T]) GetState() cbc.PluginState {
	return p.state
}

func (p *LocalMsgPlugin[K, T]) Init(context *cbc.Context, cloader cbc.ConfigLoader, logger *cbc.Logger) {
	if p.state != cbc.Enable {
		p.sender = NewWebSocketSender[K, T](context.Socket, context.Logger())
		p.state = cbc.Enable
	}
}

func (p *LocalMsgPlugin[K, T]) Close() {
	if p.state == cbc.Enable {
		p.state = cbc.Disable
	}
}

func (p *LocalMsgPlugin[K, T]) WriteToClient(accountID K, wsData T) {
	p.sender.WriteTo(accountID, wsData)
}

func (p *LocalMsgPlugin[K, T]) WriteToClients(accountID []K, wsData T) {
	for _, v := range accountID {
		p.WriteToClient(v, wsData)
	}
}

func (p *LocalMsgPlugin[K, T]) WriteToAllClient(wsData T) {
	p.sender.WriteToAll(wsData)
}

// PublishJson 透過 stomp 發佈 JSON
func (p *LocalMsgPlugin[K, T]) PublishJson(msg T) {
	p.sender.WriteToAll(msg)
}

// PublishJsonTo 透過 stomp 發佈 JSON
func (p *LocalMsgPlugin[K, T]) PublishJsonTo(UserIdArr []K, msg T) {
	p.sender.WriteToSome(UserIdArr, msg)
}

// RegisterClient 註冊一個 account 的 socket 到 client 列表中
func (p *LocalMsgPlugin[K, T]) RegisterClient(accountID K, socket *websocket.Conn, wlock *sync.Mutex) {
	p.sender.RegisterClient(accountID, socket, wlock)
}

// UnregisterClient 解除註冊一個 account 的 socket 到 client 列表中
func (p *LocalMsgPlugin[K, T]) UnregisterClient(accountID K, socket *websocket.Conn) {
	p.sender.UnregisterClient(accountID, socket)
}
