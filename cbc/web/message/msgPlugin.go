package message

import (
	"sync"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc"
	"github.com/gorilla/websocket"
	"golang.org/x/exp/constraints"
)

type MsgPlugin[K constraints.Integer | string, T any] interface {
	cbc.ThirdPlugin
	cbc.FullPlugin
	WriteToClient(accountID K, wsData T)
	WriteToClients(accountID []K, wsData T)
	WriteToAllClient(wsData T)
	// PublishJson 發佈 JSON
	PublishJson(msg T)
	// PublishJsonTo 發佈 JSON
	PublishJsonTo(UserIdArr []K, msg T)
	// RegisterClient 註冊一個 account 的 socket 到 client 列表中
	RegisterClient(accountID K, socket *websocket.Conn, wlock *sync.Mutex)
	// UnregisterClient 解除註冊一個 account 的 socket 到 client 列表中
	UnregisterClient(accountID K, socket *websocket.Conn)
}
