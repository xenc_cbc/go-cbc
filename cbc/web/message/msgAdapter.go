package message

import (
	"encoding/json"

	"go.uber.org/zap"
)

// MsgAdapter 不區分 user 直接轉送
type MsgAdapter struct {
	sender    []Sender[any]
	receiver  Receiver
	clientLog *zap.Logger
}

func NewAdpter[T any](s []Sender[any], r Receiver, log *zap.Logger) *MsgAdapter {
	nd := &MsgAdapter{
		sender:    s,
		receiver:  r,
		clientLog: log.With(zap.String("source", "Msg Adapter")),
	}

	return nd
}

func (nd *MsgAdapter) doFun(msg []byte, isJsonType bool) {
	var jsonData interface{}
	jsonErr := json.Unmarshal(msg, &jsonData)

	if jsonErr == nil {
		for i := 0; i < len(nd.sender); i++ {
			go func(s Sender[any]) {
				s.Write(jsonData)
			}(nd.sender[i])
		}
	} else {
		nd.clientLog.Error("json unmarshal error", zap.Error(jsonErr))
	}
}

func (nd *MsgAdapter) Subscribe() {
	nd.receiver.Subscribe(nd.doFun)
}

func (nd *MsgAdapter) UnSubscribe() {
	nd.receiver.UnSubscribe()
}
