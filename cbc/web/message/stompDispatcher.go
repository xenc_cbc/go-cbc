package message

import (
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc"
	"github.com/go-stomp/stomp"
	"go.uber.org/zap"
	"golang.org/x/exp/constraints"
)

// StompDispatcher 訂閱 stomp，收到後用指定的 UserSender 傳出去
type StompDispatcher[K constraints.Integer | string] struct {
	dispatcher  *MsgDispatcher[K]
	stompSender *StompSender[any]
}

func NewStompDispatcher[K constraints.Integer | string](stompName string, stompEntity *cbc.Stomp,
	stCon *stomp.Conn, log *zap.Logger, senders ...UserSender[K, any]) *StompDispatcher[K] {
	return &StompDispatcher[K]{
		dispatcher: NewDispatcher(senders,
			NewStompReceiver(stompName, stompEntity, stCon, log),
			log),
		stompSender: NewStompSender[any](stompName, stompEntity, stCon, log),
	}
}

// PublishString 透過 stomp 發佈文字
func (d *StompDispatcher[K]) PublishString(msg string) {
	d.stompSender.PublishString(msg)
}

// PublishJSON 透過 stomp 發佈 JSON
func (d *StompDispatcher[K]) PublishJSON(msg interface{}) {
	d.stompSender.PublishJSON(msg)
}

// Subscribe
func (d *StompDispatcher[K]) Subscribe() {
	d.dispatcher.Subscribe()
}

// UnSubscribe
func (d *StompDispatcher[K]) UnSubscribe() {
	d.dispatcher.UnSubscribe()
}
