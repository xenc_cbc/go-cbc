package message

import (
	"sync"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc"
	"github.com/gorilla/websocket"
	"golang.org/x/exp/constraints"
)

var _ MsgPlugin[int, any] = &StompMsgPlugin[int]{}

type StompMsgPlugin[K constraints.Integer | string] struct {
	StompName  string
	state      cbc.PluginState
	sender     *WebSocketSender[K, any]
	dispatcher *StompDispatcher[K]
}

func (p *StompMsgPlugin[K]) GetName() string {
	return MsgPluginName
}

func (p *StompMsgPlugin[K]) GetState() cbc.PluginState {
	return p.state
}

func (p *StompMsgPlugin[K]) Init(context *cbc.Context, cloader cbc.ConfigLoader, logger *cbc.Logger) {
	if p.state != cbc.Enable {
		p.sender = NewWebSocketSender[K, any](context.Socket, context.Logger())
		p.dispatcher = NewStompDispatcher[K](p.StompName, context.Stomp,
			context.Stomp.Con(), context.Logger(), p.sender)
		p.dispatcher.Subscribe()
		p.state = cbc.Enable
	}
}

func (p *StompMsgPlugin[K]) Close() {
	if p.state == cbc.Enable {
		p.dispatcher.UnSubscribe()
		p.state = cbc.Disable
	}
}

func (p *StompMsgPlugin[K]) WriteToClient(accountID K, wsData interface{}) {
	p.sender.WriteTo(accountID, wsData)
}

func (p *StompMsgPlugin[K]) WriteToClients(accountID []K, wsData interface{}) {
	for _, v := range accountID {
		p.WriteToClient(v, wsData)
	}
}

func (p *StompMsgPlugin[K]) WriteToAllClient(wsData interface{}) {
	p.sender.WriteToAll(wsData)
}

// PublishJson 透過 stomp 發佈 JSON
func (p *StompMsgPlugin[K]) PublishJson(msg interface{}) {
	p.dispatcher.PublishJSON(UserStompData[K]{Data: msg})
}

// PublishJsonTo 透過 stomp 發佈 JSON
func (p *StompMsgPlugin[K]) PublishJsonTo(UserIdArr []K, msg interface{}) {
	p.dispatcher.PublishJSON(UserStompData[K]{Target: UserIdArr, Data: msg})
}

// RegisterClient 註冊一個 account 的 socket 到 client 列表中
func (p *StompMsgPlugin[K]) RegisterClient(accountID K, socket *websocket.Conn, wlock *sync.Mutex) {
	p.sender.RegisterClient(accountID, socket, wlock)
}

// UnregisterClient 解除註冊一個 account 的 socket 到 client 列表中
func (p *StompMsgPlugin[K]) UnregisterClient(accountID K, socket *websocket.Conn) {
	p.sender.UnregisterClient(accountID, socket)
}
