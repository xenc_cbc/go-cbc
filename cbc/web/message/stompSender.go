package message

import (
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc"
	"github.com/go-stomp/stomp"
	"go.uber.org/zap"
)

var _ Sender[string] = &StompSender[string]{}

// StompData JSON 使用
type StompData struct {
	Target []int
	Data   interface{}
}

type StompSender[T any] struct {
	topicName   string
	clientLog   *zap.Logger
	stompEntity *cbc.Stomp
	stompCon    *stomp.Conn
}

func NewStompSender[T any](stTopicName string,
	stompEntity *cbc.Stomp, stCon *stomp.Conn, log *zap.Logger) *StompSender[T] {
	md := &StompSender[T]{}
	md.stompEntity = stompEntity
	md.stompCon = stCon
	md.topicName = stTopicName
	md.clientLog = log.With(zap.String("source", "stomp sender["+stTopicName+"]"))
	return md
}

func (md *StompSender[T]) Write(data T) {
	md.PublishJSON(data)
}

// PublishString 透過 stomp 發佈文字
func (md *StompSender[T]) PublishString(msg string) {
	err := md.stompEntity.SendTextWithCon(md.stompCon, md.topicName, msg)

	if err == nil {
		md.clientLog.Debug("Publish String", zap.String("msg", msg))
	} else {
		md.clientLog.Warn("Publish String", zap.String("msg", msg))
	}
}

// PublishJSON 透過 stomp 發佈 JSON
func (md *StompSender[T]) PublishJSON(msg T) {
	err := md.stompEntity.SendJSONWithCon(md.stompCon, md.topicName, msg)

	if err != nil {
		md.clientLog.Warn("Publish JSON Failed", zap.Error(err))
	}
}
