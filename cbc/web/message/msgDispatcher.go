package message

import (
	"encoding/json"

	"go.uber.org/zap"
	"golang.org/x/exp/constraints"
)

// MsgDispatcher 可指定 user id 去轉送 (要來源寫入時就使用 UserStompData)
type MsgDispatcher[K constraints.Integer | string] struct {
	sender    []UserSender[K, any]
	receiver  Receiver
	clientLog *zap.Logger
}

func NewDispatcher[K constraints.Integer | string](s []UserSender[K, any],
	r Receiver, log *zap.Logger) *MsgDispatcher[K] {
	nd := &MsgDispatcher[K]{
		sender:    s,
		receiver:  r,
		clientLog: log.With(zap.String("source", "Msg Dispatcher")),
	}

	return nd
}

func (nd *MsgDispatcher[K]) doParse(msg []byte, isJsonType bool) {
	if isJsonType {
		var stompData UserStompData[K]
		jsonErr := json.Unmarshal(msg, &stompData)
		targetCount := len(stompData.Target)

		if jsonErr == nil {
			for i := 0; i < len(nd.sender); i++ {
				go func(s UserSender[K, any]) {
					if targetCount == 0 {
						s.WriteToAll(stompData.Data)
					} else if targetCount == 1 {
						s.WriteTo(stompData.Target[0], stompData.Data)
					} else {
						s.WriteToSome(stompData.Target, stompData.Data)
					}
				}(nd.sender[i])
			}

			return
		}
	}

	var jsonData interface{}
	jsonErr := json.Unmarshal(msg, &jsonData)

	if jsonErr == nil {
		for i := 0; i < len(nd.sender); i++ {
			go func(s UserSender[K, any]) {
				s.WriteToAll(jsonData)
			}(nd.sender[i])
		}
	} else {
		nd.clientLog.Error("json unmarshal error", zap.Error(jsonErr))
	}
}

func (nd *MsgDispatcher[K]) Subscribe() {
	nd.receiver.Subscribe(nd.doParse)
}

func (nd *MsgDispatcher[K]) UnSubscribe() {
	nd.receiver.UnSubscribe()
}
