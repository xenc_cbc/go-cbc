package message

import (
	"sync"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc"
	"github.com/go-stomp/stomp"
	"go.uber.org/zap"
)

var _ Receiver = &StompReceiver{}

type StompReceiver struct {
	topicName       string
	lock            sync.RWMutex
	stSubscription  *stomp.Subscription
	clientLog       *zap.Logger
	clientConnected bool
	clientStopChain chan bool
	stompEntity     *cbc.Stomp
	stompCon        *stomp.Conn
}

func NewStompReceiver(stTopicName string,
	stompEntity *cbc.Stomp, stCon *stomp.Conn, log *zap.Logger) *StompReceiver {
	md := &StompReceiver{}
	md.stompEntity = stompEntity
	md.stompCon = stCon
	md.topicName = stTopicName
	md.clientLog = log.With(zap.String("source", "stomp receiver["+stTopicName+"]"))
	return md
}

// Subscribe 初始化及訂閱 Stomp 的更新
func (md *StompReceiver) Subscribe(doFun func([]byte, bool)) {
	md.lock.Lock()
	defer md.lock.Unlock()

	if !md.clientConnected {
		var err error
		md.stSubscription, err = md.stompCon.Subscribe(md.topicName, stomp.AckAuto)

		if err != nil {
			md.clientLog.Error("Stomp Subscribe Failed", zap.Error(err))
		} else {
			md.clientLog.Info("Stomp Subscribe OK")
			md.clientConnected = true
			md.clientStopChain = make(chan bool)

			go func() {
			endLoop:
				for {
					select {
					case <-md.clientStopChain:
						break endLoop
					case msg, chOk := <-md.stSubscription.C:
						if chOk {
							contentType := msg.ContentType
							isJsonType := contentType == "application/json"
							body := msg.Body
							msg.Conn.Ack(msg)
							doFun(body, isJsonType)
						} else {
							break endLoop
						}
					}
				}
			}()
		}
	}
}

// UnSubscribe 取消 Stomp 的訂閱
func (md *StompReceiver) UnSubscribe() {
	md.lock.Lock()
	defer md.lock.Unlock()

	if md.clientConnected {
		md.clientStopChain <- true
		close(md.clientStopChain)
		err := md.stSubscription.Unsubscribe()

		if err != nil {
			md.clientLog.Error("Stomp Unsubscribe Failed", zap.Error(err))
		} else {
			md.clientLog.Info("Stomp Unsubscribe OK")
		}

		md.clientConnected = false
	}
}
