package message

type Receiver interface {
	Subscribe(doFun func([]byte, bool))
	UnSubscribe()
}
