package message

import (
	"sync"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/arrayc"
	"github.com/gorilla/websocket"
	"go.uber.org/zap"
	"golang.org/x/exp/constraints"
)

var _ UserSender[int, string] = &WebSocketSender[int, string]{}

type WebSocketSender[K constraints.Integer | string, T any] struct {
	clientMap map[K]*Client[K]
	lock      *sync.RWMutex
	clientLog *zap.Logger
	socket    *cbc.WebSocket
}

func NewWebSocketSender[K constraints.Integer | string, T any](socket *cbc.WebSocket, log *zap.Logger) *WebSocketSender[K, T] {
	md := &WebSocketSender[K, T]{}
	md.socket = socket
	md.lock = &sync.RWMutex{}
	md.clientMap = make(map[K]*Client[K])
	md.clientLog = log.With(zap.String("source", "websocket sender"))
	return md
}

func (md *WebSocketSender[K, T]) Write(data T) {
	md.WriteToAll(data)
}

func (md *WebSocketSender[K, T]) WriteTo(accountID K, wsData T) {
	var invalidSockets []*ClientSocket = make([]*ClientSocket, 0)

	md.lock.RLocker().Lock()
	defer func() {
		md.lock.RLocker().Unlock()

		if len(invalidSockets) != 0 {
			md.clearInvalidSockets(accountID, invalidSockets)
		}
	}()

	c, ok := md.clientMap[accountID]

	if ok {
		for _, sc := range c.Sockets {
			err := sc.WriteJSON(md.socket, wsData)

			if err != nil {
				md.clientLog.Warn("WsData Write Error.", zap.Error(err), zap.Any("Account ID", accountID))
				invalidSockets = append(invalidSockets, sc)
			}
		}
	} else {
		md.clientLog.Debug("Client Offline", zap.Any("Account ID", accountID))
	}
}

func (md *WebSocketSender[K, T]) WriteToSome(accountID []K, wsData T) {
	for _, v := range accountID {
		md.WriteTo(v, wsData)
	}
}

func (md *WebSocketSender[K, T]) WriteToAll(wsData T) {
	var invalidSockets []*InvalidClient[K] = make([]*InvalidClient[K], 0)

	md.lock.RLocker().Lock()
	defer func() {
		md.lock.RLocker().Unlock()

		if len(invalidSockets) != 0 {
			for _, v := range invalidSockets {
				md.clearInvalidSocket(v.AccountID, v.Socket)
			}
		}
	}()

	for _, c := range md.clientMap {
		for _, sc := range c.Sockets {
			err := sc.WriteJSON(md.socket, wsData)

			if err != nil {
				md.clientLog.Warn("WsData Write Error.", zap.Error(err), zap.Any("Account ID", c.AccountID))
				invalidClient := &InvalidClient[K]{
					AccountID: c.AccountID,
					Socket:    sc,
				}
				invalidSockets = append(invalidSockets, invalidClient)
			}
		}
	}
}

// clearInvalidSockets 清除多個的 ClientSocket
func (md *WebSocketSender[K, T]) clearInvalidSockets(accountID K, sockets []*ClientSocket) {
	md.lock.Lock()
	defer md.lock.Unlock()

	c, ok := md.clientMap[accountID]

	if ok {
		var newSockets []*ClientSocket = make([]*ClientSocket, 0)
		socketLen := len(c.Sockets)
		invalidSocketCount := len(sockets)
		hitSocketCount := 0

		md.clientLog.Info("Unregister Invalid Sockets.", zap.Any("Account ID", accountID), zap.Int("InvalidSocketCount", invalidSocketCount))

		for i := 0; i < socketLen; i++ {
			var hit bool

			for _, sc := range sockets {
				if c.Sockets[i] == sc {
					hit = true
					break
				}
			}

			if hit {
				hitSocketCount++

				if hitSocketCount == invalidSocketCount {
					newSockets = append(newSockets, c.Sockets[i+1:]...)
					break
				}
			} else {
				newSockets = append(newSockets, c.Sockets[i])
			}
		}

		newLen := len(newSockets)

		if newLen == 0 {
			delete(md.clientMap, accountID)
		} else {
			c.Sockets = newSockets
		}

		md.clientLog.Debug("Clear Invalid Socket OK.", zap.Any("AccountID", accountID), zap.Int("validCount", newLen))
	}
}

// clearInvalidSocket 清除單一的 ClientSocket
func (md *WebSocketSender[K, T]) clearInvalidSocket(accountID K, sockets *ClientSocket) {
	md.lock.Lock()
	defer md.lock.Unlock()

	c, ok := md.clientMap[accountID]

	if ok {
		md.clientLog.Info("Unregister Invalid Sockets.", zap.Any("Account ID", accountID))
		var newSockets []*ClientSocket = make([]*ClientSocket, 0)
		socketLen := len(c.Sockets)

		for i := 0; i < socketLen; i++ {
			if c.Sockets[i] == sockets {
				newSockets = append(newSockets, c.Sockets[i+1:]...)
				break
			} else {
				newSockets = append(newSockets, c.Sockets[i])
			}
		}

		newLen := len(newSockets)

		if newLen == 0 {
			delete(md.clientMap, accountID)
		} else {
			c.Sockets = newSockets
		}

		md.clientLog.Debug("Clear Invalid Socket OK.", zap.Any("AccountID", accountID), zap.Int("validCount", newLen))
	}
}

// RegisterClient 註冊一個 account 的 socket 到 client 列表中
func (md *WebSocketSender[K, T]) RegisterClient(accountID K, socket *websocket.Conn, wlock *sync.Mutex) {
	md.lock.Lock()
	defer md.lock.Unlock()

	c, ok := md.clientMap[accountID]

	if !ok {
		c = &Client[K]{AccountID: accountID, Sockets: make([]*ClientSocket, 0)}
		md.clientMap[accountID] = c
	}

	c.Sockets = append(c.Sockets, &ClientSocket{WriteLock: wlock, Socket: md.socket.WrapWithConnection(socket)})
	md.clientLog.Debug("Register.", zap.Any("AccountID", accountID), zap.Int("Count", len(c.Sockets)))
}

// UnregisterClient 解除註冊一個 account 的 socket 到 client 列表中
func (md *WebSocketSender[K, T]) UnregisterClient(accountID K, socket *websocket.Conn) {
	md.lock.Lock()
	defer md.lock.Unlock()

	c, ok := md.clientMap[accountID]

	if ok {
		socketLen := len(c.Sockets)

		if socketLen == 1 {
			if c.Sockets[0].Socket.GetConnection() != socket {
				md.clientLog.Warn("Unregister Target Not Found.", zap.Any("AccountID", accountID))
				return
			}

			delete(md.clientMap, accountID)
			md.clientLog.Debug("Unregister Target OK.", zap.Any("AccountID", accountID))
		} else {
			var (
				hit bool
				i   int
			)

			for ; i < socketLen; i++ {
				if c.Sockets[i].Socket.GetConnection() == socket {
					hit = true
					break
				}
			}

			if hit {
				c.Sockets = arrayc.DeleteWithIndex(c.Sockets, i)
				md.clientLog.Debug("Unregister Target OK.", zap.Any("AccountID", accountID),
					zap.Int("Remove Index", i), zap.Int("LastCount", socketLen-1))
			} else {
				md.clientLog.Warn("Unregister Target Not Found.", zap.Any("AccountID", accountID))
			}
		}
	}
}
