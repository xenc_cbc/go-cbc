package message

import (
	"sync"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc"
	"golang.org/x/exp/constraints"
)

const MsgPluginName = "MsgBroadcast"

// Client MessageDispatcher 的 Client
type Client[K constraints.Integer | string] struct {
	AccountID K
	Sockets   []*ClientSocket
}

// InvalidClient 無效的 Client
type InvalidClient[K constraints.Integer | string] struct {
	AccountID K
	Socket    *ClientSocket
}

// ClientSocket 寫入用的 socket
type ClientSocket struct {
	WriteLock *sync.Mutex
	Socket    *cbc.WebSocketWrap
}

func (cs *ClientSocket) WriteJSON(socket *cbc.WebSocket, v interface{}) error {
	cs.WriteLock.Lock()
	cs.Socket.NextWriteDeadline()
	err := cs.Socket.WriteJSON(v)
	cs.WriteLock.Unlock()
	return err
}

// UserStompData MsgDispatcher 使用
type UserStompData[K constraints.Integer | string] struct {
	Target []K
	Data   interface{}
}
