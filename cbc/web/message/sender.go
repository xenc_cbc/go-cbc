package message

import "golang.org/x/exp/constraints"

type Sender[T any] interface {
	Write(data T)
}

type UserSender[K constraints.Integer | string, T any] interface {
	Sender[T]
	WriteTo(accountID K, data T)
	WriteToSome(accountID []K, data T)
	WriteToAll(data T)
}
