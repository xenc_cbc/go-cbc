package web

import (
	"fmt"
	"strings"
	"sync"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/model"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/web/oauth"
	"github.com/gorilla/websocket"
	"go.uber.org/zap"
)

var (
	RouteArr []Route
)

// Route 初始化 route
type Route interface {
	Init(ctxt *WebContext, instance interface{})
	Setup(routes *Routes)
}

// Context 自定義 Context
type WebContext struct {
	*cbc.Context
	HostURL          string
	OAuthRedirectURL string
	OAuth            OAuthConfig
	UserApi          model.UserApi
	RoleApi          model.RoleApi
	OAuthApi         oauth.OAuthApi
}

// OAuthConfig oauth 相關的設定
type OAuthConfig struct {
	Facebook         *oauth.FacebookConfig
	Line             *oauth.LineConfig
	Google           *oauth.GoogleConfig
	IsActiveLinkUser bool
	LinkUserMap      map[string]bool `json:"linkUserMap"`
	ExecutorMap      map[string]*oauth.OAuthExecutor
	DatabaseApi      oauth.OAuthApi
}

// WsData 泛用 websocket 使用格式
type WsData struct {
	Action string      `json:"action"`
	Data   interface{} `json:"data"`
}

// Client MessageDispatcher 的 Client
type Client struct {
	AccountID int
	Sockets   []*ClientSocket
}

// InvalidClient 無效的 Client
type InvalidClient struct {
	AccountID int
	Socket    *ClientSocket
}

// ClientSocket 寫入用的 socket
type ClientSocket struct {
	WriteLock *sync.Mutex
	Sockets   *websocket.Conn
}

// StompData JSON 使用
// @Deprecated: use message package
type StompData struct {
	Target []int
	Data   interface{}
}

// NewDefaultContext 建立一個新的 Default WebContext
//
//	@param configLoader
//	@return *WebContext
func NewDefaultContext(configLoader cbc.ConfigLoader) *WebContext {
	userApi := &model.PgUserApi{}
	roleApi := &model.PgRoleApi{}
	oauthApi := &model.PgOAuthApi{
		UserApi: userApi,
		RoleApi: roleApi,
	}
	return NewContext(configLoader, userApi, roleApi, oauthApi)
}

// NewContextWithoutOAuth 建立一個新的 Default WebContext 但不含 OAuth
//
//	@param configLoader
//	@return *WebContext
func NewContextWithoutOAuth(configLoader cbc.ConfigLoader) *WebContext {
	userApi := &model.PgUserApi{}
	roleApi := &model.PgRoleApi{}

	return NewContext(configLoader, userApi, roleApi, nil)
}

// NewContext 建立一個新的 WebContext
//
//	@param configLoader
//	@param userApi
//	@param roleApi
//	@param oauthApi
//	@return *WebContext
func NewContext(configLoader cbc.ConfigLoader,
	userApi model.UserApi, roleApi model.RoleApi, oauthApi oauth.OAuthApi) *WebContext {
	return &WebContext{
		Context: cbc.NewContext(configLoader),
		UserApi: userApi, RoleApi: roleApi, OAuthApi: oauthApi}
}

// Init 初始化
//
//	@receiver cxt
//	@return initResult
//	@return initError
func (cxt *WebContext) Init() (initResult bool, initError error) {
	if initRs, initRsErr := cxt.Context.Init(); initRs {
		defer func() {
			if r := recover(); r != nil {
				fmt.Println("WebContext init error")
				fmt.Println(r)
				initError = r.(error)
				cxt.CloseResource()
			}
		}()

		hostURL := cxt.Config.GetString("hostURL", "")

		if len(hostURL) == 0 {
			var protocol string
			host := cxt.ServerConfig().ServerName + ":" + cxt.ServerConfig().Port

			if cxt.ServerConfig().Ssl {
				protocol = "https://"
			} else {
				protocol = "http://"
			}

			hostURL = protocol + host
		}

		cxt.HostURL = hostURL
		loginURL := cxt.Config.GetString("oauthRedirectURL", "")

		if len(loginURL) == 0 {
			cxt.OAuthRedirectURL = cxt.ServerConfig().WebURL + "/oauth/login"
		} else {
			cxt.OAuthRedirectURL = cxt.ServerConfig().WebURL + loginURL
		}

		cxt.Logger().Info("HostURL", zap.String("URL", hostURL),
			zap.String("configKey", "hostURL"))
		cxt.Logger().Info("OAuthRedirectURL", zap.String("URL", cxt.OAuthRedirectURL),
			zap.String("configKey", "oauthRedirectURL"))

		if cxt.OAuthApi == nil {
			cxt.Logger().Info("Skip OAuth Init , Need DatabaseApi")
		} else {
			cxt.initOAuth()
		}

		initResult = true
	} else {
		initError = initRsErr
	}

	return
}

func (cxt *WebContext) initOAuth() {
	logger := cxt.Logger().With(zap.String("source", "oauth"))
	cxt.OAuth.DatabaseApi = cxt.OAuthApi
	cxt.OAuth.LinkUserMap = make(map[string]bool)
	cxt.OAuth.ExecutorMap = make(map[string]*oauth.OAuthExecutor)
	cxt.initFbOAuth("oauth.fb.json", cxt.OAuthApi, logger)
	cxt.initLineOAuth("oauth.line.json", cxt.OAuthApi, logger)
	cxt.initGoogleOAuth("oauth.google.json", cxt.OAuthApi, logger)
}

func (cxt *WebContext) RouteSetup(routes []Route) {
	r := RoutesWrap(cxt, cxt.Router())

	for _, rt := range routes {
		rt.Init(cxt, rt)
		rt.Setup(r)
	}
}

func (cxt *WebContext) getOAuthRedirectURL() string {
	return cxt.OAuthRedirectURL
}

func (cxt *WebContext) initFbOAuth(cfgFileName string,
	dbApi oauth.OAuthApi, log *zap.Logger) {
	jsonExist, jsonError := cxt.Loader.DecodeConfig(cfgFileName, &cxt.OAuth.Facebook)

	if !jsonExist {
		log.Info("FaceBook json not found , Disable.", zap.String("file", cfgFileName))
	} else if jsonError != nil {
		log.Info("FaceBook json error , Disable.", zap.String("file", cfgFileName))
		return
	}

	if cxt.OAuth.Facebook != nil && cxt.OAuth.Facebook.Enabled {
		scope := strings.Join(cxt.OAuth.Facebook.Scopes, ",")
		roles := strings.Join(cxt.OAuth.Facebook.DefaultRoles, ",")

		if len(cxt.OAuth.Facebook.RedirectURL) == 0 {
			cxt.OAuth.Facebook.RedirectURL = cxt.getOAuthRedirectURL()
		}

		log.Info("FaceBook OAuth",
			zap.String("ClientId", cxt.OAuth.Facebook.ClientID),
			zap.String("ClientSecret", cxt.OAuth.Facebook.ClientSecret),
			zap.String("scopes", scope),
			zap.String("redirectURL", cxt.OAuth.Facebook.RedirectURL),
			zap.Bool("emailCheck", cxt.OAuth.Facebook.EmailCheck),
			zap.String("defaultRoles", roles))
		cxt.afterInitOAuth(cxt.OAuth.Facebook, log, dbApi)
	} else {
		log.Info("FaceBook Disable.")
	}
}

func (cxt *WebContext) initLineOAuth(cfgFileName string,
	dbApi oauth.OAuthApi, log *zap.Logger) {
	jsonExist, jsonError := cxt.Loader.DecodeConfig(cfgFileName, &cxt.OAuth.Line)

	if !jsonExist {
		log.Info("Line json not found , Disable.", zap.String("file", cfgFileName))
	} else if jsonError != nil {
		log.Info("Line json error , Disable.", zap.String("file", cfgFileName))
		return
	}

	if cxt.OAuth.Line != nil && cxt.OAuth.Line.Enabled {
		scope := strings.Join(cxt.OAuth.Line.Scopes, ",")
		roles := strings.Join(cxt.OAuth.Line.DefaultRoles, ",")
		locales := strings.Join(cxt.OAuth.Line.Locales, ",")

		if len(cxt.OAuth.Line.RedirectURL) == 0 {
			cxt.OAuth.Line.RedirectURL = cxt.getOAuthRedirectURL()
		}

		log.Info("Line OAuth",
			zap.String("ClientId", cxt.OAuth.Line.ClientID),
			zap.String("ClientSecret", cxt.OAuth.Line.ClientSecret),
			zap.String("Nonce", cxt.OAuth.Line.Nonce),
			zap.String("Prompt", cxt.OAuth.Line.Prompt),
			zap.Int("MaxAge", cxt.OAuth.Line.MaxAge),
			zap.String("BotPrompt", cxt.OAuth.Line.BotPrompt),
			zap.String("Locales", locales),
			zap.String("scopes", scope),
			zap.String("redirectURL", cxt.OAuth.Line.RedirectURL),
			zap.Bool("emailCheck", cxt.OAuth.Line.EmailCheck),
			zap.String("defaultRoles", roles))
		cxt.afterInitOAuth(cxt.OAuth.Line, log, dbApi)
	} else {
		log.Info("Line Disable.")
	}
}

func (cxt *WebContext) initGoogleOAuth(cfgFileName string,
	dbApi oauth.OAuthApi, log *zap.Logger) {
	jsonExist, jsonError := cxt.Loader.DecodeConfig(cfgFileName, &cxt.OAuth.Google)

	if !jsonExist {
		log.Info("Google json not found , Disable.", zap.String("file", cfgFileName))
	} else if jsonError != nil {
		log.Info("Google json error , Disable.", zap.String("file", cfgFileName))
		return
	}

	if cxt.OAuth.Google != nil && cxt.OAuth.Google.Enabled {
		scope := strings.Join(cxt.OAuth.Google.Scopes, ",")
		roles := strings.Join(cxt.OAuth.Google.DefaultRoles, ",")

		if len(cxt.OAuth.Google.RedirectURL) == 0 {
			cxt.OAuth.Google.RedirectURL = cxt.getOAuthRedirectURL()
		}

		log.Info("Google OAuth",
			zap.String("ClientId", cxt.OAuth.Google.ClientID),
			zap.String("ClientSecret", cxt.OAuth.Google.ClientSecret),
			zap.String("scopes", scope),
			zap.String("accessType", cxt.OAuth.Google.AccessType),
			zap.Bool("includeGrantedScopes", cxt.OAuth.Google.IncludeGrantedScopes),
			zap.String("loginHint", cxt.OAuth.Google.LoginHint),
			zap.String("prompt", cxt.OAuth.Google.Prompt),
			zap.String("redirectURL", cxt.OAuth.Google.RedirectURL),
			zap.Bool("emailCheck", cxt.OAuth.Google.EmailCheck),
			zap.String("defaultRoles", roles))
		cxt.afterInitOAuth(cxt.OAuth.Google, log, dbApi)
	} else {
		log.Info("Google Disable.")
	}
}

func (cxt *WebContext) afterInitOAuth(executor oauth.OAuthExecutor,
	log *zap.Logger, dbApi oauth.OAuthApi) {
	executor.InitConfig(log.With(zap.String("oauthType", executor.GetPlatformCode())),
		dbApi)

	if executor.IsActiveLinkUser() {
		cxt.OAuth.IsActiveLinkUser = true
	}

	cxt.OAuth.LinkUserMap[executor.GetPlatformCode()] = executor.IsActiveLinkUser()
	cxt.OAuth.ExecutorMap[executor.GetPlatformCode()] = &executor
}

// StartWeb 啟動 , 結束時會自動呼叫 CloseResource
// syscall.SIGINT, syscall.SIGTERM 可以觸發中斷
//
//	@receiver cxt
//	@param setupFun 在 Gin 開始之前要執行的設定，像是 Route
//	@param startupOk 是在執行完成時才執行
//	@param shutdownOk 則是在確定都停止完成後才執行
//	@param waitSec 關閉時要等待多久
//	@return err 有遇到錯誤的話會存放在這邊
func (cxt *WebContext) StartWeb(
	setupFun func(cxt *WebContext),
	startupOk func(),
	shutdownOk func(),
	waitSec int) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = r.(error)
		}

		cxt.CloseResource()
	}()

	setupFun(cxt)

	cxtStartupOkFun := func() {
		startupOk()
		cxt.DoPluginStartup()
	}

	cxt.Gin().Startup(cxtStartupOkFun, func() {
		if len(cxt.ShutdownFuncArr) > 0 {
			for _, f := range cxt.ShutdownFuncArr {
				f()
			}
		}

		cxt.DoPluginShutdown()

		if shutdownOk != nil {
			shutdownOk()
		}
	}, waitSec)

	return
}

// ShutdownWeb 停止 StartWeb 的執行
func (cxt *WebContext) ShutdownWeb() {
	cxt.Gin().Shutdown()
}
