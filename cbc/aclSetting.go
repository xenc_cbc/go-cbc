package cbc

// ACLSetting 設定檔
type ACLSetting struct {
	Path    string          `json:"path"`
	Type    ACLType         `json:"type"`
	Roles   []string        `json:"roles"`
	IsJSON  bool            `json:"isJson"`
	JSONKey string          `json:"jsonKey"`
	Methods []ACLMethodType `json:"methods"`
}
