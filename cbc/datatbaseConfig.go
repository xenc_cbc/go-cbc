package cbc

// DatatbaseConfig 資料庫設定
type DatatbaseConfig struct {
	Type          string   `json:"type"`
	ConStr        string   `json:"conStr"`
	ConTestStr    string   `json:"conTestStr"`
	MaxOpen       int      `json:"maxOpen"`
	MaxIdle       int      `json:"maxIdle"`
	MaxLife       int      `json:"maxLife"`
	OtherDatabase []string `json:"otherDatabase"`
}
