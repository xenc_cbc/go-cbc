package cbc

// WebSocketConfig 設定檔
type WebSocketConfig struct {
	CheckOrigin     bool  `json:"checkOrigin"`
	ReadBufferSize  int   `json:"readBufferSize"`
	WriteBufferSize int   `json:"writeBufferSize"`
	WriteWait       int64 `json:"writeWait"`
	PongWait        int64 `json:"pongWait"`
	PingPeriod      int64 `json:"pingPeriod"`
	MaxMessageSize  int64 `json:"maxMessageSize"`
	ShowPingLog     bool  `json:"showPingLog"`
}
