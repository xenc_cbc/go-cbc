package datec

import "time"

// newTime 用來判別是否為初始值
var newTime time.Time = time.Time{}

// AddYear 加年
func AddYear(baseTime time.Time, val int) time.Time {
	return baseTime.AddDate(val, 0, 0)
}

// AddMonth 加月
func AddMonth(baseTime time.Time, val int) time.Time {
	return baseTime.AddDate(0, val, 0)
}

// AddDay 加天
func AddDay(baseTime time.Time, val int) time.Time {
	return baseTime.AddDate(0, 0, val)
}

// ToDayStart baseTime 的 0
func ToDayStart(baseTime time.Time) time.Time {
	return time.Date(baseTime.Year(), baseTime.Month(), baseTime.Day(), 0, 0, 0, 0, baseTime.Location())
}

// ToDayEnd baseTime 的 23:59:59
func ToDayEnd(baseTime time.Time) time.Time {
	return time.Date(baseTime.Year(), baseTime.Month(), baseTime.Day(), 23, 59, 59, 999999999, baseTime.Location())
}

// ToMonthStart baseTime 的當月第一天
func ToMonthStart(baseTime time.Time) time.Time {
	return time.Date(baseTime.Year(), baseTime.Month(), 1, 0, 0, 0, 0, baseTime.Location())
}

// ToMonthEnd baseTime 的當月最後一天
func ToMonthEnd(baseTime time.Time) time.Time {
	return ToMonthStart(baseTime).AddDate(0, 1, 0).Add(-time.Nanosecond)
}

// OneDay baseTime 的 0 到 23:59:59
func OneDay(baseTime time.Time) []time.Time {
	start := time.Date(baseTime.Year(), baseTime.Month(), baseTime.Day(), 0, 0, 0, 0, baseTime.Location())
	end := time.Date(baseTime.Year(), baseTime.Month(), baseTime.Day(), 23, 59, 59, 999999999, baseTime.Location())
	return []time.Time{start, end}
}

// OneDayOffset OneDay 但加減 offset 偏移天數
func OneDayOffset(baseTime time.Time, offsetDay int) []time.Time {
	return OneDay(baseTime.AddDate(0, 0, offsetDay))
}

// DayRange 由 baseTime 向前或向後延伸幾天
func DayRange(baseTime time.Time, addDay int) []time.Time {
	var (
		start time.Time
		end   time.Time
	)

	if addDay >= 0 {
		start = time.Date(baseTime.Year(), baseTime.Month(), baseTime.Day(), 0, 0, 0, 0, baseTime.Location())
		end = time.Date(baseTime.Year(), baseTime.Month(), baseTime.Day(), 23, 59, 59, 999999999, baseTime.Location())
		end = end.AddDate(0, 0, addDay)
	} else {
		start = time.Date(baseTime.Year(), baseTime.Month(), baseTime.Day(), 0, 0, 0, 0, baseTime.Location())
		end = time.Date(baseTime.Year(), baseTime.Month(), baseTime.Day(), 23, 59, 59, 999999999, baseTime.Location())
		start = start.AddDate(0, 0, addDay)
	}

	return []time.Time{start, end}
}

// ConvertToRange start 到 0 點，end 到 23:59:59
func ConvertToRange(start time.Time, end time.Time) []time.Time {
	start = time.Date(start.Year(), start.Month(), start.Day(), 0, 0, 0, 0, start.Location())
	end = time.Date(end.Year(), end.Month(), end.Day(), 23, 59, 59, 999999999, end.Location())
	return []time.Time{start, end}
}

// ToWeekStart 到 baseTime 所在的 week 星期一
func ToWeekStart(baseTime time.Time) time.Time {
	w := baseTime.Weekday()

	switch w {
	case time.Monday:
		return ToDayStart(baseTime)
	case time.Sunday:
		return ToDayStart(baseTime.AddDate(0, 0, -6))
	default:
		wInt := int(w)
		start := ToDayStart(baseTime).AddDate(0, 0, -(wInt - 1))
		return start
	}
}

// ToWeekEnd 到 baseTime 所在的 week 星期日
func ToWeekEnd(baseTime time.Time) time.Time {
	w := baseTime.Weekday()

	switch w {
	case time.Monday:
		return ToDayEnd(baseTime.AddDate(0, 0, 6))
	case time.Sunday:
		return ToDayEnd(baseTime)
	default:
		wInt := int(w)
		end := ToDayEnd(baseTime).AddDate(0, 0, 7-wInt)
		return end
	}
}

// OneWeek 傳回一星期的範圍，可用 offsetWeek 做偏移
func OneWeek(baseTime time.Time, offsetWeek int) []time.Time {
	offsetTime := baseTime.AddDate(0, 0, offsetWeek*7)
	return WeekRange(offsetTime, 0)
}

// WeekRange 由 baseTime 向後或是向前延伸幾個 week
//	從星期一開始到星期日結束
func WeekRange(baseTime time.Time, week int) []time.Time {
	endBaseTime := baseTime.AddDate(0, 0, week*7)

	if week >= 0 {
		start := ToWeekStart(baseTime)
		end := ToWeekEnd(endBaseTime)
		return []time.Time{start, end}
	}

	start := ToWeekStart(endBaseTime)
	end := ToWeekEnd(baseTime)
	return []time.Time{start, end}
}

// OneMonth 傳回一個月的範圍，可用 offsetMonth 做偏移
func OneMonth(baseTime time.Time, offsetMonth int) []time.Time {
	offsetTime := baseTime.AddDate(0, offsetMonth, 0)
	return MonthRange(offsetTime, 0)
}

// MonthRange 由 baseTime 向前或向後延伸幾個月
//	從月的開始到月的結束
func MonthRange(baseTime time.Time, addMonth int) []time.Time {
	if addMonth < 0 {
		baseTime = baseTime.AddDate(0, addMonth, 0)
		addMonth = -addMonth + 1
	} else {
		addMonth = addMonth + 1
	}

	start := time.Date(baseTime.Year(), baseTime.Month(), 1, 0, 0, 0, 0, baseTime.Location())
	end := start.AddDate(0, addMonth, 0).Add(-time.Nanosecond)
	return []time.Time{start, end}
}

// OneYear 傳回一個月的範圍，可用 offsetYear 做偏移
func OneYear(baseTime time.Time, offsetYear int) []time.Time {
	offsetTime := baseTime.AddDate(offsetYear, 0, 0)
	return YearRange(offsetTime, 0)
}

// YearRange 由 baseTime 向前或向後延伸幾年
//	從一年的開始到一年的結束
func YearRange(baseTime time.Time, addYear int) []time.Time {
	if addYear < 0 {
		baseTime = baseTime.AddDate(addYear, 0, 0)
		addYear = -addYear + 1
	} else {
		addYear = addYear + 1
	}

	start := time.Date(baseTime.Year(), 1, 1, 0, 0, 0, 0, baseTime.Location())
	end := start.AddDate(addYear, 0, 0).Add(-time.Nanosecond)
	return []time.Time{start, end}
}

// IsNewTime 看是否為初始值 0001-01-01 00:00:00 +0000 UTC
//
func IsNewTime(obj *time.Time) bool {
	return obj == nil || obj.Equal(newTime)
}

// BeforeOrEqual data 是否 Before 或是 Equal base
//
func BeforeOrEqual(data time.Time, base time.Time) bool {
	return data.Before(base) || data.Equal(base)
}

// AfterOrEqual data 是否 After 或是 Equal base
//
func AfterOrEqual(data time.Time, base time.Time) bool {
	return data.After(base) || data.Equal(base)
}

var timestampLayout string = "20060102150405"

// GetTimeStampStr 轉換為 20060102150405 格式的 string
func GetTimeStampStr(t time.Time) string {
	return t.Format(timestampLayout)
}
