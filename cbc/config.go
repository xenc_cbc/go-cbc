package cbc

// Config 泛用的設定檔
type Config map[string]interface{}

// GetString 取得數值，不存在或是型態不符則傳回 def
func (cfg Config) GetString(key string, def string) string {
	v, ok := cfg[key]

	if ok {
		v2, f := v.(string)

		if f {
			return v2
		}
	}

	return def
}

// GetInt 取得數值，不存在則傳回 def
func (cfg Config) GetInt(key string, def int) int {
	v, ok := cfg[key]

	if ok {
		v2, f := v.(float64)

		if f {
			return int(v2)
		}
	}

	return def
}

// GetFloat32 取得數值，不存在則傳回 def
func (cfg Config) GetFloat32(key string, def float32) float32 {
	v, ok := cfg[key]

	if ok {
		v2, f := v.(float64)

		if f {
			return float32(v2)
		}
	}

	return def
}

// GetFloat64 取得數值，不存在則傳回 def
func (cfg Config) GetFloat64(key string, def float64) float64 {
	v, ok := cfg[key]

	if ok {
		v2, f := v.(float64)

		if f {
			return v2
		}
	}

	return def
}

// GetBool 取得數值，不存在則傳回 def
func (cfg Config) GetBool(key string, def bool) bool {
	v, ok := cfg[key]

	if ok {
		v2, f := v.(bool)

		if f {
			return v2
		}
	}

	return def
}
