package cbc

import (
	"net/http"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	gws "github.com/gorilla/websocket"
	"go.uber.org/zap"
)

var _ PluginWithInit = &WebSocket{}

// WebSocket gorilla/websocket 的應用
type WebSocket struct {
	PluginImp
	Upgrader       *gws.Upgrader
	Config         WebSocketConfig
	WriteWait      time.Duration
	PongWait       time.Duration
	PingPeriod     time.Duration
	MaxMessageSize int64
	ActiveLogger   *Logger
	log            *zap.Logger
}

func (ws *WebSocket) GetName() string {
	return WebsocketPluginName
}

// Init 初始化
func (ws *WebSocket) Init(context *Context, cloader ConfigLoader, logger *Logger) {
	ws.ActiveLogger = logger
	ws.log = logger.Log.With(zap.String("source", "websocket"))
	jsonExist, jsonErr := cloader.DecodeConfig("websocket.json", &ws.Config)

	if jsonErr != nil {
		ws.log.Info("json error")
		ws.state = Failed
		return
	} else if !jsonExist {
		ws.log.Info("json not found , Disable.")
		ws.state = Disable
		return
	}

	ws.Upgrader = &gws.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			return ws.Config.CheckOrigin
		},
		ReadBufferSize:  ws.Config.ReadBufferSize,
		WriteBufferSize: ws.Config.WriteBufferSize,
	}

	ws.WriteWait = time.Duration(ws.Config.WriteWait) * time.Second
	ws.PongWait = time.Duration(ws.Config.PongWait) * time.Second
	ws.PingPeriod = time.Duration(ws.Config.PingPeriod) * time.Second
	ws.MaxMessageSize = ws.Config.MaxMessageSize
	ws.state = Enable
}

// ReadMessageLoop 使用 for 循環呼叫 ReadMessage
//
//	一直不斷 ReadMessage 傳給 handler 處理，
//	如果 handler 回傳 false代表要結束循環，
//	如果有 error 則呼叫 errorHandler，如果 errorHandler 傳回 false 或是沒有設定 errorHandler 都會結束 for
//	結束時如果有 finishHandler 就呼叫。
//
// Deprecated: Use WebSocketWrap instead.
func (ws *WebSocket) ReadMessageLoop(con *gws.Conn,
	handler func(con *gws.Conn, msgType int, data []byte) bool,
	errorHandler func(con *gws.Conn, err error) bool,
	finishHandler func(con *gws.Conn)) {
	ws.ReadMessageLoopWithPong(con, handler, errorHandler, finishHandler, nil)
}

// ReadMessageLoopWithPong 使用 for 循環呼叫 ReadMessage
//
//	一直不斷 ReadMessage 傳給 handler 處理，
//	如果 handler 回傳 false代表要結束循環，
//	如果有 error 則呼叫 errorHandler，如果 errorHandler 傳回 false 或是沒有設定 errorHandler 都會結束 for
//	結束時如果有 finishHandler 就呼叫，在 Pong 結束後呼叫指定的 func。
//
// Deprecated: Use WebSocketWrap instead.
func (ws *WebSocket) ReadMessageLoopWithPong(con *gws.Conn,
	handler func(con *gws.Conn, msgType int, data []byte) bool,
	errorHandler func(con *gws.Conn, err error) bool,
	finishHandler func(con *gws.Conn),
	pongAfterHandler func(con *gws.Conn)) {
	defer func() {
		if finishHandler != nil {
			finishHandler(con)
		}
	}()

	ws.initReadLoopSetting(con, pongAfterHandler)

	for {
		msgType, data, err := con.ReadMessage()

		if err == nil {
			if !handler(con, msgType, data) {
				ws.socketLogBreakReadLoop("Handler Return Break Flag.", con)
				break
			}
		} else {
			if errorHandler == nil {
				ws.socketLogBreakReadLoop("Default Error Handle.", con)
				break
			} else {
				if !errorHandler(con, err) {
					ws.socketLogBreakReadLoop("Error Handler Return Break Flag.", con)
					break
				}
			}
		}
	}
}

// ReadLoop 使用 for 循環呼叫 handler
//
//	一直不斷將 con 傳給 handler 處理，
//	如果 handler 回傳 false，代表要結束循環，
//	結束時如果有 finishHandler 就呼叫。
//
// Deprecated: Use WebSocketWrap instead.
func (ws *WebSocket) ReadLoop(con *gws.Conn,
	handler func(con *gws.Conn) bool,
	finishHandler func(con *gws.Conn)) {
	ws.ReadLoopWithPong(con, handler, finishHandler, nil)
}

// ReadLoopWithPong 使用 for 循環呼叫 handler
//
//	一直不斷將 con 傳給 handler 處理，
//	如果 handler 回傳 false，代表要結束循環，
//	結束時如果有 finishHandler 就呼叫，在 Pong 結束後呼叫指定的 func。
//
// Deprecated: Use WebSocketWrap instead.
func (ws *WebSocket) ReadLoopWithPong(con *gws.Conn,
	handler func(con *gws.Conn) bool,
	finishHandler func(con *gws.Conn),
	pongAfterHandler func(con *gws.Conn)) {
	defer func() {
		if finishHandler != nil {
			finishHandler(con)
		}
	}()

	ws.initReadLoopSetting(con, pongAfterHandler)

	for {
		if !handler(con) {
			break
		}
	}
}

func (ws *WebSocket) socketLogBreakReadLoop(msg string, con *gws.Conn) {
	if ws.ActiveLogger.IsDebug() {
		ws.log.Debug(msg,
			zap.String("action", "breakReadLoop"),
			zap.String("remote", con.RemoteAddr().String()))
	}
}

func (ws *WebSocket) initReadLoopSetting(con *gws.Conn, callback func(*gws.Conn)) {
	con.SetReadLimit(ws.MaxMessageSize)
	ws.NextReadDeadline(con)
	con.SetPongHandler(func(string) error {
		ws.NextReadDeadline(con)

		if ws.Config.ShowPingLog {
			ws.log.Debug("Receive Pong.",
				zap.String("action", "pong"),
				zap.String("remote", con.RemoteAddr().String()))
		}

		if callback != nil {
			callback(con)
		}

		return nil
	})
}

// StartPingTicker 使用 goroutine 定時 ping
//
//	回傳的 func 如果呼叫，將會結束 goroutine，在結束後呼叫會導致 error
//	sync.Mutex 是 write 時的鎖定使用.
//
// Deprecated: Use WebSocketWrap instead.
func (ws *WebSocket) StartPingTicker(con *gws.Conn) (func(), *sync.Mutex) {
	var stopChan chan bool = make(chan bool, 1)
	lock := sync.Mutex{}
	go func() {
		ticker := ws.CreatePingTicker()

		defer func() {
			ticker.Stop()
		}()

		if ws.Config.ShowPingLog {
			ws.log.Debug("Start Ping Ticker.",
				zap.String("action", "Start PingTicker"),
				zap.String("remote", con.RemoteAddr().String()))
		}

	bkfor:
		for {
			select {
			case <-stopChan:
				if ws.Config.ShowPingLog {
					ws.log.Debug("Stop Ping Ticker.",
						zap.String("action", "breakPingTicker"),
						zap.String("remote", con.RemoteAddr().String()))
				}
				break bkfor
			case <-ticker.C:
				if ws.Config.ShowPingLog {
					ws.log.Debug("Send Ping.",
						zap.String("action", "ping"),
						zap.String("remote", con.RemoteAddr().String()))
				}

				ws.NextWriteDeadline(con)
				lock.Lock()
				err := con.WriteControl(gws.PingMessage,
					nil,
					time.Now().Add(ws.WriteWait))
				lock.Unlock()
				if err != nil {
					if ws.Config.ShowPingLog {
						ws.log.Debug("Ping Error.",
							zap.String("action", "ping"),
							zap.String("remote", con.RemoteAddr().String()),
							zap.Error(err))
					}

					break bkfor
				}
			}
		}

		close(stopChan)
	}()
	return func() {
		stopChan <- true
	}, &lock
}

// NextWriteDeadline 設定 WriteDeadline
//
// Deprecated: Use WebSocketWrap instead.
func (ws *WebSocket) NextWriteDeadline(con *gws.Conn) {
	con.SetWriteDeadline(time.Now().Add(ws.WriteWait))
}

// NextReadDeadline 設定 ReadDeadline
//
// Deprecated: Use WebSocketWrap instead.
func (ws *WebSocket) NextReadDeadline(con *gws.Conn) {
	con.SetReadDeadline(time.Now().Add(ws.PongWait))
}

// CreatePingTicker 建立 Ping 的 Ticker
//
// Deprecated: Use WebSocketWrap instead.
func (ws *WebSocket) CreatePingTicker() *time.Ticker {
	return time.NewTicker(ws.PingPeriod)
}

// WirtePingMessage 寫入 Ping
//
// Deprecated: Use WebSocketWrap instead.
func (ws *WebSocket) WirtePingMessage(con *gws.Conn, msg []byte) error {
	ws.NextWriteDeadline(con)
	if err := con.WriteMessage(gws.PingMessage, msg); err != nil {
		return err
	}

	return nil
}

// UpgradeWithContext 呼叫 Upgrader 的 Upgrade
//
// Deprecated: Use WrapWithContext instead.
func (ws *WebSocket) UpgradeWithContext(gc *gin.Context, responseHeader http.Header) (*gws.Conn, error) {
	return ws.Upgrader.Upgrade(gc.Writer, gc.Request, responseHeader)
}

// Upgrade 呼叫 Upgrader 的 Upgrade
//
// Deprecated: Use Wrap instead.
func (ws *WebSocket) Upgrade(
	w http.ResponseWriter, r *http.Request, responseHeader http.Header) (*gws.Conn, error) {
	return ws.Upgrader.Upgrade(w, r, responseHeader)
}

// WrapWithContext 呼叫 Upgrader 的 Upgrade，回傳 WebSocketWrap
func (ws *WebSocket) WrapWithContext(gc *gin.Context, responseHeader http.Header) (*WebSocketWrap, error) {
	return ws.Wrap(gc.Writer, gc.Request, responseHeader)
}

// Wrap 呼叫 Upgrader 的 Upgrade，回傳 WebSocketWrap
func (ws *WebSocket) Wrap(
	w http.ResponseWriter, r *http.Request, responseHeader http.Header) (*WebSocketWrap, error) {
	wrap := &WebSocketWrap{}
	con, err := ws.Upgrader.Upgrade(w, r, responseHeader)

	if err == nil {
		wrap.cWebSocket = ws
		wrap.con = con
	}

	return wrap, err
}

func (ws *WebSocket) WrapWithConnection(wsCon *gws.Conn) *WebSocketWrap {
	return &WebSocketWrap{cWebSocket: ws, con: wsCon}
}
