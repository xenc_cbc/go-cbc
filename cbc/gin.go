package cbc

import (
	"context"
	"crypto/tls"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/filec"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/httpc"
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-contrib/sessions/redis"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"go.uber.org/zap"
)

var _ FullPlugin = &Gin{}

// Gin 對應 gin
type Gin struct {
	PluginImp
	// Router gin.Engine
	Router *gin.Engine
	// Config 設定檔
	Config ServerConfig
	// HTTP 啟動後的 http.Server
	HTTP         *http.Server
	log          *zap.Logger
	quit         chan os.Signal
	lock         sync.Mutex
	stopChanFlag bool
}

func (gn *Gin) GetName() string {
	return GinPluginName
}

// Init 初始化
func (gn *Gin) Init(context *Context, cloader ConfigLoader, logger *Logger) {
	gn.log = logger.Log.With(zap.String("source", "http"))
	gn.quit = make(chan os.Signal)
	jsonExist, jsonErr := cloader.DecodeConfig("server.json", &gn.Config)

	if jsonErr != nil {
		gn.log.Info("json error")
		gn.state = Failed
		return
	} else if !jsonExist {
		gn.log.Info("json not found , Disable.")
		gn.state = Disable
		return
	}

	binding.Validator = Validator

	if gn.Config.StorePath == "" {
		gn.Config.StorePath = "store"
	}

	gn.Config.StorePath = context.ParsePath(gn.Config.StorePath)

	filec.CheckAndCreateDir(gn.Config.StorePath)

	if gn.Config.SslKey == "" {
		gn.Config.SslKey = "server.key"
	}

	gn.Config.SslKey = context.ConvertPath(gn.Config.SslKey)

	if gn.Config.SslCrt == "" {
		gn.Config.SslCrt = "server.crt"
	}

	gn.Config.SslCrt = context.ConvertPath(gn.Config.SslCrt)

	gn.log.Info("Init Gin",
		zap.String("Address", gn.Config.Listen),
		zap.String("Port", gn.Config.Port),
		zap.Bool("SSL", gn.Config.Ssl),
		zap.Bool("SslSkipVerify", gn.Config.SslSkipVerify),
		zap.String("SslKey", gn.Config.SslKey),
		zap.String("SslCrt", gn.Config.SslCrt),
		zap.String("WebURL", gn.Config.WebURL),
		zap.Bool("Allow CORS", gn.Config.AllowCORS),
		zap.Int("ReadTimeoutSecond", gn.Config.ReadTimeoutSecond),
		zap.Int("WriteTimeoutSecond", gn.Config.WriteTimeoutSecond),
		zap.String("File Stroe", gn.Config.StorePath),
		zap.Bool("Debug", logger.IsDebug()))

	if logger.IsDebug() {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}

	gn.Router = gin.New()
	gn.Router.Use(gin.Logger())

	gn.Router.Use(func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				var (
					errEntity    error
					stringEntity string
					isError      bool
					isString     bool
				)

				h := c.Request.Header

				if errEntity, isError = err.(error); !isError {
					stringEntity, isString = err.(string)
				}

				if isError {
					logger.Log.Error("Error Catch", zap.Error(errEntity))
				} else if isString {
					logger.Log.Error("Error Catch", zap.String("error", stringEntity))
				} else {
					logger.Log.Error("Error Catch", zap.Any("error", err))
				}

				typeTxtArr, hOk := h["Accept"]

				isJsonResponse := hOk &&
					len(typeTxtArr) > 0 &&
					strings.Contains(typeTxtArr[0], "application/json")

				if !isJsonResponse {
					_, isJsonResponse = h["Er-Json"]
				}

				if isJsonResponse {
					json := httpc.JSONResult{}
					json.Msg = httpc.JSONErrorGeneral

					if isError {
						// error is v10 or other
						fArr, ok := httpc.ConvertValidationError(&errEntity)

						if ok {
							json.Msg = httpc.JSONErrorField
							json.Result = fArr
						} else {
							// is other error
							json.Msg = httpc.JSONErrorGeneral

							if logger.IsDebug() {
								json.Result = errEntity.Error()
							}
						}
					} else if isString {
						json.Msg = httpc.JSONErrorGeneral

						if logger.IsDebug() {
							json.Result = stringEntity
						}
					}

					c.JSON(http.StatusOK, json)
				}
			}
		}()
		c.Next()
	})

	if gn.Config.AllowCORS {
		corsCfg := cors.DefaultConfig()
		originLen := len(gn.Config.CorsConfig.AllowOrigins)

		gn.log.Info("Cors Config",
			zap.Any("Config", corsCfg))

		gn.log.Info("Cors Config",
			zap.String("AllowOrigins", strings.Join(gn.Config.CorsConfig.AllowOrigins, ",")),
			zap.String("AllowHeaders", strings.Join(gn.Config.CorsConfig.AllowHeaders, ",")),
			zap.String("AllowMethods", strings.Join(gn.Config.CorsConfig.AllowMethods, ",")),
			zap.String("ExposeHeaders", strings.Join(gn.Config.CorsConfig.ExposeHeaders, ",")),
			zap.Bool("AllowCredentials", gn.Config.CorsConfig.AllowCredentials),
			zap.Int("MaxAge", gn.Config.CorsConfig.MaxAge),
		)

		if originLen > 0 {
			corsCfg.AllowOriginFunc = func(origin string) bool {
				for i := 0; i < originLen; i++ {
					if gn.Config.CorsConfig.AllowOrigins[i] == origin {
						return true
					}
				}

				return false
			}
		} else {
			corsCfg.AllowOriginFunc = func(origin string) bool {
				return true
			}
		}

		if len(gn.Config.CorsConfig.AllowHeaders) > 0 {
			corsCfg.AllowHeaders = gn.Config.CorsConfig.AllowHeaders
		}

		if len(gn.Config.CorsConfig.AllowMethods) > 0 {
			corsCfg.AllowMethods = gn.Config.CorsConfig.AllowMethods
		}

		if len(gn.Config.CorsConfig.ExposeHeaders) > 0 {
			corsCfg.ExposeHeaders = gn.Config.CorsConfig.ExposeHeaders
		}

		if gn.Config.CorsConfig.AllowCredentials {
			corsCfg.AllowCredentials = gn.Config.CorsConfig.AllowCredentials
		}

		corsCfg.MaxAge = time.Duration(gn.Config.CorsConfig.MaxAge) * time.Hour
		gn.Router.Use(cors.New(corsCfg))
	}

	gn.initSession()

	httpReadTimeout := time.Duration(gn.Config.ReadTimeoutSecond) * time.Second
	httpWriteTimeout := time.Duration(gn.Config.WriteTimeoutSecond) * time.Second

	if gn.Config.Ssl {
		gn.HTTP = &http.Server{
			Addr:         gn.Config.Listen + ":" + gn.Config.Port,
			Handler:      gn.Router,
			ReadTimeout:  httpReadTimeout,
			WriteTimeout: httpWriteTimeout,
		}
	} else {
		gn.HTTP = &http.Server{
			Addr: gn.Config.Listen + ":" + gn.Config.Port,
			TLSConfig: &tls.Config{
				ServerName:         gn.Config.ServerName,
				InsecureSkipVerify: gn.Config.SslSkipVerify,
			},
			Handler:      gn.Router,
			ReadTimeout:  httpReadTimeout,
			WriteTimeout: httpWriteTimeout,
		}
	}

	gn.stopChanFlag = true
	gn.state = Enable
}

func (gn *Gin) initSession() {
	var (
		store sessions.Store
		err   error
	)
	sessionConfig := gn.Config.SessionConfig

	gn.log.Info("Session Type : " + sessionConfig.Type)

	switch sessionConfig.Type {
	case "redis":
		gn.log.Info("Redis",
			zap.String("Server", sessionConfig.RedisAddr),
			zap.String("Protocol", sessionConfig.RedisProtocol),
			zap.Int("MaxIdel", sessionConfig.RedisMaxIdel))
		store, err = redis.NewStore(sessionConfig.RedisMaxIdel,
			sessionConfig.RedisProtocol,
			sessionConfig.RedisAddr,
			sessionConfig.RedisPassword,
			[]byte(sessionConfig.KeyPairs))

		if err != nil {
			gn.log.Fatal("Session Init Failed.", zap.Error(err))
		}
	default:
		store = cookie.NewStore([]byte(sessionConfig.KeyPairs))
	}

	if !gn.Config.Ssl && sessionConfig.Secure {
		gn.log.Info("==!!!== Server SSL Disabled , Session Cookie Secure set to false ? (sessionConfig.Secure = false)")
		// gn.log.Info("==!!!== sessionConfig.Secure force set false")
		// sessionConfig.Secure = false
	}

	if sessionConfig.SameSite == http.SameSiteNoneMode {
		if gn.Config.Ssl {
			if !sessionConfig.Secure {
				sessionConfig.Secure = true
				gn.log.Info("==!!!== Cookie SameSite Is None , Session Cookie Secure set to true.")
			}
		} else {
			gn.log.Info("==!!!== Server SSL Disabled , Cookie Skip Set SameSite ? (sessionConfig.SameSite = 0)")
			// gn.log.Info("==!!!== sessionConfig.SameSite force set 0")
			// sessionConfig.SameSite = 0
		}
	}

	var sameSiteStr string

	switch sessionConfig.SameSite {
	case http.SameSiteDefaultMode:
		sameSiteStr = "Default"
	case http.SameSiteLaxMode:
		sameSiteStr = "Lax"
	case http.SameSiteStrictMode:
		sameSiteStr = "Strict"
	case http.SameSiteNoneMode:
		sameSiteStr = "None"
	default:
		sameSiteStr = "Skip"
	}

	gn.log.Info("Session Cookie Setting.",
		zap.String("Path", sessionConfig.Path),
		zap.String("Domain", sessionConfig.Domain),
		zap.Int("MaxAge", sessionConfig.MaxAge),
		zap.Bool("HTTPOnly", sessionConfig.HTTPOnly),
		zap.Bool("Secure", sessionConfig.Secure),
		zap.String("SameSite", sameSiteStr))

	if sameSiteStr == "Skip" {
		store.Options(sessions.Options{
			Path:     sessionConfig.Path,
			Domain:   sessionConfig.Domain,
			MaxAge:   sessionConfig.MaxAge,
			HttpOnly: sessionConfig.HTTPOnly,
			Secure:   sessionConfig.Secure,
		})
	} else {
		store.Options(sessions.Options{
			Path:     sessionConfig.Path,
			Domain:   sessionConfig.Domain,
			MaxAge:   sessionConfig.MaxAge,
			HttpOnly: sessionConfig.HTTPOnly,
			Secure:   sessionConfig.Secure,
			SameSite: sessionConfig.SameSite,
		})
	}

	gn.Router.Use(sessions.Sessions(sessionConfig.Name, store))
}

// Startup 啟動
//
// startupOK 是在執行完成時才執行
// shutdownOk 則是在確定都停止完成後才執行
// waitSec 關閉時要等待多久
func (gn *Gin) Startup(startupOk func(), shutdownOk func(), waitSec int) {
	gn.lock.Lock()

	if gn.stopChanFlag {
		gn.stopChanFlag = false

		gn.log.Info("Startup Server.", zap.String("ServerName", gn.Config.ServerName), zap.String("Port", gn.Config.Port))
		signal.Notify(gn.quit, syscall.SIGINT, syscall.SIGTERM)

		if gn.Config.Ssl {
			go func() {
				if err := gn.HTTP.ListenAndServeTLS(gn.Config.SslCrt, gn.Config.SslKey); err != nil && err != http.ErrServerClosed {
					gn.log.Error("Https Listen Error", zap.Error(err))
					gn.lock.Lock()
					defer gn.lock.Unlock()

					if !gn.stopChanFlag {
						gn.quit <- syscall.SIGINT
					}
				}
			}()
		} else {
			go func() {
				if err := gn.HTTP.ListenAndServe(); err != nil && err != http.ErrServerClosed {
					gn.log.Error("Http Listen Error", zap.Error(err))
					gn.lock.Lock()
					defer gn.lock.Unlock()

					if !gn.stopChanFlag {
						gn.quit <- syscall.SIGINT
					}
				}
			}()
		}

		if startupOk != nil {
			startupOk()
		}

		gn.lock.Unlock()

		<-gn.quit

		gn.lock.Lock()
		gn.log.Info("Shutdown Server Start")

		signal.Stop(gn.quit)
		ctx, cancel := context.WithTimeout(context.Background(), time.Duration(waitSec)*time.Second)

		defer cancel()

		if err := gn.HTTP.Shutdown(ctx); err != nil {
			gn.log.Error("Server Shutdown Error.", zap.Error(err))
		}

		gn.log.Info("Wait ...", zap.Int("Second", waitSec))
		<-ctx.Done()

		gn.log.Info("Shutdown Server Finished.")
		gn.stopChanFlag = true
		gn.lock.Unlock()

		if shutdownOk != nil {
			shutdownOk()
		}
	} else {
		gn.log.Info("Server Already Startup")
		gn.lock.Unlock()
	}
}

// Shutdown 停止
func (gn *Gin) Shutdown() {
	gn.log.Info("Call Shutdown")

	gn.lock.Lock()

	if gn.stopChanFlag {
		gn.log.Info("Server Already Shutdown")
	} else {
		gn.quit <- syscall.SIGINT
	}

	gn.lock.Unlock()
}

// Close 同 Shutdown()
func (gn *Gin) Close() {
	gn.Shutdown()
}
