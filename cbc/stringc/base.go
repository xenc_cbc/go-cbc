package stringc

import (
	"strconv"
	"strings"
)

// SplitToInt64Array 將 data 依 split 分割之後轉為 []int64
//
//	@param data
//	@param split
//	@return []int64
//	@return error
func SplitToInt64Array(data string, split string) ([]int64, error) {
	return ToInt64Array(strings.Split(data, split))
}

// SplitToIntArray 將 data 依 split 分割之後轉為 []int
//
//	@param data
//	@param split
//	@return []int
//	@return error
func SplitToIntArray(data string, split string) ([]int, error) {
	return ToIntArray(strings.Split(data, split))
}

// SplitToBoolArray 將 data 依 split 分割之後轉為 []bool
//
//	@param data
//	@param split
//	@return []bool
//	@return error
func SplitToBoolArray(data string, split string) ([]bool, error) {
	return ToBoolArray(strings.Split(data, split))
}

// SplitToFloatArray 將 data 依 split 分割之後轉為 []float64
//
//	@param data
//	@param split
//	@return []float64
//	@return error
func SplitToFloatArray(data string, split string) ([]float64, error) {
	return ToFloatArray(strings.Split(data, split))
}

// ToInt64Array 將 data 轉為 []int64
//
//	@param data
//	@return []int64
//	@return error
func ToInt64Array(data []string) ([]int64, error) {
	arrLen := len(data)

	if arrLen == 0 {
		return []int64{}, nil
	}

	intArr := make([]int64, arrLen)

	for i := 0; i < arrLen; i++ {
		num, err := strconv.ParseInt(data[i], 10, 64)

		if err == nil {
			intArr[i] = num
		} else {
			return []int64{}, err
		}
	}

	return intArr, nil
}

// ToIntArray 將 data 轉為 []int
//
//	@param data
//	@return []int
//	@return error
func ToIntArray(data []string) ([]int, error) {
	arrLen := len(data)

	if arrLen == 0 {
		return []int{}, nil
	}

	intArr := make([]int, arrLen)

	for i := 0; i < arrLen; i++ {
		num, err := strconv.Atoi(data[i])

		if err == nil {
			intArr[i] = num
		} else {
			return []int{}, err
		}
	}

	return intArr, nil
}

// ToBoolArray 將 data 轉為 []bool
//
//	@param data
//	@return []bool
//	@return error
func ToBoolArray(data []string) ([]bool, error) {
	arrLen := len(data)

	if arrLen == 0 {
		return []bool{}, nil
	}

	arr := make([]bool, arrLen)

	for i := 0; i < arrLen; i++ {
		val, err := strconv.ParseBool(data[i])

		if err == nil {
			arr[i] = val
		} else {
			return []bool{}, err
		}
	}

	return arr, nil
}

// ToFloatArray 將 data 轉為 []float64
//
//	@param data
//	@return []float64
//	@return error
func ToFloatArray(data []string) ([]float64, error) {
	arrLen := len(data)

	if arrLen == 0 {
		return []float64{}, nil
	}

	arr := make([]float64, arrLen)

	for i := 0; i < arrLen; i++ {
		val, err := strconv.ParseFloat(data[i], 64)

		if err == nil {
			arr[i] = val
		} else {
			return []float64{}, err
		}
	}

	return arr, nil
}
