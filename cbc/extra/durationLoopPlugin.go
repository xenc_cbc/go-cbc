package extra

import (
	"errors"
	"sort"
	"time"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc"
	"go.uber.org/zap"
)

var _ cbc.PluginWithInit = &DurationLoopPlugin{}
var _ cbc.PluginWithStartup = &DurationLoopPlugin{}
var timeFormat string = "2006-01-02 15:04:05"

type SecDurationGenerator func(previous time.Time, now time.Time) time.Duration

func createFixedDurationGenerator(hours []int, minutes []int,
	seconds []int) (SecDurationGenerator, error) {
	emptyHours := len(hours) == 0
	emptyMinutes := len(minutes) == 0
	emptySeconds := len(seconds) == 0

	if emptyHours && emptyMinutes && emptySeconds {
		return nil, errors.New("need hours ,minutes or seconds")
	}

	if !emptyHours {
		sort.Ints(hours)

		if hours[0] < 0 {
			return nil, errors.New("hour range invalid , less than 0")
		}

		if hours[len(hours)-1] > 23 {
			return nil, errors.New("hour range invalid , greater than 23")
		}
	}

	if !emptyMinutes {
		sort.Ints(minutes)

		if minutes[0] < 0 {
			return nil, errors.New("minute range invalid , less than 0")
		}

		if minutes[len(minutes)-1] > 59 {
			return nil, errors.New("minute range invalid , greater than 59")
		}
	}

	if !emptySeconds {
		sort.Ints(seconds)

		if seconds[0] < 0 {
			return nil, errors.New("second range invalid , less than 0")
		}

		if seconds[len(seconds)-1] > 59 {
			return nil, errors.New("second range invalid , greater than 59")
		}
	}

	return func(previous time.Time, now time.Time) time.Duration {
		var (
			nextHour   = -1
			nextMinute = -1
			nextSec    = -1
		)

		runTime := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())

		if !emptySeconds {
			for _, hitSec := range seconds {
				if now.Second() < hitSec {
					nextSec = hitSec
					break
				}
			}

			if nextSec < 0 && emptyMinutes && emptyHours {
				nextSec = seconds[0]

				nHour := now.Hour()
				nMinute := now.Minute()

				if nMinute == 59 {
					if nHour == 23 {
						runTime = runTime.AddDate(0, 0, 1)
					} else {
						nextHour = nHour + 1
					}
				} else {
					nextMinute = nMinute + 1
					nextHour = nHour
				}
			}

		}

		if nextSec < 0 {
			if !emptyMinutes {
				for _, hitMinute := range minutes {
					if now.Minute() < hitMinute {
						nextMinute = hitMinute

						if emptySeconds {
							nextSec = 0
						} else {
							nextSec = seconds[0]
						}

						break
					}
				}
			}
		}

		if nextMinute < 0 {
			if !emptyHours {
				for _, hitHour := range hours {
					if now.Hour() < hitHour {
						nextHour = hitHour

						if emptyMinutes {
							nextMinute = 0
						} else {
							nextMinute = minutes[0]
						}

						if emptySeconds {
							nextSec = 0
						} else {
							nextSec = seconds[0]
						}
						break
					}
				}

				if nextHour == -1 {
					runTime = runTime.AddDate(0, 0, 1)

					if emptyHours {
						nextHour = 0
					} else {
						nextHour = hours[0]
					}

					if emptyMinutes {
						nextMinute = 0
					} else {
						nextMinute = minutes[0]
					}

					if emptySeconds {
						nextSec = 0
					} else {
						nextSec = seconds[0]
					}
				}
			}
		}

		if nextHour == -1 {
			nextHour = now.Hour()
		}

		if nextMinute == -1 {
			nextMinute = now.Minute()
		}

		if nextSec == -1 {
			nextSec = 0
		}

		finalTimeAddDuration :=
			(time.Hour * time.Duration(nextHour)) +
				(time.Minute * time.Duration(nextMinute)) +
				(time.Second * time.Duration(nextSec))

		runTime = runTime.Add(finalTimeAddDuration)

		return runTime.Sub(now)
	}, nil
}

type DurationLoopPlugin struct {
	cbc.PluginImp
	log            *zap.Logger
	name           string
	immediately    bool
	generator      SecDurationGenerator
	interruptChain chan bool
	isStartup      bool
	state          cbc.PluginState
	runFunc        func(ctx *cbc.Context)
}

// CreateIntervalLoopPlugin 固定間隔執行
//
//	@param name 名稱
//	@param amount 間隔數
//	@param duration 間隔單位
//	@param immediately 一開始馬上執行一次
//	@param runFunc 執行的內容
//	@return cbc.Plugin 建立的實體
func CreateIntervalLoopPlugin(name string, amount uint, duration time.Duration,
	immediately bool, runFunc func(ctx *cbc.Context)) cbc.Plugin {
	p := DurationLoopPlugin{
		name: name,
		generator: func(previous time.Time, now time.Time) time.Duration {
			runTime := now.Add(duration * time.Duration(amount))
			return time.Duration(runTime.Sub(now).Seconds()) * time.Second
		},
		immediately: immediately,
		runFunc:     runFunc,
	}
	return &p
}

func CreateFixedLoopPlugin(name string, hours []int, minutes []int,
	seconds []int, immediately bool, runFunc func(ctx *cbc.Context)) (cbc.Plugin, error) {
	durationFunc, durationFuncErr := createFixedDurationGenerator(hours, minutes, seconds)

	if durationFuncErr != nil {
		return nil, durationFuncErr
	}

	p := DurationLoopPlugin{
		name:        name,
		generator:   durationFunc,
		immediately: immediately,
		runFunc:     runFunc,
	}
	return &p, nil
}

func (p *DurationLoopPlugin) GetState() cbc.PluginState {
	return p.state
}

func (p *DurationLoopPlugin) Init(context *cbc.Context,
	cloader cbc.ConfigLoader, logger *cbc.Logger) {
	p.log = logger.Log.With(zap.String("source", "IntervalLoopPlugin."+p.name))
	p.log.Info("Interval Loop",
		zap.String("Name", p.name), zap.Bool("Immediately", p.immediately))
	p.interruptChain = make(chan bool)
	p.state = cbc.Enable
}

func (p *DurationLoopPlugin) GetName() string {
	return p.name
}

func (p *DurationLoopPlugin) Startup(cxt *cbc.Context) {
	if !p.isStartup && (p.state == cbc.Enable || p.state == cbc.Default) {
		p.log.Info("startup")

		go func(c chan bool) {
			if p.immediately {
				p.runFunc(cxt)
			}

			var (
				nowTime      time.Time
				previous     time.Time
				nextDuration time.Duration
			)

			timer := time.NewTimer(time.Hour)
			nextDuration = 0 * time.Second
			nowTime = time.Now()
			previous = nowTime

			for {
				nextDuration = p.generator(previous, nowTime)
				p.log.Info("Next Run", zap.String("Interval", nextDuration.String()))
				previous = previous.Add(nextDuration)
				timer.Reset(nextDuration)

				select {
				case <-timer.C:

				case bk := <-c:
					if bk {
						goto ForEnd
					}
				}

				p.log.Info("Run", zap.String("datetime", previous.Format(timeFormat)))
				p.runFunc(cxt)
				nowTime = time.Now()
			}

		ForEnd:

			p.log.Info("shutdown (interrupt)")
		}(p.interruptChain)
	}
}

func (p *DurationLoopPlugin) Shutdown() {
	if p.isStartup {
		p.interruptChain <- true
	}
}

func (p *DurationLoopPlugin) IsStartup() bool {
	return p.isStartup
}

func (p *DurationLoopPlugin) GetGenerator() SecDurationGenerator {
	return p.generator
}
