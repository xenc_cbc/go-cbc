package extra

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestCreateFixedDurationGeneratorWithCheckArgs(t *testing.T) {
	sg, sgErr := createFixedDurationGenerator(nil, nil, nil)

	assert.Nil(t, sg)
	assert.NotNil(t, sgErr)

	sg, sgErr = createFixedDurationGenerator(nil, nil, []int{0, 60})
	assert.Nil(t, sg)
	assert.NotNil(t, sgErr)

	sg, sgErr = createFixedDurationGenerator(nil, nil, []int{-1})
	assert.Nil(t, sg)
	assert.NotNil(t, sgErr)

	sg, sgErr = createFixedDurationGenerator(nil, []int{0, 60}, nil)
	assert.Nil(t, sg)
	assert.NotNil(t, sgErr)

	sg, sgErr = createFixedDurationGenerator(nil, []int{-1}, nil)
	assert.Nil(t, sg)
	assert.NotNil(t, sgErr)

	sg, sgErr = createFixedDurationGenerator([]int{0, 60}, nil, nil)
	assert.Nil(t, sg)
	assert.NotNil(t, sgErr)

	sg, sgErr = createFixedDurationGenerator([]int{-1}, nil, nil)
	assert.Nil(t, sg)
	assert.NotNil(t, sgErr)
}

func TestCreateFixedDurationGeneratorWithSec(t *testing.T) {
	sg, sgErr := createFixedDurationGenerator(nil, nil, []int{1, 15, 20})
	assert.Nil(t, sgErr)
	assert.NotNil(t, sg)

	var (
		now     time.Time
		runTime time.Time
	)

	now = time.Now()
	now = time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 2, 0, now.Location())

	v := sg(now, now)
	// 00:02 -> 00:15
	assert.Equal(t, time.Second*13, v)

	runTime = now.Add(time.Second * 15)
	v = sg(now, runTime)
	// 00:17 -> 00:20
	assert.Equal(t, time.Second*3, v)

	runTime = now.Add(time.Second * 20)
	v = sg(now, runTime)
	// 00:22 -> 01:01
	assert.Equal(t, time.Second*39, v)
}

// todo test min
// todo test hour

// todo test min + sec
// todo test hour + min + sec
// todo test hour + sec
// todo test hour + min
