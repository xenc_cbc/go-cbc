package cbc

import (
	"crypto/tls"
	"encoding/json"
	"strings"
	"time"

	"github.com/go-stomp/stomp"
	"github.com/go-stomp/stomp/frame"
	"go.uber.org/zap"
)

var _ FullPlugin = &Stomp{}

// Stomp go-stomp 的應用
type Stomp struct {
	PluginImp
	con    *stomp.Conn
	tlsCon *tls.Conn
	Config StompConfig
	log    *zap.Logger
	opts   []func(*stomp.Conn) error
}

func (sp *Stomp) GetName() string {
	return StompPluginName
}

// Init 初始化
func (sp *Stomp) Init(context *Context, cloader ConfigLoader, logger *Logger) {
	sp.InitWithFileName(context, cloader, "stomp.json", logger)
}

// InitWithFileName 初始化，可自定檔名
func (sp *Stomp) InitWithFileName(context *Context, cloader ConfigLoader,
	fileName string, logger *Logger) {
	sp.log = logger.Log.With(zap.String("source", "stomp"))
	jsonExist, jsonErr := cloader.DecodeConfig(fileName, &sp.Config)
	jsonFailFlag := jsonErr != nil

	if jsonFailFlag {
		sp.log.Info("json not found , Disable.", zap.String("file", fileName))
		sp.state = Failed
		return
	} else if !jsonExist {
		sp.state = Disable
		return
	}

	sp.log.Info("Init Info", zap.String("Protocol", sp.Config.Protocol),
		zap.String("Host", sp.Config.Host),
		zap.Bool("FixedConnection", sp.Config.FixedConnection),
		zap.Bool("SSL", sp.Config.SSL))

	if len(sp.Config.Username) > 0 {
		sp.log.Info("Login", zap.String("Username", sp.Config.Username),
			zap.String("Password", sp.Config.Password))
		sp.opts = append(sp.opts, stomp.ConnOpt.Login(sp.Config.Username, sp.Config.Password))
	}

	if len(sp.Config.Version) > 0 {
		sp.log.Info("Opt", zap.String("Version", sp.Config.Version))
		arr := strings.Split(sp.Config.Version, ",")

		for _, val := range arr {
			sp.opts = append(sp.opts, stomp.ConnOpt.AcceptVersion(stomp.Version(val)))
		}
	}

	if sp.Config.UseStomp {
		sp.log.Info("Opt", zap.Bool("UseStomp", sp.Config.UseStomp))
		sp.opts = append(sp.opts, stomp.ConnOpt.UseStomp)
	}

	if sp.Config.HeartBeatSendMinute != 0 && sp.Config.HeartBeatReceiveMinute != 0 {
		sp.log.Info("Opt", zap.Int("HeartBeatSendMinute", sp.Config.HeartBeatSendMinute),
			zap.Int("HeartBeatReceiveMinute", sp.Config.HeartBeatReceiveMinute))
		var (
			st time.Duration
			rt time.Duration
		)

		if sp.Config.HeartBeatSendMinute > 0 {
			st = time.Duration(sp.Config.HeartBeatSendMinute) * time.Minute
		} else {
			st = time.Duration(0)
		}

		if sp.Config.HeartBeatReceiveMinute > 0 {
			rt = time.Duration(sp.Config.HeartBeatReceiveMinute) * time.Minute
		} else {
			rt = time.Duration(0)
		}

		sp.opts = append(sp.opts, stomp.ConnOpt.HeartBeat(st, rt))
	}

	if sp.Config.HeartBeatErrorSec != 0 {
		sp.log.Info("Opt", zap.Int("HeartBeatErrorSec", sp.Config.HeartBeatErrorSec))
		st := time.Duration(sp.Config.HeartBeatErrorSec) * time.Second
		sp.opts = append(sp.opts, stomp.ConnOpt.HeartBeatError(st))
	}

	if sp.Config.MsgSendTimeoutSec != 0 {
		sp.log.Info("Opt", zap.Int("MsgSendTimeoutSec", sp.Config.MsgSendTimeoutSec))
		st := time.Duration(sp.Config.MsgSendTimeoutSec) * time.Second
		sp.opts = append(sp.opts, stomp.ConnOpt.MsgSendTimeout(st))
	}

	if sp.Config.HeartBeatGracePeriodMultiplier != 0 {
		sp.log.Info("Opt", zap.Float64("HeartBeatGracePeriodMultiplier", sp.Config.HeartBeatGracePeriodMultiplier))
		sp.opts = append(sp.opts,
			stomp.ConnOpt.HeartBeatGracePeriodMultiplier(
				sp.Config.HeartBeatGracePeriodMultiplier))
	}

	if sp.Config.ReadChannelCapacity != 0 {
		sp.log.Info("Opt", zap.Int("ReadChannelCapacity", sp.Config.ReadChannelCapacity))
		sp.opts = append(sp.opts, stomp.ConnOpt.ReadChannelCapacity(sp.Config.ReadChannelCapacity))
	}

	if sp.Config.WriteChannelCapacity != 0 {
		sp.log.Info("Opt", zap.Int("WriteChannelCapacity", sp.Config.WriteChannelCapacity))
		sp.opts = append(sp.opts, stomp.ConnOpt.WriteChannelCapacity(sp.Config.WriteChannelCapacity))
	}

	if len(sp.Config.Headers) > 0 {
		for _, val := range sp.Config.Headers {
			sp.log.Info("Opt", zap.String("Key", val.Key),
				zap.String("Value", val.Value))
			sp.opts = append(sp.opts, stomp.ConnOpt.Header(val.Key, val.Value))
		}
	}

	var err error
	sp.con, err = sp.NewCon()

	if err != nil {
		logger.Log.Error("Test Stomp Connect Failed", zap.Error(err))
		sp.state = Failed
	} else {
		logger.Log.Info("Test Stomp Connect Success.")
		sp.state = Enable
	}

	if !sp.Config.FixedConnection {
		defer sp.con.MustDisconnect()
	}
}

// Close 同 Disconnect
func (sp *Stomp) Close() {
	sp.Disconnect()
}

// Disconnect 關閉 Con() 的連線
//
//	createSingleConnection 是 false 的話，呼叫也沒關係
func (sp *Stomp) Disconnect() {
	if sp.con != nil {
		err := sp.con.Disconnect()

		if err != nil {
			sp.log.Error("Stomp Disconnect Failed.", zap.Error(err))
		} else {
			sp.log.Info("Stomp Disconnect Success.")
		}
	}

	if sp.Config.SSL && sp.tlsCon != nil {
		err := sp.tlsCon.Close()

		if err != nil {
			sp.log.Error("Stomp SSL Connection Disconnect Failed.", zap.Error(err))
		} else {
			sp.log.Info("Stomp SSL Connection Disconnect Success.")
		}
	}

	sp.state = Disable
}

// SendText 使用固定的連線 Send text/plain
func (sp *Stomp) SendText(destination, body string, opts ...func(*frame.Frame) error) error {
	return sp.con.Send(destination, "text/plain", []byte(body), opts...)
}

// SendTextWithCon 使用指定的連線 Send text/plain
func (sp *Stomp) SendTextWithCon(con *stomp.Conn, destination string, body string, opts ...func(*frame.Frame) error) error {
	return con.Send(destination, "text/plain", []byte(body), opts...)
}

// SendJSON 使用固定的連線 Send application/json
func (sp *Stomp) SendJSON(destination string, body interface{}, opts ...func(*frame.Frame) error) error {
	bodyBytes, jsonErr := json.Marshal(body)

	if jsonErr == nil {
		return sp.con.Send(destination, "application/json", bodyBytes, opts...)
	}

	return jsonErr
}

// SendJSONWithCon 使用指定的連線 Send application/json
func (sp *Stomp) SendJSONWithCon(con *stomp.Conn, destination string, body interface{}, opts ...func(*frame.Frame) error) error {
	bodyBytes, jsonErr := json.Marshal(body)

	if jsonErr == nil {
		return con.Send(destination, "application/json", bodyBytes, opts...)
	}

	return jsonErr
}

// Send 使用指定的連線 Send
func (sp *Stomp) Send(con *stomp.Conn, destination string, contentType string, body []byte, opts ...func(*frame.Frame) error) error {
	return con.Send(destination, contentType, body, opts...)
}

// Con 使用固定的連線
//
//	要設定 createSingleConnection 才會有值
func (sp *Stomp) Con() *stomp.Conn {
	return sp.con
}

// Subscribe 使用固定的連線 Subscribe
func (sp *Stomp) Subscribe(destination string, ack stomp.AckMode, opts ...func(*frame.Frame) error) (*stomp.Subscription, error) {
	return sp.con.Subscribe(destination, ack, opts...)
}

// NewCon 建立連線
//
//	使用設定檔中的 Opts
func (sp *Stomp) NewCon() (*stomp.Conn, error) {
	var (
		con *stomp.Conn
		err error
	)

	if sp.Config.SSL {
		var tlsCon *tls.Conn
		tlsCon, err = tls.Dial("tcp", sp.Config.Host, &tls.Config{InsecureSkipVerify: true})

		if err == nil {
			if len(sp.opts) == 0 {
				con, err = stomp.Connect(tlsCon)
			} else {
				con, err = stomp.Connect(tlsCon, sp.opts...)
			}
		}
	} else {
		if len(sp.opts) == 0 {
			con, err = stomp.Dial(sp.Config.Protocol, sp.Config.Host)
		} else {
			con, err = stomp.Dial(sp.Config.Protocol, sp.Config.Host, sp.opts...)
		}
	}

	return con, err
}

// NewConDoSomething 建立新連線並執行特定的 func，結束後自動執行 Disconnect
func (sp *Stomp) NewConDoSomething(f func(stomp *StompWrap, err error)) {
	con, err := sp.NewCon()

	if err == nil {
		f(&StompWrap{Con: con}, nil)
	} else {
		f(nil, err)
	}

	con.Disconnect()
}

// MustNewCon 建立連線
//
//	使用設定檔中的 Opts，有錯即 panic
func (sp *Stomp) MustNewCon() *stomp.Conn {
	con, err := sp.NewCon()

	if err != nil {
		panic(err)
	}

	return con
}

// NewConWithOpt 建立連線
//
//	不使用設定檔中的 Opts
func (sp *Stomp) NewConWithOpt(opts ...func(*stomp.Conn) error) (*stomp.Conn, error) {
	var (
		con *stomp.Conn
		err error
	)

	if len(opts) == 0 {
		con, err = stomp.Dial(sp.Config.Protocol, sp.Config.Host)
	} else {
		con, err = stomp.Dial(sp.Config.Protocol, sp.Config.Host, opts...)
	}

	return con, err
}

// MustNewConWithOpt 建立連線
//
//	不使用設定檔中的 Opts，有錯即 panic
func (sp *Stomp) MustNewConWithOpt(opts ...func(*stomp.Conn) error) *stomp.Conn {
	con, err := sp.NewConWithOpt(opts...)

	if err != nil {
		panic(err)
	}

	return con
}
