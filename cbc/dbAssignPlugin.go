package cbc

import "bitbucket.org/xenc_cbc/go-cbc/v2/cbc/sqlc"

// DbAssignPlugin 在一開始就要指定使用的 資料庫
type DbAssignPlugin interface {
	ThirdPlugin
	GetDatabaseName() string
}

// DbAssignTarget 設定資料庫來源的目標
type DbAssignTarget interface {
	SetDatabaseSource(db sqlc.DB)
}

var _ DbAssignPlugin = &DbAssignPluginImp{}

// DbAssignPluginImp DbAssignPlugin 的實作
type DbAssignPluginImp struct {
	PluginName   string
	state        PluginState
	Databasename string
	TargetArr    []DbAssignTarget
}

func (dap *DbAssignPluginImp) GetDatabaseName() string {
	return dap.Databasename
}

func (dap *DbAssignPluginImp) GetName() string {
	return dap.PluginName
}

func (dap *DbAssignPluginImp) GetState() PluginState {
	return dap.state
}

func (dap *DbAssignPluginImp) Init(context *Context, cloader ConfigLoader, logger *Logger) {
	if dap.state != Enable {
		for _, v := range dap.TargetArr {
			v.SetDatabaseSource(context.Database.ConMap[dap.Databasename])
		}

		dap.state = Enable
	}
}
