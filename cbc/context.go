package cbc

import (
	"bufio"
	"fmt"
	"html/template"
	"os"
	"path"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/arrayc"
	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/filec"
	"github.com/gin-gonic/gin"
	"github.com/gomodule/redigo/redis"
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
)

// Context 整合
type Context struct {
	Loader          ConfigLoader
	log             *Logger
	Database        *Database
	redis           *Redis
	gin             *Gin
	Socket          *WebSocket
	template        *Template
	ACL             *ACL
	Stomp           *Stomp
	Config          *Config
	ShutdownFuncArr []func() //	ShutdownFuncArr 會在 Shutdown 時執行，但其他功能(例如 DB)在這時還可以使用
	workDir         string
	pluginArr       []Plugin
	pluginMap       map[string]Plugin
	isInit          bool
	initLock        sync.Mutex
}

// NewContext 建立一個新的 Context
//
//	@param configLoader
//	@return *Context
func NewContext(configLoader ConfigLoader) *Context {
	cxt := &Context{}

	cxt.workDir, _ = filec.GetExecutableFolder()
	cxt.pluginMap = make(map[string]Plugin)
	cxt.Loader = configLoader

	cxt.log = &Logger{}
	cxt.pluginArr = make([]Plugin, 0)

	cxt.pluginMap[cxt.log.GetName()] = cxt.log
	cxt.pluginArr = append(cxt.pluginArr, cxt.log)
	cxt.log.Init(cxt, cxt.Loader)
	_, cxt.Config = cxt.Loader.LoadConfig("config.json")

	if cxt.IsDebug() {
		for k, v := range *cxt.Config {
			cxt.log.Log.Debug("Config", zap.String("key", k), zap.Any("value", v))
		}
	}

	cxt.Database = &Database{}
	cxt.Stomp = &Stomp{}
	cxt.redis = &Redis{}
	cxt.gin = &Gin{}
	cxt.Socket = &WebSocket{}
	cxt.ACL = &ACL{}
	cxt.template = &Template{}

	cxt.addPlugin(cxt.Database)
	cxt.addPlugin(cxt.Stomp)
	cxt.addPlugin(cxt.redis)
	cxt.addPlugin(cxt.gin)
	cxt.addPlugin(cxt.Socket)
	cxt.addPlugin(cxt.ACL)
	cxt.addPlugin(cxt.template)

	return cxt
}

// Init 初始化
//
//	@receiver cxt
//	@return initResult 初始化
//	@return initErr 初始化的錯誤
func (cxt *Context) Init() (initResult bool, initErr error) {
	cxt.initLock.Lock()

	if cxt.isInit {
		cxt.log.Log.Info("Already Init")
		return
	}

	defer func() {
		cxt.initLock.Unlock()

		if r := recover(); r != nil {
			fmt.Println("Context init error")
			fmt.Println(r)
			initErr = r.(error)
			cxt.CloseResource()
		}
	}()

	for _, p := range cxt.pluginArr {
		if initPlugin, isInit := p.(PluginWithInit); isInit {
			_, isThirdPlugin := p.(ThirdPlugin)
			cxt.log.Log.Info("Plugin Init", zap.String("Name", p.GetName()),
				zap.Bool("third", isThirdPlugin))
			initPlugin.Init(cxt, cxt.Loader, cxt.log)
		}
	}

	if len(strings.Trim(cxt.ACL.Config.Hostname, " ")) == 0 {
		cxt.ACL.Config.Hostname = cxt.gin.Config.ServerName
	}

	cxt.isInit = true
	initResult = true
	return
}

// IsInit 是否已是初始化完成的狀態
//
//	@receiver cxt
//	@return bool
func (cxt *Context) IsInit() bool {
	return cxt.isInit
}

// addPlugin 加入 plugin
//
//	@receiver cxt
//	@param plugin
func (cxt *Context) addPlugin(plugin Plugin) {
	cxt.log.Log.Info("Add Plugin", zap.String("Name", plugin.GetName()))
	cxt.pluginMap[plugin.GetName()] = plugin
	cxt.pluginArr = append(cxt.pluginArr, plugin)
}

// GetPlugin 取得指定的 Plugin
//
//	@receiver cxt
//	@param name plugin 的名稱
//	@return Plugin
//	@return bool 是否存在和名稱對應的 plugin
func (cxt *Context) GetPlugin(name string) (Plugin, bool) {
	v, ok := cxt.pluginMap[name]
	return v, ok
}

// RemovePlugin 移除並關閉對應的 plugin
//
//	@receiver cxt
//	@param name plugin 的 name
//	@return Plugin 被移除的 plugin
//	@return bool 是否有存在對應的 plugin
func (cxt *Context) RemovePlugin(name string) (Plugin, bool) {
	cxt.initLock.Lock()

	defer cxt.initLock.Unlock()

	v, ok := cxt.pluginMap[name]

	if ok {
		cxt.closePlugin(v)
		delete(cxt.pluginMap, name)
		pluginIndex := -1

		for index, val := range cxt.pluginArr {
			if val.GetName() == name {
				pluginIndex = index
				break
			}
		}

		if pluginIndex >= 0 {
			cxt.pluginArr = arrayc.DeleteWithIndex(cxt.pluginArr, pluginIndex)
		}
	}

	return v, ok
}

func (cxt *Context) closePlugin(plugin Plugin) bool {
	closeP, isClose := plugin.(PluginWithClose)

	if isClose {
		if closeP.GetState() == Enable {
			cxt.log.Log.Info("Plugin Close", zap.String("Name", closeP.GetName()))
			closeP.Close()
		}
	}

	return isClose
}

// GetThirdPlugin 取得額外加入的 plugin
//
//	@receiver cxt
//	@param name 名稱會在前方自動加上 ThirdPluginNamePrefix
//	@return ThirdPlugin
//	@return bool
func (cxt *Context) GetThirdPlugin(name string) (ThirdPlugin, bool) {
	v, ok := cxt.pluginMap[ThirdPluginNamePrefix+name]

	if ok {
		tp, tpOk := v.(ThirdPlugin)
		return tp, tpOk
	}

	return nil, ok
}

// AddThirdPlugin 加入額外的 plugin
//
//	@receiver cxt
//	@param plugin 註冊的名稱會在前方自動加上 ThirdPluginNamePrefix
func (cxt *Context) AddThirdPlugin(plugin ThirdPlugin) {
	cxt.initLock.Lock()

	defer func() {
		cxt.initLock.Unlock()

		if r := recover(); r != nil {
			cxt.CloseResource()
			panic(r)
		}
	}()

	pluginName := ThirdPluginNamePrefix + plugin.GetName()
	_, ok := cxt.pluginMap[pluginName]

	if ok {
		cxt.log.Log.Info("Third Plugin exist", zap.String("Name", pluginName))
	} else {
		cxt.log.Log.Info("Third Plugin Add", zap.String("Name", pluginName))
		cxt.pluginMap[pluginName] = plugin
		cxt.pluginArr = append(cxt.pluginArr, plugin)

		if cxt.isInit {
			cxt.log.Log.Info("Third Plugin Init", zap.String("Name", pluginName))
			if initPlugin, isInit := plugin.(PluginWithInit); isInit {
				initPlugin.Init(cxt, cxt.Loader, cxt.log)
			}
		}

		if startupPlugin, isStartup := plugin.(PluginWithStartup); isStartup {
			cxt.log.Log.Info("Third Plugin Startup", zap.String("Name", plugin.GetName()))
			startupPlugin.Startup(cxt)
		}
	}
}

// CheckPluginState 如果全部 Plugin 都不為 None 或是 Failed
func (cxt *Context) CheckPluginState() bool {
	rs := true

CheckLoop:
	for _, p := range cxt.pluginArr {

		switch p.GetState() {
		case None:
			fallthrough
		case Failed:
			rs = false
			cxt.log.Log.Info("Check Plugin State", zap.String("Name", p.GetName()),
				zap.String("State", p.GetState().ToString()))
			break CheckLoop
		}
	}

	return rs
}

// ShowPluginState 印出目前 Plugin 的狀態
func (cxt *Context) ShowPluginState() {
	hasLogger := cxt.log != nil && cxt.log.state != None

	for _, p := range cxt.pluginArr {
		var (
			stateTxt string
			init     bool
			close    bool
		)

		_, init = p.(PluginWithInit)
		_, close = p.(PluginWithClose)

		stateTxt = p.GetState().ToString()

		if hasLogger {
			cxt.log.Log.Info("Plugin",
				zap.String("Name", p.GetName()), zap.String("State", stateTxt),
				zap.Bool("Init", init), zap.Bool("Close", close))
		} else {
			fmt.Printf("%s : %s , Init : %t , Close : %t", p.GetName(), stateTxt, init, close)
			fmt.Println()
		}

	}
}

// IsDebug is debug mode
func (cxt *Context) IsDebug() bool {
	return cxt.log.IsDebug()
}

// ActiveACL Active gin ACL
func (cxt *Context) ActiveACL() *ACL {
	cxt.ACL.Active(cxt.gin.Router)
	return cxt.ACL
}

// ActiveACL Active gin template engine
func (cxt *Context) ActiveTemplate(extFuncMap template.FuncMap) *Template {
	cxt.template.Active(cxt.gin.Router, extFuncMap)
	return cxt.template
}

// GetStorePath Web Store Path
func (cxt *Context) GetStorePath() string {
	return cxt.gin.Config.StorePath
}

// LoggerWrap Logger
func (cxt *Context) LoggerWrap() *Logger {
	return cxt.log
}

// Logger zap.Logger
func (cxt *Context) Logger() *zap.Logger {
	return cxt.log.Log
}

// Redis redis Pool
func (cxt *Context) Redis() *redis.Pool {
	return cxt.redis.RedisPool
}

// Gin Gin Web Framework
//
//	@receiver cxt
//	@return *Gin
func (cxt *Context) Gin() *Gin {
	return cxt.gin
}

// DB Database Connection
func (cxt *Context) DB() *sqlx.DB {
	return cxt.Database.Con
}

// Router gin Router
func (cxt *Context) Router() *gin.Engine {
	return cxt.gin.Router
}

// ServerConfig 和 Server 相關的設定
func (cxt *Context) ServerConfig() ServerConfig {
	return cxt.gin.Config
}

// CloseResource 關閉資源
//
//	@receiver cxt
//	@return closeRs 是否全部都成功執行
//	@return closeErr 關閉途中產生的錯誤
func (cxt *Context) CloseResource() (closeRs bool, closeErr error) {
	cxt.initLock.Lock()

	defer func() {
		cxt.initLock.Unlock()

		if r := recover(); r != nil {
			fmt.Println("Context CloseResource error")
			fmt.Println(r)
			closeErr = r.(error)
		}
	}()

	plugArrLen := len(cxt.pluginArr) - 1

	for i := plugArrLen; i >= 0; i-- {
		cxt.closePlugin(cxt.pluginArr[i])
	}

	cxt.isInit = false
	closeRs = true
	return
}

// StartCmdMode 可以輸入指令，預設 q 為結束循環，中止時會自動呼叫 CloseResource
//
//	inputFunc 會傳入鍵盤 enter 輸入的文字和可以停止循環的 func
//	其他參數設定和 StartLoopMode 相同，但可以透過 scanner 輸入指令
func (cxt *Context) StartCmdMode(inputFunc func(keyin string, stopFunc func()),
	loopFunc func(<-chan bool), runHourPoint []int, immediately bool) {
	log := cxt.log.Log.With(zap.String("source", "CmdMode"))
	stopFunc, fchan := cxt.StartLoopMode(loopFunc, runHourPoint, immediately)
	scanner := bufio.NewScanner(os.Stdin)
	runFlag := true

	for runFlag && scanner.Scan() {
		keyin := strings.Trim(scanner.Text(), "")

		if keyin == "q" {
			break
		} else {
			inputFunc(keyin, func() {
				runFlag = false
			})
		}
	}

	log.Info("Trigger Interrupt Loop")
	stopFunc()
	log.Info("Wait Loop Finish ...")

	a := <-fchan

	log.Info("Loop Finished", zap.Bool("Result", a))
	log.Info("End CMD Mode")
}

// DoPluginStartup 啟動執行 plugin 的額外運作 (應該給 Context 自行呼叫)
func (cxt *Context) DoPluginStartup() {
	for _, p := range cxt.pluginArr {
		if startupPlugin, isStartup := p.(PluginWithStartup); isStartup {
			_, isThirdPlugin := p.(ThirdPlugin)
			cxt.log.Log.Info("Plugin Startup", zap.String("Name", p.GetName()),
				zap.Bool("third", isThirdPlugin))
			startupPlugin.Startup(cxt)
		}
	}
}

// DoPluginShutdown 停止執行 plugin 的額外運作 (應該給 Context 自行呼叫)
func (cxt *Context) DoPluginShutdown() {
	for _, p := range cxt.pluginArr {
		if startupPlugin, isStartup := p.(PluginWithStartup); isStartup {
			_, isThirdPlugin := p.(ThirdPlugin)
			cxt.log.Log.Info("Plugin Shutdown", zap.String("Name", p.GetName()),
				zap.Bool("third", isThirdPlugin))
			startupPlugin.Shutdown()
		}
	}
}

// StartLoopMode 在指定的幾點執行 loopFunc func 中止，中止時會自動呼叫 CloseResource
//
//	loopFunc 傳入的 chan 為判別是否有呼叫中止 loop 的循環判斷
//	immediately 是否要馬上執行一次
//	回傳的第一個 func 呼叫即中止 loop 的循環判斷 (已在執行的，會等他跑完)
//	回傳的第二個 chan 讀取到任意值為 loop 中的執行都完成了 (可確保沒有跑到一半的)
func (cxt *Context) StartLoopMode(loopFunc func(<-chan bool), runHourPoint []int, immediately bool) (func(), <-chan bool) {
	log := cxt.log.Log.With(zap.String("source", "LoopMode"))
	timeFormat := "2006-01-02 15:04:05"
	interruptChain := make(chan bool)
	finishedChain := make(chan bool)
	interruptLoopFuncChain := make(chan bool)

	go func() {
		cxt.DoPluginStartup()

		if immediately && loopFunc != nil {
			if loopFunc == nil {
				log.Info("Run func is nil")
			} else {
				log.Info("First Run")
				loopFunc(interruptLoopFuncChain)
			}
		}

		go func(c chan bool) {
			runTime := time.Now()
			runTime = time.Date(runTime.Year(), runTime.Month(), runTime.Day(), runTime.Hour(), 0, 0, 0, runTime.Location())
			timer := time.NewTimer(time.Second)

			for {
				var (
					nextHour = -1
					nextTime time.Time
					nowTime  = time.Now()
				)

				for _, pointHour := range runHourPoint {
					if runTime.Hour() < pointHour {
						nextHour = pointHour
					}
				}

				if nextHour >= 0 {
					runTime = runTime.AddDate(0, 0, 1)
					nextTime = time.Date(runTime.Year(), runTime.Month(), runTime.Day(), nextHour, 0, 0, 0, runTime.Location())
					t := nextTime.Sub(nowTime)
					runTime = nextTime
					log.Info(nextTime.Format(timeFormat), zap.String("Type", "Next Run"), zap.String("Interval", t.String()))
					timer.Reset(t)
				}

				select {
				case <-timer.C:
				case bk := <-interruptChain:
					if bk {
						goto ForEnd
					}
				}

				if loopFunc == nil {
					log.Info("Run func is nil")
				} else {
					loopFunc(interruptLoopFuncChain)
				}
			}

		ForEnd:
			log.Info("Interrupt Loop OK")

			if len(cxt.ShutdownFuncArr) > 0 {
				for _, f := range cxt.ShutdownFuncArr {
					f()
				}
			}

			cxt.DoPluginShutdown()

			go func() {
				cxt.CloseResource()
				finishedChain <- true
			}()
		}(interruptChain)
	}()

	return func() {
		go func() {
			interruptLoopFuncChain <- true
		}()
		go func() {
			interruptChain <- true
		}()
	}, finishedChain
}

// GetWorkPath 取得目前執行檔的所在位置路徑
func (cxt *Context) GetWorkPath() string {
	return cxt.workDir
}

// ConvertPath 如果不是 . 開頭或第二個字元是 : 的路徑都在前方加上 WorkPath
func (cxt *Context) ConvertPath(pathStr string) string {
	if !filepath.IsAbs(pathStr) {
		pathStr = path.Join(cxt.GetWorkPath(), pathStr)
	}

	return pathStr
}

// ParsePath 如果是 file: 開頭去除 file: 開頭回傳，如果不是的話呼叫 ConvertPath
//
//	@receiver cxt
//	@param pathStr
//	@return string
func (cxt *Context) ParsePath(pathStr string) string {
	if realPath, isFilePrefix := filec.IsFilePrefixPath(pathStr); isFilePrefix {
		return realPath
	} else {
		return cxt.ConvertPath(pathStr)
	}
}
