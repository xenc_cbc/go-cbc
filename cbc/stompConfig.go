package cbc

// StompConfig stomp 設定
type StompConfig struct {
	SSL                            bool          `json:"ssl"`
	Protocol                       string        `json:"protocol"`
	Host                           string        `json:"host"`
	Username                       string        `json:"username"`
	Password                       string        `json:"password"`
	Version                        string        `json:"version"`
	UseStomp                       bool          `json:"useStomp"`
	HeartBeatSendMinute            int           `json:"heartBeatSendMinute"`
	HeartBeatReceiveMinute         int           `json:"heartBeatReceiveMinute"`
	HeartBeatErrorSec              int           `json:"heartBeatErrorSec"`
	MsgSendTimeoutSec              int           `json:"msgSendTimeoutSec"`
	HeartBeatGracePeriodMultiplier float64       `json:"heartBeatGracePeriodMultiplier"`
	Headers                        []StompHeader `json:"headers"`
	ReadChannelCapacity            int           `json:"readChannelCapacity"`
	WriteChannelCapacity           int           `json:"writeChannelCapacity"`
	FixedConnection                bool          `json:"fixedConnection"`
}

// StompHeader stomp 的 header
type StompHeader struct {
	Key   string `json:"key"`
	Value string `json:"val"`
}
