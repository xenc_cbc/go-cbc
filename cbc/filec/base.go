package filec

import (
	"bufio"
	"fmt"
	"image"
	"image/color"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/utilc"
	"github.com/disintegration/imaging"
)

var fileNameRegx *regexp.Regexp

func init() {
	fileNameRegx, _ = regexp.Compile("[\\/?:<>*\"|]")
}

// GetFileExtension 取得副檔名，如果沒有副檔名，第二個回傳值會傳回 false
func GetFileExtension(fileName string) (string, bool) {
	eName := filepath.Ext(fileName)

	if eName == "" {
		return eName, false
	}

	return eName[1:], true
}

// GetFileNameWithoutExtension 取得檔名，如果有副檔名的話，則去除
func GetFileNameWithoutExtension(fileName string) string {
	return strings.TrimSuffix(fileName, filepath.Ext(fileName))
}

// AppendFileExtension 加上指定的副檔名，如果副檔名為空字串則略過
func AppendFileExtension(path string, extension string) string {
	if extension == "" {
		return path
	}

	return path + "." + extension
}

// GetRandomFileName 同 GetRandomName 但會加上指定的副檔名
//
//	副檔名為空字串 "" 的話，會略過副檔名的處理
func GetRandomFileName(extension string) string {
	rn := utilc.GetRandomName()

	if extension == "" {
		return rn
	}

	return rn + "." + extension
}

// FileNameToRandom 同 GetRandomFileName 會使用傳入的 fileName 解析出副檔名
func FileNameToRandom(fileName string) string {
	eName, _ := GetFileExtension(fileName)
	return GetRandomFileName(eName)
}

// ReadFileToChannelByLine reads a text file line by line into a channel.
//
//	c, err := fileutil.ReadFileToChannelByLine(fileName)
//	if err != nil {
//	   log.Fatalf("readLines: %s\n", err)
//	}
//	for line := range c {
//	   fmt.Printf("  Line: %s\n", line)
//	}
//
// nil is returned (with the error) if there is an error opening the file
func ReadFileToChannelByLine(filePath string) (<-chan string, error) {
	c := make(chan string)
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	go func() {
		defer file.Close()
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			c <- scanner.Text()
		}
		close(c)
	}()
	return c, nil
}

// ReadFileToSliceByLine reads a text file line by line into a slice of strings.
// Not recommended for use with very large files due to the memory needed.
//
//	lines, err := fileutil.ReadFileToSliceByLine(filePath)
//	if err != nil {
//	    log.Fatalf("readLines: %s\n", err)
//	}
//	for i, line := range lines {
//	    fmt.Printf("  Line: %d %s\n", i, line)
//	}
//
// nil is returned if there is an error opening the file
func ReadFileToSliceByLine(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

// WriteSliceToFile writes the given slice of lines to the given file.
func WriteSliceToFile(lines []string, path string) error {
	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()

	w := bufio.NewWriter(file)
	for _, line := range lines {
		fmt.Fprintln(w, line)
	}
	return w.Flush()
}

// PathExist returns whether or not the given file or directory exists
func PathExist(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return false
}

// FileExist 檔案是否存在
func FileExist(path string) bool {
	fs, err := os.Stat(path)

	if err == nil && !fs.IsDir() {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return false
}

// DirectoryExist 目錄是否存在
func DirectoryExist(path string) bool {
	fs, err := os.Stat(path)

	if err == nil && fs.IsDir() {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return false
}

// CheckAndCreatePath 取得 path 上一層的目錄，如果不存在則建立目錄
func CheckAndCreatePath(path string) {
	dirPath := filepath.Dir(path)
	CheckAndCreateDir(dirPath)
}

// CheckAndCreateDir 如果不存在則建立目錄
func CheckAndCreateDir(dirPath string) {
	if !PathExist(dirPath) {
		os.MkdirAll(dirPath, os.ModePerm)
	}
}

// GetFilePathWithExtension 會使用傳入的 fileName 解析出副檔名和 pathName Join 出 filePath
//
//	1.jpg , a , b , c => a/b/c.jpg
func GetFilePathWithExtension(fileName string, pathName ...string) string {
	extName, _ := GetFileExtension(fileName)
	path := filepath.Join(pathName...)
	return AppendFileExtension(path, extName)
}

// CopyFileByIoCopy 使用 io.Copy 複製檔案
func CopyFileByIoCopy(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}

// GetExecutableFolder 取得目前執行的目錄(執行檔)
func GetExecutableFolder() (string, error) {
	p, err := os.Executable()

	if err != nil {
		return "", err
	}

	return filepath.Dir(filepath.Clean(p)), nil
}

// FileNameFilter 把 \\/?:<>*\"| 取代成空白
func FileNameFilter(fileName string) string {
	return FileNameFilterWith(fileName, " ")
}

// FileNameFilter 把 \\/?:<>*\"| 取代成指定的文字
func FileNameFilterWith(fileName string, repl string) string {
	return fileNameRegx.ReplaceAllString(fileName, repl)
}

// DownloadFile 透過 http 下載檔案
func DownloadFile(savePath string, url string, overrideWrite bool) error {
	if !overrideWrite {
		if PathExist(savePath) {
			return nil
		}
	}

	out, err := os.Create(savePath)

	if err == nil {
		defer out.Close()
		resp, resErr := http.Get(url)

		if resErr == nil {
			defer resp.Body.Close()
			_, ioErr := io.Copy(out, resp.Body)

			if ioErr != nil {
				err = ioErr
			}
		} else {
			err = resErr
		}
	}

	return err
}

// DownloadFileWith 透過 http 下載檔案
func DownloadFileWith(client *http.Client, savePath string, url string, overrideWrite bool) error {
	if !overrideWrite {
		if PathExist(savePath) {
			return nil
		}
	}

	out, err := os.Create(savePath)

	if err == nil {
		defer out.Close()
		resp, resErr := client.Get(url)

		if resErr == nil {
			defer resp.Body.Close()
			_, ioErr := io.Copy(out, resp.Body)

			if ioErr != nil {
				err = ioErr
			}
		} else {
			err = resErr
		}
	}

	return err
}

// CreateThumbnail 建立圖片的縮圖，格式和原圖一樣
//
//	width 或 height 其中一個可以指定為 0 表示依另一項數值為準等比例縮放
func CreateThumbnail(srcPath string, storePath string, width int, height int) error {
	src, err := imaging.Open(srcPath)

	if err != nil {
		return err
	}

	src = imaging.Resize(src, width, height, imaging.Lanczos)
	err = imaging.Save(src, storePath)

	if err != nil {
		return err
	}

	return nil
}

// CreateThumbnailWithFixedSize 建立圖片的縮圖，格式和原圖一樣。
//
//	為固定尺寸，如果等比縮放後，有不足的地方，會以指定的背景色 RGBA 填滿。
func CreateThumbnailWithFixedSize(srcPath string, storePath string, width int, height int,
	r uint8, g uint8, b uint8, a uint8) error {
	src, err := imaging.Open(srcPath)

	if err != nil {
		return err
	}

	var (
		reX int
		reY int
	)
	srcRec := src.Bounds()
	useWidth := srcRec.Dx() > srcRec.Dy()

	if useWidth {
		src = imaging.Resize(src, width, 0, imaging.Lanczos)
		srcRec = src.Bounds()
		reY = height - srcRec.Dy()

		if reY > 0 {
			reY = reY / 2
		}
	} else {
		src = imaging.Resize(src, 0, height, imaging.Lanczos)
		srcRec = src.Bounds()
		reX = width - srcRec.Dx()

		if reX > 0 {
			reX = reX / 2
		}
	}

	dst := imaging.New(width, height, color.RGBA{r, g, b, a})
	dst = imaging.Paste(dst, src, image.Pt(reX, reY))
	err = imaging.Save(dst, storePath)

	if err != nil {
		return err
	}

	return nil
}

// IsFilePrefixPath 檢查 path 是否為 file: 開頭的文字
//
//	@param path
//	@return string 如果是的話，此字串為去掉開頭的 file:，不然就是空字串
//	@return bool
func IsFilePrefixPath(path string) (string, bool) {
	if idx := strings.Index(path, "file:"); idx == 0 {
		return strings.Replace(path, "file:", "", 1), true
	}

	return "", false
}
