package cbc

// TemplateConfig 設定檔
type TemplateConfig struct {
	Root      string   `json:"root"`
	Extension string   `json:"extension"`
	Master    string   `json:"master"`
	Partials  []string `json:"partials"`
	Cache     bool     `json:"cache"`
	Delims    string   `json:"delims"`
}
