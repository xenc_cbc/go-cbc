package cbc

import (
	"html/template"
	"strings"

	"github.com/foolin/goview"
	"github.com/foolin/goview/supports/ginview"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

var _ PluginWithInit = &Template{}

// Template 樣版
type Template struct {
	PluginImp
	Engine  *ginview.ViewEngine
	Config  TemplateConfig
	funcMap template.FuncMap
}

func (tpls *Template) GetName() string {
	return TemplatePluginName
}

// Init 初始化
func (tpls *Template) Init(context *Context, cloader ConfigLoader,
	logger *Logger) {
	log := logger.Log.With(zap.String("source", "template"))
	jsonExist, jsonErr := cloader.DecodeConfig("template.json", &tpls.Config)

	if jsonErr != nil {
		log.Info("json error")
		tpls.state = Failed
		return
	} else if !jsonExist {
		log.Info("json not found , Disable.")
		tpls.state = Disable
		return
	}

	tpls.Config.Root = context.ConvertPath(tpls.Config.Root)

	log.Info("Info", zap.String("Root", tpls.Config.Root),
		zap.String("Extension", tpls.Config.Extension),
		zap.String("Master", tpls.Config.Master),
		zap.String("Delims", tpls.Config.Delims),
		zap.String("Partials", strings.Join(tpls.Config.Partials, ",")),
		zap.Bool("Cache", tpls.Config.Cache))

	tpls.state = Enable
}

// Active Template 啟用
func (tpls *Template) Active(router *gin.Engine, extFuncMap template.FuncMap) {
	tpls.funcMap = template.FuncMap{}

	for k, v := range extFuncMap {
		tpls.funcMap[k] = v
	}

	var delims goview.Delims

	delimsArr := strings.Split(tpls.Config.Delims, ",")

	if len(delimsArr) != 2 {
		delims = goview.DefaultConfig.Delims
	} else {
		delims = goview.Delims{Left: delimsArr[0], Right: delimsArr[1]}
	}

	tpls.Engine = ginview.New(goview.Config{
		Root:         tpls.Config.Root,
		Extension:    "." + tpls.Config.Extension,
		Master:       tpls.Config.Master,
		Partials:     tpls.Config.Partials,
		Funcs:        tpls.funcMap,
		Delims:       delims,
		DisableCache: !tpls.Config.Cache})

	router.HTMLRender = tpls.Engine
}
