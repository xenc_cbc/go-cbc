package cbc

import (
	"errors"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
)

var _ FullPlugin = &Database{}

// Database 資料庫
type Database struct {
	PluginImp
	Con    *sqlx.DB
	ConMap map[string]*sqlx.DB
	Config DatatbaseConfig
	log    *zap.Logger
}

func (dbc *Database) GetName() string {
	return DatabasePluginName
}

// Init 初始化 (database.json)
func (dbc *Database) Init(context *Context, cloader ConfigLoader, logger *Logger) {
	dbc.InitWithFileName(context, cloader, "database.json", logger)
}

// InitWithFileName 初始化，可自定檔名
func (dbc *Database) InitWithFileName(context *Context, cloader ConfigLoader,
	fileName string, logger *Logger) *sqlx.DB {
	dbc.log = logger.Log.With(zap.String("source", "database"))
	dbc.ConMap = make(map[string]*sqlx.DB)
	jsonExist, jsonErr := cloader.DecodeConfig(fileName, &dbc.Config)

	if jsonErr != nil {
		dbc.log.Info("Load JSON Error", zap.String("file", fileName))
		dbc.state = Failed
		return nil
	}

	if !jsonExist {
		dbc.state = Disable
		return nil
	}

	dbc.Con = dbc.InitDatabase(dbc.Config, logger)
	dbc.ConMap["default"] = dbc.Con

	childErrFlag := false

	for i := 0; i < len(dbc.Config.OtherDatabase); i++ {
		otherDbName := dbc.Config.OtherDatabase[i]
		lowerName := strings.ToLower(otherDbName)
		otherFileName := "database." + lowerName + ".json"
		dbc.log.Info("Init Other Database", zap.String("Name", lowerName))
		var cfg DatatbaseConfig

		jsonExist, jsonErr := cloader.DecodeConfig(otherFileName, &cfg)

		if jsonErr != nil {
			dbc.log.Fatal("child db json error", zap.String("file", otherFileName))
			childErrFlag = true
			break
		} else if !jsonExist {
			dbc.log.Fatal("child db json not found", zap.String("file", otherFileName))
			childErrFlag = true
			break
		} else {
			odb := dbc.InitDatabase(cfg, logger)
			dbc.ConMap[lowerName] = odb
		}
	}

	if childErrFlag {
		dbc.state = Failed
		defer dbc.Close()
	} else {
		dbc.state = Enable
	}

	return dbc.Con
}

func (dbc *Database) Close() {
	if dbc.Con != nil {
		dbc.Con.Close()
	}

	for _, v := range dbc.ConMap {
		v.Close()
	}

	dbc.state = Disable
}

func (dbc *Database) InitDatabaseWithName(name string, config DatatbaseConfig, logger *Logger) *sqlx.DB {
	odb := dbc.InitDatabase(config, logger)
	lowerName := strings.ToLower(name)
	dbc.ConMap[lowerName] = odb
	return odb
}

func (dbc *Database) InitDatabase(config DatatbaseConfig, logger *Logger) *sqlx.DB {
	var (
		connStr string
		err     error
		con     *sqlx.DB
	)

	if logger.IsDebug() {
		connStr = config.ConTestStr
	} else {
		connStr = config.ConStr
	}

	dbc.log.Info("Init Info", zap.String("Type", config.Type), zap.Int("MaxOpen", config.MaxOpen),
		zap.Int("MaxIdle", config.MaxIdle), zap.Int("MaxLife", config.MaxLife),
		zap.Bool("Use Test DB", logger.IsDebug()))
	con, err = sqlx.Open(config.Type, connStr)

	if err != nil {
		logger.Log.Error("Database Connect Failed", zap.Error(err))
		panic(errors.New("Database Connect Failed"))
	}

	con.SetMaxOpenConns(config.MaxOpen)
	con.SetMaxIdleConns(config.MaxIdle)
	con.SetConnMaxLifetime(time.Duration(config.MaxLife) * time.Second)

	return con
}
