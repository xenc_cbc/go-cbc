package sqlc

import (
	"database/sql"

	"github.com/jmoiron/sqlx"
)

// IsNoRow 是否為 sql.ErrNoRows
func IsNoRow(err *error) bool {
	return err != nil && *err == sql.ErrNoRows
}

// CheckResultChanged 確認是不是沒有資料變動，如果沒有問題回傳 true
func CheckResultChanged(rs *sql.Result) bool {
	if count, _ := (*rs).RowsAffected(); count == 0 {
		return false
	}

	return true
}

// CheckOtherError 確認除了沒有資料之外(略過 NoRow)是不是有其他的錯誤，如果沒有問題回傳 true
func CheckOtherError(scanErr *error) bool {
	if IsNoRow(scanErr) || *scanErr == nil {
		return true
	}

	return false
}

// MustScan 沒 error 的話 return true，是 NoRow 則為 false，如果是其他的 error 則呼叫 panic。
func MustScan(row *sqlx.Row, object ...interface{}) bool {
	err := row.Scan(object...)

	if err == nil {
		return true
	}

	if IsNoRow(&err) {
		return false
	}

	panic(err)
}

// MustGet 沒 error 的話 return true，是 NoRow 則為 false，如果是其他的 error 則呼叫 panic
func MustGet(dbCon *sqlx.DB, object interface{}, sql string, params ...interface{}) bool {
	err := dbCon.Get(object, sql, params...)

	if err == nil {
		return true
	}

	if IsNoRow(&err) {
		return false
	}

	panic(err)
}

// MustGetx 沒 error 的話 return true，是 NoRow 則為 false，如果是其他的 error 則呼叫 panic
func MustGetx(dbCon DB, object interface{}, sql string, params ...interface{}) bool {
	err := dbCon.Get(object, sql, params...)

	if err == nil {
		return true
	}

	if IsNoRow(&err) {
		return false
	}

	panic(err)
}

// MustSelect 只要有 error(略過 NoRow) 都呼叫 painc
func MustSelect(dbCon *sqlx.DB, object interface{}, sql string, params ...interface{}) {
	err := dbCon.Select(object, sql, params...)
	PanicOtherError(&err)
}

// MustSelectx 只要有 error(略過 NoRow) 都呼叫 painc
func MustSelectx(dbCon DB, object interface{}, sql string, params ...interface{}) {
	err := dbCon.Select(object, sql, params...)
	PanicOtherError(&err)
}

// PanicOtherError 只要有 error(略過 NoRow) 都呼叫 painc
func PanicOtherError(err *error) {
	if !IsNoRow(err) && *err != nil {
		panic(*err)
	}
}

// UseTx 開始 tx
// defer rollback , 接著呼叫 exeFunc , 最後依 exeFunc 回傳的 error 決定是否執行 commit。
//
// 另一項回傳的 error exeFunc 回傳的 或是 Commit 產生，如果 Commit 沒有錯誤的話就是 nil
// 所以如果 exeFunc 為 nil , Commit 沒有問題，回傳的就是 true,nil
func UseTx(dbCon *sqlx.DB, exeFunc func(txDbCon DB) error) (result bool, err error) {
	tx := dbCon.MustBegin()

	defer tx.Rollback()
	err = exeFunc(tx)
	result = err == nil

	if result {
		err = tx.Commit()
		result = err == nil
	}

	return
}

// MustExecx 只要有 error 都呼叫 painc，最後回傳異動數量 (RowsAffected 有錯誤的話也會為 0)
func MustExecx(dbCon DB, sql string, params ...interface{}) int64 {
	rs := dbCon.MustExec(sql, params...)
	c, e := rs.RowsAffected()

	if e != nil {
		return 0
	}

	return c
}
