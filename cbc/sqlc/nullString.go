package sqlc

import (
	"database/sql"
	"database/sql/driver"
	"encoding/json"
)

type NullString sql.NullString

// GobEncode implements the gob.GobEncoder interface.
func (l NullString) GobEncode() ([]byte, error) {
	if l.Valid {
		return json.Marshal(l.String)
	}

	return []byte("null"), nil
}

// GobDecode implements the gob.GobDecoder interface.
func (l *NullString) GobDecode(data []byte) error {
	var (
		val   string
		vaild bool
	)
	str := string(data)

	if str != "null" {
		val = str
		vaild = true
	}

	*l = NullString{
		String: val,
		Valid:  vaild,
	}

	return nil
}

func (l NullString) MarshalJSON() ([]byte, error) {
	if l.Valid {
		return json.Marshal(l.String)
	}

	return []byte("null"), nil
}

func (l *NullString) UnmarshalJSON(bs []byte) error {
	var str string
	strErr := json.Unmarshal(bs, &str)

	if strErr != nil {
		return strErr
	}

	if str == "null" || len(str) == 0 {
		*l = NullString{
			String: str,
			Valid:  false,
		}

		return nil
	}

	*l = NullString{
		String: str,
		Valid:  true,
	}

	return nil
}

// Scan implements the Scanner interface.
func (ns *NullString) Scan(value interface{}) error {
	if value == nil {
		ns.Valid = false
	} else {
		nsValue, ok := value.(string)
		ns.Valid = ok

		if ok {
			ns.String = nsValue
		} else {
			switch s := value.(type) {
			case []byte:
				ns.String = string(s)
				ns.Valid = true
			}
		}
	}

	return nil
}

// Value implements the driver Valuer interface.
func (ns NullString) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return ns.String, nil
}

// ParseString 如果為空字串則轉換為 NullString
func ParseString(s string) NullString {
	if len(s) == 0 {
		return NullString{}
	}
	return NullString{
		String: s,
		Valid:  true,
	}
}
