package sqlc

import (
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"strconv"
)

type NullInt32 sql.NullInt32

// String 透過 strconv.Itoa 轉換為 string
//
//	@receiver l
//	@return string 如果 Valid false 會是空字串
func (l NullInt32) String() string {
	if l.Valid {
		return strconv.Itoa(int(l.Int32))
	}

	return ""
}

// GobEncode implements the gob.GobEncoder interface.
func (l NullInt32) GobEncode() ([]byte, error) {
	if l.Valid {
		return []byte(strconv.Itoa(int(l.Int32))), nil
	}

	return []byte("null"), nil
}

// GobDecode implements the gob.GobDecoder interface.
func (l *NullInt32) GobDecode(data []byte) error {
	var (
		val   int64
		vaild bool
		err   error
	)
	str := string(data)

	if str != "null" {
		val, err = strconv.ParseInt(str, 10, 32)
		vaild = err == nil
	}

	if err == nil {
		*l = NullInt32{
			Int32: int32(val),
			Valid: vaild,
		}
	}

	return err
}

func (l NullInt32) MarshalJSON() ([]byte, error) {
	if l.Valid {
		return []byte(strconv.FormatInt(int64(l.Int32), 10)), nil
	}

	return []byte("null"), nil
}

func (l *NullInt32) UnmarshalJSON(bs []byte) error {
	var str string
	strErr := json.Unmarshal(bs, &str)

	if strErr != nil {
		return strErr
	}

	if len(str) == 0 || str == "null" {
		*l = NullInt32{
			Valid: false,
		}

		return nil
	}

	t, err := strconv.ParseInt(str, 10, 32)

	if err != nil {
		return err
	}

	*l = NullInt32{
		Int32: int32(t),
		Valid: false,
	}

	return nil
}

// Scan implements the Scanner interface.
func (ns *NullInt32) Scan(value interface{}) error {
	if value == nil {
		ns.Valid = false
	} else {
		nsValue, ok := value.(int32)
		ns.Valid = ok

		if ok {
			ns.Int32 = nsValue
		}
	}

	return nil
}

// Value implements the driver Valuer interface.
func (ns NullInt32) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return ns.Int32, nil
}
