package sqlc

import (
	"encoding/json"
	"fmt"
	"time"
)

type LocalDate time.Time

// ToTime 轉換為 time.Time
func (t LocalDate) ToTime() time.Time {
	return time.Time(t)
}

// Format 透過 time.Time 的 Format 轉換為 string
//
//	@receiver t
//	@param layout 格式
//	@return string
func (t LocalDate) Format(layout string) string {
	return t.ToTime().Format(layout)
}

// ToString 依 2006-01-02 做格式化
//
// @return string
func (t LocalDate) ToString() string {
	return t.ToTime().Format("2006-01-02")
}

// GobEncode implements the gob.GobEncoder interface.
func (t LocalDate) GobEncode() ([]byte, error) {
	return time.Time(t).MarshalBinary()
}

// GobDecode implements the gob.GobDecoder interface.
func (t *LocalDate) GobDecode(data []byte) error {
	tt := time.Time{}
	err := tt.UnmarshalBinary(data)

	if err == nil {
		*t = LocalDate(tt)
	}

	return err
}

func (l LocalDate) MarshalJSON() ([]byte, error) {
	stamp := fmt.Sprintf("\"%s\"", time.Time(l).Format("2006-01-02"))
	return []byte(stamp), nil
}

func (l *LocalDate) UnmarshalJSON(bs []byte) error {
	var str string
	strErr := json.Unmarshal(bs, &str)

	if strErr != nil {
		return strErr
	}

	if len(str) == 0 || str == "null" {
		return nil
	}

	t, err := time.Parse("2006-01-02", str)

	if err != nil {
		return err
	}

	*l = LocalDate(t)
	return nil
}
