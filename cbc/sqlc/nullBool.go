package sqlc

import (
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"strconv"
)

type NullBool sql.NullBool

// String 透過 strconv.Itoa 轉換為 string
//
//	@receiver l
//	@return string 如果 Valid false 會是空字串
func (l NullBool) String() string {
	if l.Valid {
		return strconv.FormatBool(l.Bool)
	}

	return ""
}

// GobEncode implements the gob.GobEncoder interface.
func (l NullBool) GobEncode() ([]byte, error) {
	if l.Valid {
		return []byte(strconv.FormatBool(l.Bool)), nil
	}

	return []byte("null"), nil
}

// GobDecode implements the gob.GobDecoder interface.
func (l *NullBool) GobDecode(data []byte) error {
	var (
		val   bool
		vaild bool
		err   error
	)
	str := string(data)

	if str != "null" {
		val, err = strconv.ParseBool(str)
		vaild = err == nil
	}

	if err == nil {
		*l = NullBool{
			Bool:  val,
			Valid: vaild,
		}
	}

	return err
}

func (l NullBool) MarshalJSON() ([]byte, error) {
	if l.Valid {
		return []byte(strconv.FormatBool(l.Bool)), nil
	}

	return []byte("null"), nil
}

func (l *NullBool) UnmarshalJSON(bs []byte) error {
	var str string
	strErr := json.Unmarshal(bs, &str)

	if strErr != nil {
		return strErr
	}

	if len(str) == 0 || str == "null" {
		*l = NullBool{
			Valid: false,
		}

		return nil
	}

	t, err := strconv.ParseBool(str)

	if err != nil {
		return err
	}

	*l = NullBool{
		Bool:  t,
		Valid: false,
	}

	return nil
}

// Scan implements the Scanner interface.
func (ns *NullBool) Scan(value interface{}) error {
	if value == nil {
		ns.Valid = false
	} else {
		nsValue, ok := value.(bool)
		ns.Valid = ok

		if ok {
			ns.Bool = nsValue
		}
	}

	return nil
}

// Value implements the driver Valuer interface.
func (ns NullBool) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return ns.Bool, nil
}
