package sqlc

import (
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"strconv"
)

type NullInt64 sql.NullInt64

// String 透過 strconv.FormatInt 轉換為 string
//
//	@receiver l
//	@return string 如果 Valid false 會是空字串
func (l NullInt64) String() string {
	if l.Valid {
		return strconv.FormatInt(l.Int64, 10)
	}

	return ""
}

// GobEncode implements the gob.GobEncoder interface.
func (l NullInt64) GobEncode() ([]byte, error) {
	if l.Valid {
		return []byte(strconv.FormatInt(l.Int64, 10)), nil
	}

	return []byte("null"), nil
}

// GobDecode implements the gob.GobDecoder interface.
func (l *NullInt64) GobDecode(data []byte) error {
	var (
		val   int64
		vaild bool
		err   error
	)
	str := string(data)

	if str != "null" {
		val, err = strconv.ParseInt(str, 10, 64)
		vaild = err == nil
	}

	if err == nil {
		*l = NullInt64{
			Int64: val,
			Valid: vaild,
		}
	}

	return err
}

func (l NullInt64) MarshalJSON() ([]byte, error) {
	if l.Valid {
		return []byte(strconv.FormatInt(l.Int64, 10)), nil
	}

	return []byte("null"), nil
}

func (l *NullInt64) UnmarshalJSON(bs []byte) error {
	var str string
	strErr := json.Unmarshal(bs, &str)

	if strErr != nil {
		return strErr
	}

	if len(str) == 0 || str == "null" {
		*l = NullInt64{
			Valid: false,
		}

		return nil
	}

	t, err := strconv.ParseInt(str, 10, 64)

	if err != nil {
		return err
	}

	*l = NullInt64{
		Int64: t,
		Valid: false,
	}

	return nil
}

// Scan implements the Scanner interface.
func (ns *NullInt64) Scan(value interface{}) error {
	if value == nil {
		ns.Valid = false
	} else {
		nsValue, ok := value.(int64)
		ns.Valid = ok

		if ok {
			ns.Int64 = nsValue
		}
	}

	return nil
}

// Value implements the driver Valuer interface.
func (ns NullInt64) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return ns.Int64, nil
}
