package sqlc

import (
	"database/sql/driver"
	"fmt"
	"time"
)

type NullLocalDateTime struct {
	Time LocalDateTime
	// Valid is true if Time is not NULL
	Valid bool
}

// Format 透過 time.Time 的 Format 轉換為 string
//
//	@receiver t
//	@param layout 格式
//	@return string 如果 Valid false 會是空字串
func (t NullLocalDateTime) Format(layout string) string {
	if t.Valid {
		return t.Time.Format(layout)
	}

	return ""
}

// ToString 依 2006-01-02 15:04:05 做格式化

// @return string
func (t NullLocalDateTime) ToString() string {
	if t.Valid {
		return t.Time.ToTime().Format("2006-01-02 15:04:05")
	}

	return ""
}

func (l NullLocalDateTime) MarshalJSON() ([]byte, error) {
	if l.Valid {
		stamp := fmt.Sprintf("\"%s\"", time.Time(l.Time).Format("2006-01-02 15:04:05"))
		return []byte(stamp), nil
	}

	return []byte("null"), nil
}

// Scan implements the Scanner interface.
func (ns *NullLocalDateTime) Scan(value interface{}) error {
	if value == nil {
		ns.Valid = false
	} else {
		valTime, ok := value.(time.Time)

		if ok {
			ns.Valid = true
			ns.Time = LocalDateTime(valTime)
		} else {
			ns.Valid = false
		}
	}

	return nil
}

// Value implements the driver Valuer interface.
func (ns NullLocalDateTime) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return ns.Time, nil
}
