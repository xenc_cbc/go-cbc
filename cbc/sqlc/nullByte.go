package sqlc

import (
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"strconv"
)

type NullByte sql.NullByte

// String 透過 strconv.Itoa 轉換為 string
//
//	@receiver l
//	@return string 如果 Valid false 會是空字串
func (l NullByte) String() string {
	if l.Valid {
		return strconv.Itoa(int(l.Byte))
	}

	return ""
}

// GobEncode implements the gob.GobEncoder interface.
func (l NullByte) GobEncode() ([]byte, error) {
	if l.Valid {
		return []byte(strconv.Itoa(int(l.Byte))), nil
	}

	return []byte("null"), nil
}

// GobDecode implements the gob.GobDecoder interface.
func (l *NullByte) GobDecode(data []byte) error {
	var (
		val   uint64
		vaild bool
		err   error
	)
	str := string(data)

	if str != "null" {
		val, err = strconv.ParseUint(str, 10, 8)
		vaild = err == nil
	}

	if err == nil {
		*l = NullByte{
			Byte:  byte(val),
			Valid: vaild,
		}
	}

	return err
}

func (l NullByte) MarshalJSON() ([]byte, error) {
	if l.Valid {
		return []byte(strconv.FormatInt(int64(l.Byte), 10)), nil
	}

	return []byte("null"), nil
}

func (l *NullByte) UnmarshalJSON(bs []byte) error {
	var str string
	strErr := json.Unmarshal(bs, &str)

	if strErr != nil {
		return strErr
	}

	if len(str) == 0 || str == "null" {
		*l = NullByte{
			Valid: false,
		}

		return nil
	}

	t, err := strconv.ParseInt(str, 10, 8)

	if err != nil {
		return err
	}

	*l = NullByte{
		Byte:  byte(t),
		Valid: false,
	}

	return nil
}

// Scan implements the Scanner interface.
func (ns *NullByte) Scan(value interface{}) error {
	if value == nil {
		ns.Valid = false
	} else {
		nsValue, ok := value.(byte)
		ns.Valid = ok

		if ok {
			ns.Byte = nsValue
		}
	}

	return nil
}

// Value implements the driver Valuer interface.
func (ns NullByte) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return ns.Byte, nil
}
