package sqlc

import (
	"encoding/json"
	"fmt"
	"time"
)

type LocalTime time.Time

// ToTime 轉換為 time.Time
func (t LocalTime) ToTime() time.Time {
	return time.Time(t)
}

// Format 透過 time.Time 的 Format 轉換為 string
//
//	@receiver t
//	@param layout 格式
//	@return string
func (t LocalTime) Format(layout string) string {
	return t.ToTime().Format(layout)
}

// ToString 依 15:04:05 做格式化
//
// @return string
func (t LocalTime) ToString() string {
	return t.ToTime().Format("15:04:05")
}

// GobEncode implements the gob.GobEncoder interface.
func (t LocalTime) GobEncode() ([]byte, error) {
	return time.Time(t).MarshalBinary()
}

// GobDecode implements the gob.GobDecoder interface.
func (t *LocalTime) GobDecode(data []byte) error {
	tt := time.Time{}
	err := tt.UnmarshalBinary(data)

	if err == nil {
		*t = LocalTime(tt)
	}

	return err
}

func (l LocalTime) MarshalJSON() ([]byte, error) {
	stamp := fmt.Sprintf("\"%s\"", time.Time(l).Format("15:04:05"))
	return []byte(stamp), nil
}

func (l *LocalTime) UnmarshalJSON(bs []byte) error {
	var str string
	strErr := json.Unmarshal(bs, &str)

	if strErr != nil {
		return strErr
	}

	if len(str) == 0 {
		return nil
	}

	t, err := time.Parse("15:04:05", str)

	if err != nil {
		return err
	}

	*l = LocalTime(t)
	return nil
}
