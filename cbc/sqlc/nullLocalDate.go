package sqlc

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"time"
)

type NullLocalDate struct {
	Time LocalDate
	// Valid is true if Time is not NULL
	Valid bool
}

// Format 透過 time.Time 的 Format 轉換為 string
//
//	@receiver t
//	@param layout 格式
//	@return string 如果 Valid false 會是空字串
func (t NullLocalDate) Format(layout string) string {
	if t.Valid {
		return t.Time.Format(layout)
	}

	return ""
}

// ToString 依 2006-01-02 15:04:05 做格式化
//
// @return string
func (t NullLocalDate) ToString() string {
	if t.Valid {
		return t.Time.ToTime().Format("2006-01-02 15:04:05")
	}

	return ""
}

func (l NullLocalDate) MarshalJSON() ([]byte, error) {
	if l.Valid {
		stamp := fmt.Sprintf("\"%s\"", time.Time(l.Time).Format("2006-01-02"))
		return []byte(stamp), nil
	}

	return []byte("null"), nil
}

func (l *NullLocalDate) UnmarshalJSON(bs []byte) error {
	var str string
	strErr := json.Unmarshal(bs, &str)

	if strErr != nil {
		return strErr
	}

	if len(str) == 0 || str == "null" {
		*l = NullLocalDate{
			Valid: false,
		}

		return nil
	}

	t, err := time.Parse("2006-01-02", str)

	if err != nil {
		return err
	}

	*l = NullLocalDate{
		Time:  LocalDate(t),
		Valid: false,
	}

	return nil
}

// Scan implements the Scanner interface.
func (ns *NullLocalDate) Scan(value interface{}) error {
	if value == nil {
		ns.Valid = false
	} else {
		valTime, ok := value.(time.Time)

		if ok {
			ns.Valid = true
			ns.Time = LocalDate(valTime)
		} else {
			ns.Valid = false
		}
	}

	return nil
}

// Value implements the driver Valuer interface.
func (ns NullLocalDate) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return ns.Time, nil
}
