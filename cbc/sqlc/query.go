package sqlc

import "github.com/jmoiron/sqlx"

// Query 用來組合 SQL 及 Parms
//	select "Column" from "From" where "Where" "Other"
type Query[T any] struct {
	DB          DB
	column      string
	from        string
	where       string
	other       string
	params      []any
	otherParams []any
	conditions  []Condition
}

// Condition 配合 Query 使用
type Condition struct {
	SQL      string
	Params   []any
	Operator string
}

// NewQuery
//	select "Column" from "From" where "Where" "Other"
//
//  @param dbCon
//  @return *Query
func NewQuery[T any](dbCon DB) *Query[T] {
	q := &Query[T]{
		DB: dbCon,
	}

	return q
}

// CloneQuery 複製 Query 中的成員，產生新的 Query 回傳
//
//  @param q 範本
//  @return *Query
func CloneQuery[T any, K any](q *Query[K]) *Query[T] {
	return &Query[T]{
		DB:          q.DB,
		column:      q.column,
		from:        q.from,
		where:       q.where,
		other:       q.other,
		params:      q.params,
		otherParams: q.otherParams,
		conditions:  q.conditions,
	}
}

// Column 設定 Column sql 的內容
//	不含 "select" 的 sql
//
//  @receiver q
//  @param sql
//  @param params
//  @return *Query
func (q *Query[T]) Column(sql string) *Query[T] {
	q.column = sql
	return q
}

// From 設定 From sql 的內容
//	不含 "from" 的 sql
//
//  @receiver q
//  @param sql
//  @param params
//  @return *Query
func (q *Query[T]) From(sql string) *Query[T] {
	q.from = sql
	return q
}

// Where 設定 Where sql 的內容
//	不含 "where" 的 sql
//
//  @receiver q
//  @param sql
//  @param params
//  @return *Query
func (q *Query[T]) Where(sql string, params ...any) *Query[T] {
	q.where = sql
	q.params = params
	return q
}

// And 加入 Condition (AND)
//  @receiver q
//  @param sql
//  @param params
//  @return *Query
func (q *Query[T]) And(sql string, params ...any) *Query[T] {
	q.conditions = append(q.conditions, Condition{SQL: sql, Params: params, Operator: "and"})
	return q
}

// Or 加入 Condition (OR)
//  @receiver q
//  @param sql
//  @param params
//  @return *Query
func (q *Query[T]) Or(sql string, params ...any) *Query[T] {
	q.conditions = append(q.conditions, Condition{SQL: sql, Params: params, Operator: "or"})
	return q
}

// Other 設定 other 位置的 sql
//	包含 group by 及 having、order by 及 offset、limit 之類的
//
//  @receiver q
//  @param sql
//  @param params
//  @return *Query
func (q *Query[T]) Other(sql string, params ...any) *Query[T] {
	q.other = sql
	q.otherParams = params
	return q
}

// Append 附加到 other sql 的後方
//	包含 group by 及 having、order by 及 offset、limit 之類的
//
//  @receiver q
//  @param sql
//  @param params
//  @return *Query
func (q *Query[T]) Append(sql string, params ...any) *Query[T] {
	q.other = q.other + " " + sql

	if len(params) > 0 {
		q.otherParams = append(q.otherParams, params...)
	}
	return q
}

// Clone 複製一份同樣的設定
//
//  @receiver q
//  @return *Query
func (q *Query[T]) Clone() *Query[T] {
	return &Query[T]{
		DB:          q.DB,
		column:      q.column,
		from:        q.from,
		where:       q.where,
		other:       q.other,
		params:      q.params,
		otherParams: q.otherParams,
		conditions:  q.conditions,
	}
}

// Select 執行 SQL 取得多筆查詢結果
//
//  @receiver q
//  @param inOperator sql 中有沒有含有 in
//  @return []T 查詢的結果
func (q *Query[T]) Select(inOperator bool) []T {
	rsArr := make([]T, 0)
	sql, params := q.Combine()

	if inOperator {
		var err error
		sql, params, err = sqlx.In(sql, params...)

		if err != nil {
			panic(err)
		}
	}

	sql = q.DB.Rebind(sql)
	MustSelectx(q.DB, &rsArr, sql, params...)
	return rsArr
}

// Get 執行 SQL 取得一筆查詢結果
//
//  @receiver q
//  @param inOperator sql 中有沒有含有 in
//  @return T 查詢的結果
//  @return bool 無資料的話，會是 false
func (q *Query[T]) Get(inOperator bool) (T, bool) {
	var rs T
	sql, params := q.Combine()

	if inOperator {
		var err error
		sql, params, err = sqlx.In(sql, params...)

		if err != nil {
			panic(err)
		}
	}

	sql = q.DB.Rebind(sql)
	rsOk := MustGetx(q.DB, &rs, sql, params...)
	return rs, rsOk
}

// Combine 產生 SQL (不含 select) 及要傳入的參數
//
//	@receiver q
//	@return string SQL
//	@return []any 參數
func (q *Query[T]) Combine() (string, []any) {
	sql := "select " + q.column + " from " + q.from
	params := q.params
	hasWhere := len(q.where) > 0
	conditionLen := len(q.conditions)

	if hasWhere {
		sql = sql + " where " + q.where
	}

	if conditionLen > 0 {
		if hasWhere {
			sql = sql + " " + q.conditions[0].Operator + " " + q.conditions[0].SQL
		} else {
			sql = sql + " where " + q.conditions[0].SQL
		}

		if q.conditions[0].Params != nil {
			params = append(params, q.conditions[0].Params...)
		}

		for i := 1; i < conditionLen; i++ {
			sql = sql + " " + q.conditions[i].Operator + " " + q.conditions[i].SQL

			if q.conditions[i].Params != nil {
				params = append(params, q.conditions[i].Params...)
			}
		}
	}

	if len(q.other) > 0 {
		sql = sql + " " + q.other

		if len(q.otherParams) > 0 {
			params = append(params, q.otherParams...)
		}
	}

	return sql, params
}
