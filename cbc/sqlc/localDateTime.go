package sqlc

import (
	"encoding/json"
	"fmt"
	"time"
)

type LocalDateTime time.Time

// ToTime 轉換為 time.Time
func (t LocalDateTime) ToTime() time.Time {
	return time.Time(t)
}

// Format 透過 time.Time 的 Format 轉換為 string
//
//	@receiver t
//	@param layout 格式
//	@return string
func (t LocalDateTime) Format(layout string) string {
	return t.ToTime().Format(layout)
}

// ToString 依 2006-01-02 15:04:05 做格式化
//
// @return string
func (t LocalDateTime) ToString() string {
	return t.ToTime().Format("2006-01-02 15:04:05")
}

// GobEncode implements the gob.GobEncoder interface.
func (t LocalDateTime) GobEncode() ([]byte, error) {
	return time.Time(t).MarshalBinary()
}

// GobDecode implements the gob.GobDecoder interface.
func (t *LocalDateTime) GobDecode(data []byte) error {
	tt := time.Time{}
	err := tt.UnmarshalBinary(data)

	if err == nil {
		*t = LocalDateTime(tt)
	}

	return err
}

func (l LocalDateTime) MarshalJSON() ([]byte, error) {
	stamp := fmt.Sprintf("\"%s\"", time.Time(l).Format("2006-01-02 15:04:05"))
	return []byte(stamp), nil
}

func (l *LocalDateTime) UnmarshalJSON(bs []byte) error {
	var str string
	strErr := json.Unmarshal(bs, &str)

	if strErr != nil {
		return strErr
	}

	if len(str) == 0 {
		return nil
	}

	t, err := time.Parse("2006-01-02 15:04:05", str)

	if err != nil {
		return err
	}

	*l = LocalDateTime(t)
	return nil
}
