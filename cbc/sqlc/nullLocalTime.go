package sqlc

import (
	"database/sql/driver"
	"fmt"
	"time"
)

type NullLocalTime struct {
	Time LocalTime
	// Valid is true if Time is not NULL
	Valid bool
}

// Format 透過 time.Time 的 Format 轉換為 string
//
//	@receiver t
//	@param layout 格式
//	@return string 如果 Valid false 會是空字串
func (t NullLocalTime) Format(layout string) string {
	if t.Valid {
		return t.Time.Format(layout)
	}

	return ""
}

// ToString 依 15:04:05 做格式化

// @return string
func (t NullLocalTime) ToString() string {
	if t.Valid {
		return t.Time.ToTime().Format("15:04:05")
	}

	return ""
}

func (l NullLocalTime) MarshalJSON() ([]byte, error) {
	stamp := fmt.Sprintf("\"%s\"", time.Time(l.Time).Format("15:04:05"))
	return []byte(stamp), nil
}

// Scan implements the Scanner interface.
func (ns *NullLocalTime) Scan(value interface{}) error {
	if value == nil {
		ns.Valid = false
	} else {
		valTime, ok := value.(time.Time)

		if ok {
			ns.Valid = true
			ns.Time = LocalTime(valTime)
		} else {
			ns.Valid = false
		}
	}

	return nil
}

// Value implements the driver Valuer interface.
func (ns NullLocalTime) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return ns.Time, nil
}
