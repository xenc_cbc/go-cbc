package sqlc

import (
	"database/sql"
	"database/sql/driver"
	"encoding/json"
	"strconv"
)

type NullInt16 sql.NullInt16

// String 透過 strconv.Itoa 轉換為 string
//
//	@receiver l
//	@return string 如果 Valid false 會是空字串
func (l NullInt16) String() string {
	if l.Valid {
		return strconv.Itoa(int(l.Int16))
	}

	return ""
}

// GobEncode implements the gob.GobEncoder interface.
func (l NullInt16) GobEncode() ([]byte, error) {
	if l.Valid {
		return []byte(strconv.Itoa(int(l.Int16))), nil
	}

	return []byte("null"), nil
}

// GobDecode implements the gob.GobDecoder interface.
func (l *NullInt16) GobDecode(data []byte) error {
	var (
		val   int64
		vaild bool
		err   error
	)
	str := string(data)

	if str != "null" {
		val, err = strconv.ParseInt(str, 10, 16)
		vaild = err == nil
	}

	if err == nil {
		*l = NullInt16{
			Int16: int16(val),
			Valid: vaild,
		}
	}

	return err
}

func (l NullInt16) MarshalJSON() ([]byte, error) {
	if l.Valid {
		return []byte(strconv.FormatInt(int64(l.Int16), 10)), nil
	}

	return []byte("null"), nil
}

func (l *NullInt16) UnmarshalJSON(bs []byte) error {
	var str string
	strErr := json.Unmarshal(bs, &str)

	if strErr != nil {
		return strErr
	}

	if len(str) == 0 || str == "null" {
		*l = NullInt16{
			Valid: false,
		}

		return nil
	}

	t, err := strconv.ParseInt(str, 10, 16)

	if err != nil {
		return err
	}

	*l = NullInt16{
		Int16: int16(t),
		Valid: false,
	}

	return nil
}

// Scan implements the Scanner interface.
func (ns *NullInt16) Scan(value interface{}) error {
	if value == nil {
		ns.Valid = false
	} else {
		nsValue, ok := value.(int16)
		ns.Valid = ok

		if ok {
			ns.Int16 = nsValue
		}
	}

	return nil
}

// Value implements the driver Valuer interface.
func (ns NullInt16) Value() (driver.Value, error) {
	if !ns.Valid {
		return nil, nil
	}
	return ns.Int16, nil
}
