package cbc

import (
	"encoding/gob"
	"net/http"
	"regexp"
	"strings"

	"bitbucket.org/xenc_cbc/go-cbc/v2/cbc/httpc"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

var _ PluginWithInit = &ACL{}

// ACL Path 權限控管
// 有符合的 rule 之後就算不符合 role 也不會再向後檢查
type ACL struct {
	PluginImp
	Config             ACLConfig
	Settings           ACLSetting
	ruleMap            map[string]map[string]ACLSetting
	aclMap             map[string][]aclRegexp
	log                *zap.Logger
	Default403Response func(c *gin.Context, setting *ACLSetting) bool
}

type aclRegexp struct {
	regex  *regexp.Regexp
	mapKey string
}

func (acl *ACL) GetName() string {
	return AclPluginName
}

func (acl *ACL) GetState() PluginState {
	return acl.state
}

func (acl *ACL) Close() {
	acl.state = Disable
}

func init() {
	gob.Register(map[string]string{})
}

func makeDefault403Response(c *gin.Context, setting *ACLSetting) bool {
	h := c.Request.Header

	if _, ok := h["Er-Json"]; ok {
		json := httpc.JSONResult{Success: false, Msg: "403"}
		c.AbortWithStatusJSON(http.StatusOK, json)
		return true
	}

	return false
}

// Init 初始化
func (md *ACL) Init(context *Context, cloader ConfigLoader, logger *Logger) {
	md.log = logger.Log.With(zap.String("source", "acl"))
	jsonExist, jsonErr := cloader.DecodeConfig("acl.json", &md.Config)
	jsonFailFlag := jsonErr != nil

	if jsonFailFlag {
		md.state = Failed
		md.log.Info("json error")
		return
	} else if !jsonExist {
		md.state = Disable
		md.log.Info("json not found , Disable.")
		return
	}

	if len(md.Config.LoginPath) == 0 {
		md.Config.LoginPath = "/login"
	}

	ruleMap := make(map[string]map[string]ACLSetting)
	aclArrMap := make(map[string][]aclRegexp)

	for _, v := range aclMethodTypeArr {
		ruleMap[v] = make(map[string]ACLSetting)
		aclArrMap[v] = make([]aclRegexp, 0)
	}

	var regexpErr bool

	for i := 0; i < len(md.Config.Settings); i++ {
		setting := &md.Config.Settings[i]

		if len(setting.JSONKey) > 0 {
			setting.IsJSON = true
		} else if setting.IsJSON {
			setting.JSONKey = md.Config.Default403JsonKey
		}

		methods := setting.Methods

		if len(methods) == 0 {
			methods = append(methods, ALL)
			setting.Methods = methods
		}

		for _, method := range methods {
			methodStr := method.String()
			ruleMap[methodStr][setting.Path] = *setting
			rx, err := regexp.Compile(setting.Path)

			if err == nil {
				md.log.Info("Add Rule", zap.String("Method", methodStr), zap.String("Path", setting.Path))
			} else {
				md.log.Error("Rule Path Invalid.",
					zap.String("Method", methodStr),
					zap.String("Path", setting.Path), zap.Error(err))
				regexpErr = true
				continue
			}

			aclArr := aclArrMap[methodStr]
			ap := aclRegexp{rx, setting.Path}
			aclArr = append(aclArr, ap)
			aclArrMap[methodStr] = aclArr
		}
	}

	md.ruleMap = ruleMap
	md.aclMap = aclArrMap

	for _, method := range aclMethodTypeArr {
		aclArr := aclArrMap[method]

		if len(aclArr) != 0 {
			md.log.Debug("Rule Method", zap.String("Method", method))

			for _, aclRegx := range aclArr {
				aclSetting := ruleMap[method][aclRegx.mapKey]

				if aclSetting.Type == Has || aclSetting.Type == AnyOne {
					md.log.Debug("Rule", zap.String("Path", aclRegx.mapKey),
						zap.String("Type", aclSetting.Type.String()),
						zap.String("Role", strings.Join(aclSetting.Roles, ",")))
				} else {
					md.log.Debug("Rule", zap.String("Path", aclRegx.mapKey),
						zap.String("Type", aclSetting.Type.String()))
				}
			}
		}
	}

	if md.Default403Response == nil {
		md.Default403Response = makeDefault403Response
	}

	if regexpErr {
		md.state = Failed
	} else {
		md.state = Enable
	}
}

// Active 加入 Middleware
func (md *ACL) Active(g *gin.Engine) {
	md.log.Info("Active ACL",
		zap.String("Hostname", md.Config.Hostname),
		zap.Bool("Need SSL", md.Config.LogginRequireSsl),
		zap.String("SSL Port", md.Config.SslPort),
		zap.String("Login Path", md.Config.LoginPath),
		zap.String("Deny Path", md.Config.DenyPath),
		zap.String("Default Action", md.Config.DefaultAction.String()),
	)
	g.Use(md.middleware)
}

func (md *ACL) middleware(c *gin.Context) {
	path := c.Request.URL.Path
	method := c.Request.Method
	md.log.Debug("Request", zap.String("Method", method), zap.String("Path", c.Request.URL.Path))

	if md.checkRegx(method, path, c) || md.checkRegx("ALL", path, c) {
		return
	}

	switch md.Config.DefaultAction {
	case Permit:
		c.Next()
	default:
		md.redirectTo403(c, nil)
	}
}

func (md *ACL) checkRegx(method string, path string, c *gin.Context) bool {
	aclArr := md.aclMap[method]

	if len(aclArr) != 0 {
		for _, ac := range aclArr {
			rs := ac.regex.MatchString(path)

			if rs {
				key := ac.mapKey
				md.log.Debug("Check", zap.String("method", method),
					zap.String("path", path), zap.String("key", key))
				md.checkACL(method, key, c)
				return true
			}
		}
	}

	return false
}

// checkACL 檢查權限
func (md *ACL) checkACL(method string, key string, c *gin.Context) {
	roleArr := md.getRoleData(c)
	aclMap := md.ruleMap[method]
	rule := aclMap[key]

	switch rule.Type {
	case Permit:
		md.log.Debug("Permit", zap.String("method", method), zap.String("key", key))
		c.Next()
	case Deny:
		md.log.Debug("Deny", zap.String("method", method), zap.String("key", key))
		md.redirectTo403(c, &rule)
	case Has:
		if roleArr != nil && checkHasRole(roleArr, rule.Roles) {
			md.log.Debug("Has(Permit)", zap.String("method", method), zap.String("key", key))
			c.Next()
		} else {
			md.log.Debug("Has(Deny)", zap.String("method", method), zap.String("key", key))
			md.redirectToNeedRole(c, roleArr, &rule)
		}
	case AnyOne:
		if roleArr != nil && checkAnyOneRole(roleArr, rule.Roles) {
			md.log.Debug("AnyOne(Permit)", zap.String("method", method), zap.String("key", key))
			c.Next()
		} else {
			md.log.Debug("Has(Deny)", zap.String("method", method), zap.String("key", key))
			md.redirectToNeedRole(c, roleArr, &rule)
		}
	case LoggedIn:
		if md.IsAnonymous(c) {
			md.redirectToLogin(c, &rule)
		} else {
			c.Next()
		}
	case Anonymous:
		if md.IsAnonymous(c) {
			c.Next()
		} else {
			md.redirectTo403(c, &rule)
		}
	default:
		md.log.Error("Unknown ACL Type : " + rule.Type.String())
		md.redirectTo403(c, &rule)
	}
}

// HasAnyOneRole 檢查是否有指定的其中一個 Role
func (md *ACL) HasAnyOneRole(c *gin.Context, targetRole []string) bool {
	roleArr := md.getRoleData(c)
	return checkAnyOneRole(roleArr, targetRole)
}

// HasRole 檢查是否有指定的全部 Role
func (md *ACL) HasRole(c *gin.Context, targetRole []string) bool {
	roleArr := md.getRoleData(c)
	return checkHasRole(roleArr, targetRole)
}

// IsLoggedIn 檢查是否有登入
func (md *ACL) IsLoggedIn(c *gin.Context) bool {
	session := sessions.Default(c)
	u, ok := md.GetUserDetails(&session)

	if !ok {
		return false
	}

	return u.GetID() > 0
}

// IsAnonymous 檢查是否沒登入
func (md *ACL) IsAnonymous(c *gin.Context) bool {
	return !md.IsLoggedIn(c)
}

func checkAnyOneRole(nowRole []string, targetRole []string) bool {
	for _, nr := range nowRole {
		for _, tr := range targetRole {
			if nr == tr {
				return true
			}
		}
	}

	return false
}

func checkHasRole(nowRole []string, targetRole []string) bool {
	count := 0

	for _, tr := range targetRole {
		for _, nr := range nowRole {
			if nr == tr {
				count++
				break
			}
		}
	}

	return len(targetRole) == count
}

func (md *ACL) getRoleData(c *gin.Context) []string {
	session := sessions.Default(c)
	roleData, ok := md.GetRules(&session)

	if ok {
		return roleData
	}

	return nil
}

func getNowSchema(c *gin.Context) string {
	if c.Request.TLS == nil {
		return "http"
	}

	return "https"
}

func isGetMethod(c *gin.Context) bool {
	return c.Request.Method == http.MethodGet
}

func (md *ACL) redirectToNeedRole(c *gin.Context, roleArr []string, setting *ACLSetting) {
	if len(roleArr) == 0 {
		md.redirectToLogin(c, setting)
	} else {
		md.redirectTo403(c, setting)
	}
}

func (md *ACL) redirectToLogin(c *gin.Context, setting *ACLSetting) {
	if setting != nil && setting.IsJSON {
		json, exist := md.Config.JSON403Map[setting.JSONKey]

		if exist {
			c.AbortWithStatusJSON(http.StatusOK, json)
			return
		}
	}

	if !md.Default403Response(c, setting) {
		var url string

		if isGetMethod(c) {
			path := c.Request.URL.Path
			rq := c.Request.URL.RawQuery

			if len(rq) > 0 {
				path = path + "?" + rq
			}

			url = md.Config.LoginPath + "?url=" + path

			if md.Config.LogginRequireSsl && getNowSchema(c) == "http" {
				prefix := "https://" + md.Config.Hostname

				if md.Config.SslPort != "" && md.Config.SslPort != "443" {
					prefix = prefix + ":" + md.Config.SslPort
				}

				url = prefix + md.Config.LoginPath
			}

			c.Redirect(http.StatusTemporaryRedirect, url)
			c.Abort()
		} else {
			c.AbortWithStatusJSON(http.StatusForbidden, httpc.JSONResult{Success: false, Msg: "403"})
		}
	}
}

func (md *ACL) redirectTo403(c *gin.Context, setting *ACLSetting) {
	if setting != nil && setting.IsJSON {
		json, exist := md.Config.JSON403Map[setting.JSONKey]

		if exist {
			c.AbortWithStatusJSON(http.StatusOK, json)
			return
		}
	}

	if !md.Default403Response(c, setting) {
		if len(md.Config.DenyPath) > 0 {
			c.Redirect(http.StatusTemporaryRedirect, md.Config.DenyPath)
			c.Abort()
		} else {
			c.AbortWithStatus(http.StatusForbidden)
		}
	}
}

// GetRules 取得目前有的 Rules
func (md *ACL) GetRules(s *sessions.Session) ([]string, bool) {
	return httpc.GetStringArray(s, md.Config.SessionACLKey)
}

// SetRules 設定目前有的 Rules
func (md *ACL) SetRules(s *sessions.Session, rules []string) {
	(*s).Set(md.Config.SessionACLKey, rules)
}

// GetUserDetails 取得目前的 UserDetails
func (md *ACL) GetUserDetails(s *sessions.Session) (httpc.UserDetails, bool) {
	c, ok := (*s).Get(md.Config.SessionUserKey).(httpc.UserDetails)
	return c, ok
}

// SetUserDetails 設定目前的 UserDetails
func (md *ACL) SetUserDetails(s *sessions.Session, userDetails httpc.UserDetails) {
	(*s).Set(md.Config.SessionUserKey, userDetails)
	(*s).Set(md.Config.SessionACLKey, userDetails.GetRoles())
}

// GetMessage 取得目前的 Message
func (md *ACL) GetMessage(s *sessions.Session) (map[string]string, bool) {
	c, ok := (*s).Get(md.Config.SessionMessageKey).(map[string]string)
	return c, ok
}

// SetErrorMessage 設定目前的 Message
func (md *ACL) SetErrorMessage(s *sessions.Session, msg string) {
	md.SetMessage(s, "error", msg)
}

// SetErrorMessage 設定目前的 Message
func (md *ACL) SetInfoMessage(s *sessions.Session, msg string) {
	md.SetMessage(s, "info", msg)
}

// SetErrorMessage 設定目前的 Message
func (md *ACL) SetWarnMessage(s *sessions.Session, msg string) {
	md.SetMessage(s, "warn", msg)
}

func (md *ACL) SetMessage(s *sessions.Session, level string, msg string) {
	c, ok := (*s).Get(md.Config.SessionMessageKey).(map[string]string)

	if !ok {
		c = make(map[string]string)
		(*s).Set(md.Config.SessionMessageKey, c)
	}

	c[level] = msg
}

// SaveInfoMessage 設定目前的 Message,馬上執行 session save
func (md *ACL) SaveInfoMessage(s *sessions.Session, msg string) error {
	md.SetInfoMessage(s, msg)
	err := (*s).Save()

	if err != nil {
		md.log.Error("Save Session Error.", zap.Error(err))
	}

	return err
}

// SaveWarnMessage 設定目前的 Message,馬上執行 session save
func (md *ACL) SaveWarnMessage(s *sessions.Session, msg string) error {
	md.SetWarnMessage(s, msg)
	err := (*s).Save()

	if err != nil {
		md.log.Error("Save Session Error.", zap.Error(err))
	}

	return err
}

// SaveErrorMessage 設定目前的 Message,馬上執行 session save
func (md *ACL) SaveErrorMessage(s *sessions.Session, msg string) error {
	md.SetErrorMessage(s, msg)
	err := (*s).Save()

	if err != nil {
		md.log.Error("Save Session Error.", zap.Error(err))
	}

	return err
}

// SaveMessage 設定目前的 Message,馬上執行 session save
func (md *ACL) SaveMessage(s *sessions.Session, level string, msg string) error {
	md.SetMessage(s, level, msg)
	err := (*s).Save()

	if err != nil {
		md.log.Error("Save Session Error.", zap.Error(err))
	}

	return err
}

// ClearMessage 回傳目前的 Message，最後刪除掉 Session 中的值
func (md *ACL) ClearMessage(s *sessions.Session) (map[string]string, bool) {
	c, ok := (*s).Get(md.Config.SessionMessageKey).(map[string]string)

	if ok {
		(*s).Delete(md.Config.SessionMessageKey)
	}

	return c, ok
}
